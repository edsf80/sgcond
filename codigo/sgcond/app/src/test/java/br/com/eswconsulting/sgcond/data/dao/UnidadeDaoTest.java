package br.com.eswconsulting.sgcond.data.dao;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

@DataJpaTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test-config.properties")
public class UnidadeDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UnidadeDao unidadeDao;

	@Test
	public void findByIdentificadorAndCondominioId() {
		final String identificadorNovaUnidade = "XXXXX";

		Usuario usuario = this.entityManager.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", false));
		Condominio condominio = this.entityManager
				.persistFlushFind(new Condominio(null, "Teste Condominio", "111111111111"));
		Unidade unidadeNova = this.entityManager
				.persistFlushFind(new Unidade(null, identificadorNovaUnidade, usuario, condominio));

		Optional<Unidade> unidadeBusca = this.unidadeDao.findByIdentificadorAndCondominioId(
				unidadeNova.getIdentificador(), unidadeNova.getCondominio().getId());

		assertTrue(unidadeBusca.isPresent());
		assertTrue(unidadeBusca.get().getIdentificador().equals(identificadorNovaUnidade));
	}

	@Test
	public void findByIdentificadorAndCondominioIdNotFound() {

		Condominio condominio = this.entityManager
				.persistFlushFind(new Condominio(null, "Teste Condominio", "111111111111"));

		Optional<Unidade> unidadeBusca = this.unidadeDao.findByIdentificadorAndCondominioId("YYYYY", condominio.getId());

		assertTrue(!unidadeBusca.isPresent());
	}

}
