package br.com.eswconsulting.sgcond.api.rest.test;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashSet;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eswconsulting.sgcond.api.config.ResourceFilter;
import br.com.eswconsulting.sgcond.api.rest.LoginRestController;
import br.com.eswconsulting.sgcond.domain.entity.Papel;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.service.negocio.UsuarioService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {LoginRestController.class} , secure = false, excludeFilters = {@ComponentScan.Filter(
        type = FilterType.ASSIGNABLE_TYPE, classes = {ResourceFilter.class})})
public class LoginRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UsuarioService usuarioService;

	@TestConfiguration
    static class ContextConfiguration {

        @Bean
        public ModelMapper modelMapper() {            
            // set properties, etc.
            return new ModelMapper();
        }
    }
	
	@Test
	public void testExecutarLogin() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setUsername("teste");
		usuario.setSenha("senha");				

		given(usuarioService.executarLogin(usuario.getUsername(), usuario.getSenha())).willReturn(Optional.of(usuario));		

		mvc.perform(post("/login")
				  .content("{\"username\": \"teste\", \"senha\": \"senha\"}")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(header().string("Authorization", "Bearer jsdlfkjaldfjklsajdflksjalfk"))
			      .andExpect(jsonPath("$.username", is(usuario.getUsername())));		
	}

	@Test
	public void testExecutarLoginInvalido() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setUsername("teste");
		usuario.setSenha("senhaErrada");
		usuario.setId(1l);
		usuario.setMudarSenha(Boolean.FALSE);
		usuario.setNome("Teste");
		usuario.setPapeis(new HashSet<Papel>());

		given(usuarioService.executarLogin(usuario.getUsername(), usuario.getSenha())).willReturn(Optional.empty());
		
		mvc.perform(post("/login")
				  .content("{\"username\": \"teste\", \"senha\": \"senhaErrada\"}")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testExecutarLoginParametroInvalido() throws Exception {
		Usuario usuario = new Usuario();
		usuario.setUsername("teste");
		usuario.setSenha("senhaErrada");
		usuario.setId(1l);
		usuario.setMudarSenha(Boolean.FALSE);
		usuario.setNome("Teste");
		usuario.setPapeis(new HashSet<Papel>());

		given(usuarioService.executarLogin(usuario.getUsername(), usuario.getSenha())).willReturn(Optional.empty());
		
		mvc.perform(post("/login")
				  .content("{\"username\": \"teste\"}")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnprocessableEntity());
		
		mvc.perform(post("/login")
				  .content("{\"senha\": \"teste\"}")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isUnprocessableEntity());
	}
	
	@Test
	public void testLogout() throws Exception {		
		
		mvc.perform(get("/logout")
				  .header("Authorization", "Bearer dajsfkljsaflsalfjas")
				  .contentType(MediaType.APPLICATION_JSON))				  
			      .andExpect(status().isOk());
		
	}
}
