package br.com.eswconsulting.sgcond.api.rest.test;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eswconsulting.sgcond.api.config.ResourceFilter;
import br.com.eswconsulting.sgcond.api.rest.AreaComumRestController;
import br.com.eswconsulting.sgcond.data.dao.AreaComumDao;
import br.com.eswconsulting.sgcond.domain.entity.AreaComum;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { AreaComumRestController.class }, secure = false, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { ResourceFilter.class }) })
public class AreaComumRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private AreaComumDao areaComumDao;	

	private AreaComum areaComum;

	@Before
	public void setUp() {
		Condominio condominio = new Condominio(1l, "Cond teste", "23123232323232");
		this.areaComum = new AreaComum(1l, "Area comum", condominio);
	}

	@Test
	public void testBuscarTodos() throws Exception {

		List<AreaComum> areas = new ArrayList<>();
		areas.add(this.areaComum);

		given(areaComumDao.findAll()).willReturn(areas);

		mvc.perform(get("/areacomum").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", is(this.areaComum.getId().intValue())));
	}

	@Test
	public void testCriar() throws Exception {
		given(areaComumDao.save(any(AreaComum.class))).willReturn(this.areaComum);

		mvc.perform(post("/areacomum").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"descricao\": \"" + this.areaComum.getDescricao() + "\", \"condominio\": { \"id\": 1}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.areaComum.getId().intValue())));
	}

	@Test
	public void testCriarCamposObgNaoPreenchidos() throws Exception {
		mvc.perform(post("/areacomum").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"descricao\": \"" + this.areaComum.getDescricao() + "\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(post("/areacomum").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"condominio\": { \"id\": 1}}").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testAlterar() throws Exception {
		given(areaComumDao.save(any(AreaComum.class))).willReturn(this.areaComum);

		mvc.perform(put("/areacomum/" + this.areaComum.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.areaComum.getId() + ", \"descricao\": \"" + this.areaComum.getDescricao()
						+ "\", \"condominio\": { \"id\": 1}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.areaComum.getId().intValue())));
	}

	@Test
	public void testAlterarCamposObgNaoPreenchidos() throws Exception {
		given(areaComumDao.save(any(AreaComum.class))).willReturn(this.areaComum);

		mvc.perform(put("/areacomum/" + this.areaComum.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"descricao\": \"" + this.areaComum.getDescricao() + "\", \"condominio\": { \"id\": 1}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(put("/areacomum/" + this.areaComum.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.areaComum.getId() + ", \"condominio\": { \"id\": 1}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testBuscarPorId() throws Exception {

		given(areaComumDao.findById(this.areaComum.getId())).willReturn(Optional.of(this.areaComum));

		mvc.perform(get("/areacomum/" + this.areaComum.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.areaComum.getId().intValue())));
	}

	@Test
	public void testBuscarPorIdInvalido() throws Exception {

		final Long ID_INVALIDO = 2l;

		given(areaComumDao.findById(ID_INVALIDO)).willReturn(Optional.empty());

		mvc.perform(get("/areacomum/" + ID_INVALIDO).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}
}
