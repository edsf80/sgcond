package br.com.eswconsulting.sgcond.data.dao;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Enquete;
import br.com.eswconsulting.sgcond.domain.entity.Enquete.StatusEnquete;
import br.com.eswconsulting.sgcond.domain.entity.ItemEnquete;
import br.com.eswconsulting.sgcond.domain.entity.Papel;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

@DataJpaTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test-config.properties")
public class EnqueteDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private EnqueteDao enqueteDao;

	@Test
	public void testSaveSemItens() {
		Condominio cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));

		Enquete enquete = new Enquete();
		enquete.setTitulo("Quem vai querer a peça da água");
		enquete.setDataInicio(new Date());
		enquete.setDataFim(new Date());
		enquete.setCondominio(cond);
		enquete.setStatus(StatusEnquete.ABERTA);

		enqueteDao.save(enquete);
	}

	@Test
	public void testCriarComItens() {
		Condominio cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));
		Enquete enquete = new Enquete();
		enquete.setTitulo("Quem vai querer a peça da água?");
		enquete.setDataInicio(new Date());
		enquete.setDataFim(new Date());
		enquete.setCondominio(cond);
		enquete.setStatus(StatusEnquete.ABERTA);
		Set<ItemEnquete> itens = new HashSet<>();
		ItemEnquete item = new ItemEnquete();
		item.setDescricao("Sim");
		// item.setEnquete(enquete);
		itens.add(item);
		item = new ItemEnquete();
		item.setDescricao("Não");
		// item.setEnquete(enquete);
		itens.add(item);
		enquete.setItens(itens);

		enquete = enqueteDao.save(enquete);
		assertTrue(enquete.getItens().size() > 0);
	}

	@Test
	@Transactional
	public void testAlterarAdicionandoItens() {
		Condominio cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));

		Enquete enquete = this.entityManager.persistFlushFind(new Enquete(null, "Quem quer dinheiro?", cond, null,
				new Date(), new Date(), Enquete.StatusEnquete.ABERTA));

		ItemEnquete item = new ItemEnquete();
		item.setDescricao("Talvez");

		enquete.getItens().add(item);

		enquete = enqueteDao.save(enquete);
		assertTrue(enquete.getItens().size() > 0);
	}

	@Test
	public void testFindTodasSemRelacionamentos() {
		Condominio cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));

		this.entityManager.persistAndFlush(new Enquete(null, "Quem quer dinheiro?", cond, null, new Date(), new Date(),
				Enquete.StatusEnquete.ABERTA));

		List<Enquete> enquetes = StreamSupport.stream(enqueteDao.findTodasSemRelacionamentos().spliterator(), false)
				.collect(Collectors.toList());
		assertTrue(enquetes.size() > 0);
		assertTrue(enquetes.get(0).getItens().isEmpty());
	}

	@Test
	public void testFindTodasAbertas() {
		Condominio cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));

		this.entityManager.persistAndFlush(new Enquete(null, "Quem quer dinheiro?", cond, null, new Date(), new Date(),
				Enquete.StatusEnquete.ABERTA));

		List<Enquete> enquetes = StreamSupport.stream(enqueteDao.findTodasAbertas().spliterator(), false)
				.collect(Collectors.toList());
		assertTrue(enquetes.size() > 0);
		assertTrue(enquetes.get(0).getItens() == null);
	}

	@Test
	public void testFindTodasAbertasSemVotoPorUsuario() {
		Condominio cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));
		Set<Papel> papeis = new HashSet<>();
		papeis.add(new Papel(null, "Teste", "ROLE_TESTE"));
		Usuario usuario = this.entityManager
				.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", papeis, Boolean.FALSE));
		Set<ItemEnquete> itens = new HashSet<>();
		ItemEnquete item = new ItemEnquete();
		item.setDescricao("Sim");
		itens.add(item);
		item = new ItemEnquete();
		item.setDescricao("Não");
		itens.add(item);
		
		this.entityManager.persistAndFlush(new Unidade(null, "1234", usuario, cond));

		this.entityManager.persistAndFlush(new Enquete(null, "Quem vai querer a peça dagua?", cond,
				itens, new Date(), new Date(), StatusEnquete.ABERTA));

		List<Object[]> resultados = this.enqueteDao.findTodasAbertasSemVotoPorUsuario(usuario.getId());
		assertTrue(resultados.size() > 0);
	}
}
