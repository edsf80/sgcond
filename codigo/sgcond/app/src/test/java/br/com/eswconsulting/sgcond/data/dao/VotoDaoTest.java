package br.com.eswconsulting.sgcond.data.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Enquete;
import br.com.eswconsulting.sgcond.domain.entity.ItemEnquete;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.domain.entity.Voto;

@DataJpaTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test-config.properties")
public class VotoDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private VotoDao votoDao;

	private Condominio cond;

	private Enquete enquete;

	@Before
	public void setup() {
		this.cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));
		this.enquete = new Enquete(null, "Enquete teste", this.cond, new HashSet<ItemEnquete>(), null, null,
				Enquete.StatusEnquete.ABERTA);
		enquete.addItemEnquete(new ItemEnquete(null, "Sim", this.enquete));
		enquete.addItemEnquete(new ItemEnquete(null, "Não", this.enquete));
		enquete.addItemEnquete(new ItemEnquete(null, "Talvez", this.enquete));
		this.enquete = this.entityManager.persistFlushFind(enquete);
	}

	@Test
	public void testSave() {
		Voto voto = new Voto();
		Usuario usuario = this.entityManager
				.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", null, Boolean.FALSE));
		Unidade unidade = this.entityManager.persistFlushFind(new Unidade(null, "101", usuario, this.cond));

		voto.setComentario("Comentario teste");
		voto.setEnquete(this.enquete);
		voto.setItemEnquete(this.enquete.getItens().iterator().next());
		voto.setUnidade(unidade);

		assertNotNull(this.votoDao.save(voto));
	}

	@Test(expected = JpaObjectRetrievalFailureException.class)
	public void testSaveUnidadeInexistente() {
		Voto voto = new Voto();
		Usuario usuario = this.entityManager
				.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", null, Boolean.FALSE));

		voto.setComentario("Comentario teste");
		voto.setEnquete(this.enquete);
		voto.setItemEnquete(this.enquete.getItens().iterator().next());
		voto.setUnidade(new Unidade(20l, "Teste", usuario, this.cond));

		this.votoDao.save(voto);
	}

	@Test
	public void testFindVotosPorEnquete() {
		Usuario usuario = this.entityManager
				.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", null, Boolean.FALSE));
		Unidade unidade = this.entityManager.persistFlushFind(new Unidade(null, "101", usuario, this.cond));
		this.entityManager.persistFlushFind(
				new Voto(this.enquete, unidade, this.enquete.getItens().iterator().next(), "Comentario teste"));

		Set<Voto> votos = this.votoDao.findByEnqueteId(this.enquete.getId());
		assertFalse(votos.isEmpty());
	}

	@Test
	public void testFindVotosPorEnqueteInexistente() {
		Usuario usuario = this.entityManager
				.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", null, Boolean.FALSE));
		Unidade unidade = this.entityManager.persistFlushFind(new Unidade(null, "101", usuario, this.cond));
		this.entityManager.persistFlushFind(
				new Voto(this.enquete, unidade, this.enquete.getItens().iterator().next(), "Comentario teste"));

		Set<Voto> votos = this.votoDao.findByEnqueteId(1000l);
		assertTrue(votos.isEmpty());
	}
}
