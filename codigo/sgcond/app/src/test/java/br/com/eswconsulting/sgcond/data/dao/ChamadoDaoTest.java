package br.com.eswconsulting.sgcond.data.dao;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.Chamado;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.domain.entity.UsuarioSemRel;

@DataJpaTest
@TestPropertySource(locations = "classpath:application-test-config.properties")
@RunWith(SpringRunner.class)
public class ChamadoDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private ChamadoDao chamadoDao;
	
	private Condominio cond;
	
	private Usuario usr;
	
	private UsuarioSemRel criador;
	
	private Chamado cmd;
	
	@Before
	public void setup() {
		this.usr = this.entityManager.persistFlushFind(new Usuario(null, "tst", "teste", "teste", null, Boolean.FALSE));
		this.cond = this.entityManager.persistFlushFind(new Condominio(null, "Condotest", "45456465456464"));		
		this.criador = new UsuarioSemRel(usr.getId(), usr.getUsername(), usr.getNome());
		this.cmd = this.entityManager.persistFlushFind(new Chamado(null, null, "Chamado teste", "descricao teste",
				Chamado.StatusChamado.NOVO, cond, new HashSet<String>(), criador, new Date()));
	}

	@Test
	public void testFindByCondominioId() {
		List<Chamado> chamados = chamadoDao.findByCondominioId(cond.getId());
		assertTrue(!chamados.isEmpty());
	}

	@Test
	public void testFindByCondominioIdEmpty() {
		List<Chamado> chamados = chamadoDao.findByCondominioId(10l);
		assertTrue(chamados.isEmpty());
	}

	@Test
	public void testFindOne() {
		Optional<Chamado> chamado = chamadoDao.findById(this.cmd.getId());
		assertTrue(chamado.isPresent());
		assertTrue(chamado.get().getId().equals(this.cmd.getId()));
	}

	@Test
	public void testFindOneEmpty() {
		Optional<Chamado> chamado = chamadoDao.findById(cmd.getId());
		assertTrue(chamado.isPresent());
	}

	@Test
	public void testFindTop5ByCondominioIdOrderByDataCriacaoDesc() {
		List<Chamado> chamados = chamadoDao.findTop5ByCondominioIdOrderByDataCriacaoDesc(this.cond.getId());
		assertTrue(!chamados.isEmpty());
	}
}
