/**
 * 
 */
package br.com.eswconsulting.sgcond.test.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author edsf
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-config.properties")
@Import(EmbeddedRedisTestConfiguration.class)
// tirado de
// https://www.leveluplunch.com/java/tutorials/022-preload-database-execute-sql-spring-testing/
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:test-data.sql")
@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:clean-test-data.sql")
public class DashboardRestControllerTest {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	private String tokenProprietario;

	// private String tokenPorteiro;

	@Before
	public void setup() throws Exception {

		this.headers.setContentType(MediaType.APPLICATION_JSON);

		String requestBody = "{\"username\": \"proprietario@condominio.com\", \"senha\": \"123\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/login"), HttpMethod.POST,
				entity, String.class);
		this.tokenProprietario = response.getHeaders().get("Authorization").get(0);
	}

	@Test
	public void testBuscarDashboard() {
		this.headers.set("Authorization", this.tokenProprietario);

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard"), HttpMethod.GET,
				entity, String.class);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"{\"enquetes\":[{\"id\":1,\"titulo\":\"Enquete teste\",\"status\":\"ABERTA\",\"unidade\":null,\"itens\":[{\"id\":1,\"descricao\":\"Sim\"}]}],\"enquetesNaoVotadas\":[],\"chamados\":null,\"reservas\":[{\"area\":\"Area Teste\",\"inicio\":\""
						+ simpleDateFormat.format(new Date()) + " 12:00:00\",\"termino\":\""
						+ simpleDateFormat.format(new Date())
						+ " 12:00:00\",\"dono\":\"Proprietario\"}],\"avisos\":[{\"titulo\":\"Titulo teste\",\"descricao\":\"Descricao teste\"}]}"));
	}

	@Test
	public void testVotarEnquete() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 1, \"idItemEnquete\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"{\"id\":1,\"titulo\":\"Enquete teste\",\"status\":\"ABERTA\",\"unidade\":null,\"itens\":[{\"id\":1,\"descricao\":\"Sim\"}]}"));
	}

	@Test
	public void testVotarEnqueteCamposObgNaoEnviados() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testVotarItemEnqueteInexistente() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 1, \"idItemEnquete\": 8000}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testVotarEnqueteInexistente() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 1, \"idItemEnquete\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/10000"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testVotarEnqueteUnidadeNaoDono() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 2, \"idItemEnquete\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testVotarEnqueteFechada() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 1, \"idItemEnquete\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/2"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testVotarEnqueteUnidadeInexistente() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 10000, \"idItemEnquete\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"),
				HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testVotarEnqueteItemJaVotado() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"idUnidade\": 1, \"idItemEnquete\": 1}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"),
				HttpMethod.POST, entity, String.class);

		requestBody = "{\"idUnidade\": 1, \"idItemEnquete\": 1}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/dashboard/voto/1"), HttpMethod.POST, entity,
				String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
