package br.com.eswconsulting.sgcond.api.rest.test;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eswconsulting.sgcond.api.config.ResourceFilter;
import br.com.eswconsulting.sgcond.api.rest.UsuarioRestController;
import br.com.eswconsulting.sgcond.data.dao.UsuarioDao;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { UsuarioRestController.class }, secure = false, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { ResourceFilter.class }) })
public class UsuarioRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UsuarioDao usuarioDao;

	@MockBean
	private PasswordEncoder encoder;

	private Usuario usuario;

	private static final String SENHA_DESCRIPTOGRAFADA = "teste";

	private static final String SENHA_CRIPTOGRAFADA = "senhacriptografada";
	
	@TestConfiguration
    static class ContextConfiguration {

        @Bean
        public ModelMapper modelMapper() {            
            // set properties, etc.
            return new ModelMapper();
        }
    }

	@Before
	public void setUp() {
		usuario = new Usuario();
		usuario.setId(1l);
		usuario.setMudarSenha(Boolean.TRUE);
		usuario.setNome("Teste");
		usuario.setSenha(SENHA_CRIPTOGRAFADA);
		usuario.setUsername("teste");
	}

	@Test
	public void testCriarUsuario() throws Exception {
		UsuarioRestController.UsuarioRest usrRest = new UsuarioRestController.UsuarioRest();
		usrRest.setNome(this.usuario.getNome());
		usrRest.setUsername(this.usuario.getUsername());
		usrRest.setSenha(SENHA_DESCRIPTOGRAFADA);
		usrRest.setMudarSenha(Boolean.TRUE);

		given(encoder.encode(SENHA_DESCRIPTOGRAFADA)).willReturn(SENHA_CRIPTOGRAFADA);
		given(usuarioDao.save(any(Usuario.class))).willReturn(this.usuario);

		mvc.perform(post("/usuario").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"nome\": \"" + usuario.getNome() + "\", \"username\": \"" + this.usuario.getUsername()
						+ "\", \"senha\": \"" + SENHA_DESCRIPTOGRAFADA + "\", \"mudarSenha\": true}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.username", is(this.usuario.getUsername())));
	}

	@Test
	public void testCriarUsuarioSemCamposObg() throws Exception {

		mvc.perform(post("/usuario").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"nome\": \"" + usuario.getNome() + "\", \"username\": \"" + this.usuario.getUsername()
						+ "\", \"mudarSenha\": true}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(post("/usuario").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"username\": \"" + this.usuario.getUsername() + "\", \"senha\": \"" + SENHA_DESCRIPTOGRAFADA
						+ "\", \"mudarSenha\": true}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testMudarSenha() throws Exception {

		final String novaSenha = "novaSenha";

		given(usuarioDao.findById(this.usuario.getId())).willReturn(Optional.of(usuario));
		given(encoder.matches(SENHA_DESCRIPTOGRAFADA, usuario.getSenha())).willReturn(Boolean.TRUE);
		given(encoder.encode(novaSenha)).willReturn("novasenhacriptografada");

		mvc.perform(
				put("/usuario/" + this.usuario.getId() + "/senha").header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"username\": \"" + this.usuario.getUsername() + "\", \"senhaAtual\": \""
								+ SENHA_DESCRIPTOGRAFADA + "\", \"novaSenha\": \"" + novaSenha + "\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testMudarSenhaSenhasIguais() throws Exception {

		mvc.perform(
				put("/usuario/" + this.usuario.getId() + "/senha").header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"username\": \"" + this.usuario.getUsername() + "\", \"senhaAtual\": \""
								+ SENHA_DESCRIPTOGRAFADA + "\", \"novaSenha\": \"" + SENHA_DESCRIPTOGRAFADA + "\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testMudarSenhaSenhaAtualIncorreta() throws Exception {

		given(usuarioDao.findById(this.usuario.getId())).willReturn(Optional.of(usuario));
		given(encoder.matches(SENHA_DESCRIPTOGRAFADA, usuario.getSenha())).willReturn(Boolean.TRUE);

		mvc.perform(
				put("/usuario/" + this.usuario.getId() + "/senha").header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"username\": \"" + this.usuario.getUsername()
								+ "\", \"senhaAtual\": \"senhaErrada\", \"novaSenha\": \"novaSenha\"}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testMudarSenhaUsuarioNaoEncontrado() throws Exception {

		final Long idInexistente = 2l;

		given(usuarioDao.findById(idInexistente)).willReturn(Optional.empty());

		mvc.perform(put("/usuario/" + idInexistente + "/senha").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"username\": \"" + this.usuario.getUsername()
						+ "\", \"senhaAtual\": \"teste\", \"novaSenha\": \"novaSenha\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void testBuscarPorId() throws Exception {

		given(usuarioDao.findById(this.usuario.getId())).willReturn(Optional.of(usuario));		

		mvc.perform(get("/usuario/" + this.usuario.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.usuario.getId().intValue())))
				.andExpect(jsonPath("$.username", is(this.usuario.getUsername())));
	}

	@Test
	public void testBuscarPorIdInexistente() throws Exception {

		final Long idInexistente = 2l;

		given(usuarioDao.findById(idInexistente)).willReturn(Optional.empty());

		mvc.perform(get("/usuario/" + idInexistente).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void testAlterarUsuario() throws Exception {
		UsuarioRestController.UsuarioRest usrRest = new UsuarioRestController.UsuarioRest();
		usrRest.setId(this.usuario.getId());
		usrRest.setNome(this.usuario.getNome());
		usrRest.setUsername(this.usuario.getUsername());
		usrRest.setSenha("123");
		usrRest.setMudarSenha(Boolean.TRUE);

		given(encoder.encode(SENHA_DESCRIPTOGRAFADA)).willReturn(SENHA_CRIPTOGRAFADA);
		given(usuarioDao.save(any(Usuario.class))).willReturn(this.usuario);
		given(usuarioDao.findById(this.usuario.getId())).willReturn(Optional.of(this.usuario));

		mvc.perform(put("/usuario/" + usrRest.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + usrRest.getId() + ", \"nome\": \"" + usrRest.getNome() + "\", \"username\": \""
						+ usrRest.getUsername() + "\", \"senha\": \"" + usrRest.getSenha() + "\", \"mudarSenha\": "
						+ usrRest.getMudarSenha() + "}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.username", is(this.usuario.getUsername())));

		// Como a senha não é obrigatória na alteração, é necessário testar sem ela.
		mvc.perform(put("/usuario/" + usrRest.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + usrRest.getId() + ", \"nome\": \"" + usrRest.getNome() + "\", \"username\": \""
						+ usrRest.getUsername() + "\", \"mudarSenha\": " + usrRest.getMudarSenha() + "}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.id", is(1)))
				.andExpect(jsonPath("$.username", is(this.usuario.getUsername())));
	}

	@Test
	public void testAlterarUsuarioInexistente() throws Exception {
		UsuarioRestController.UsuarioRest usrRest = new UsuarioRestController.UsuarioRest();
		usrRest.setId(this.usuario.getId());
		usrRest.setNome(this.usuario.getNome());
		usrRest.setUsername(this.usuario.getUsername());
		usrRest.setSenha("123");
		usrRest.setMudarSenha(Boolean.TRUE);

		final Long idInexistente = 2l;

		given(usuarioDao.findById(idInexistente)).willReturn(Optional.empty());

		mvc.perform(put("/usuario/" + idInexistente).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + usrRest.getId() + ", \"nome\": \"" + usrRest.getNome() + "\", \"username\": \""
						+ usrRest.getUsername() + "\", \"senha\": \"" + usrRest.getSenha() + "\", \"mudarSenha\": "
						+ usrRest.getMudarSenha() + "}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void testAlterarUsuarioInexistenteSemCamposObg() throws Exception {
		UsuarioRestController.UsuarioRest usrRest = new UsuarioRestController.UsuarioRest();
		usrRest.setId(this.usuario.getId());
		usrRest.setNome(this.usuario.getNome());
		usrRest.setUsername(this.usuario.getUsername());
		usrRest.setSenha("123");
		usrRest.setMudarSenha(Boolean.TRUE);

		mvc.perform(put("/usuario/" + this.usuario.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + usrRest.getId() + ", \"username\": \"" + usrRest.getUsername()
						+ "\", \"senha\": \"" + usrRest.getSenha() + "\", \"mudarSenha\": " + usrRest.getMudarSenha()
						+ "}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(put("/usuario/" + this.usuario.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + usrRest.getId() + ", \"senha\": \"" + usrRest.getSenha() + "\", \"mudarSenha\": "
						+ usrRest.getMudarSenha() + "}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testListarTodos() throws Exception {

		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(this.usuario);

		given(usuarioDao.findAllNoRelationShips()).willReturn(usuarios);

		mvc.perform(get("/usuario").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", is(this.usuario.getId().intValue())));
	}
}
