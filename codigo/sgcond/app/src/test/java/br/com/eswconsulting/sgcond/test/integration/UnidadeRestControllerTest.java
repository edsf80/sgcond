package br.com.eswconsulting.sgcond.test.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-config.properties")
@Import(EmbeddedRedisTestConfiguration.class)
// tirado de
// https://www.leveluplunch.com/java/tutorials/022-preload-database-execute-sql-spring-testing/
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:test-data.sql")
@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:clean-test-data.sql")
public class UnidadeRestControllerTest {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	private String tokenProprietario;

	@Before
	public void setup() throws Exception {

		this.headers.setContentType(MediaType.APPLICATION_JSON);

		String requestBody = "{\"username\": \"proprietario@condominio.com\", \"senha\": \"123\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/login"), HttpMethod.POST,
				entity, String.class);
		this.tokenProprietario = response.getHeaders().get("Authorization").get(0);
	}

	@Test
	public void testBuscarTodos() {
		this.headers.set("Authorization", this.tokenProprietario);

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade"), HttpMethod.GET,
				entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"[{\"id\":1,\"identificador\":\"1601\",\"usuario\":{\"id\":1,\"username\":\"proprietario@condominio.com\",\"nome\":\"Proprietario\"},\"condominio\":{\"id\":1}},{\"id\":2,\"identificador\":\"2402\",\"usuario\":{\"id\":2,\"username\":\"sindico@condominio.com\",\"nome\":\"Sindico\"},\"condominio\":{\"id\":1}}]"));

	}

	@Test
	public void testCriar() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"identificador\": \"480A\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade"), HttpMethod.POST,
				entity, String.class);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"{\"id\":3,\"identificador\":\"480A\",\"usuario\":{\"id\":1,\"username\":null,\"nome\":null},\"condominio\":{\"id\":1}}"));
	}

	@Test
	public void testCriarSemCamposObg() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade"), HttpMethod.POST,
				entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"identificador\": \"480A\", \"condominio\": { \"id\": 1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/unidade"), HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testCriarIdentificadorUnidadeExistente() {
		this.headers.set("Authorization", this.tokenProprietario);

		// O identificador abaixo está no arquivo test-data.sql e caso o mesmo seja
		// alterado esse ponto deve ser avaliado.
		String requestBody = "{\"identificador\": \"1601\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade"), HttpMethod.POST,
				entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
		assertTrue(response.getBody().equals("Identificador da unidade já existe no condomínio"));
	}

	@Test
	public void testBuscarPorId() {
		this.headers.set("Authorization", this.tokenProprietario);

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.GET,
				entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"{\"id\":1,\"identificador\":\"1601\",\"usuario\":{\"id\":1,\"username\":\"proprietario@condominio.com\",\"nome\":\"Proprietario\"},\"condominio\":{\"id\":1}}"));
	}

	@Test
	public void testBuscarPorIdInexistente() {
		this.headers.set("Authorization", this.tokenProprietario);

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade/1000"), HttpMethod.GET,
				entity, String.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testAlterar() {
		this.headers.set("Authorization", this.tokenProprietario);
		
		String requestBody = "{\"id\": 1, \"identificador\": \"1601\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"{\"id\":1,\"identificador\":\"1601\",\"usuario\":{\"id\":1,\"username\":\"proprietario@condominio.com\",\"nome\":\"Proprietario\"},\"condominio\":{\"id\":1}}"));

		requestBody = "{\"id\": 1, \"identificador\": \"480AB\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().equals(
				"{\"id\":1,\"identificador\":\"480AB\",\"usuario\":{\"id\":1,\"username\":\"proprietario@condominio.com\",\"nome\":\"Proprietario\"},\"condominio\":{\"id\":1}}"));
	}

	@Test
	public void testAlterarNotFound() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"id\": 500, \"identificador\": \"480AB\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade/500"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testAlterarSemCamposObg() {
		this.headers.set("Authorization", this.tokenProprietario);

		String requestBody = "{\"identificador\": \"480AB\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"id\": 1, \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.PUT, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"id\": 1, \"identificador\": \"480AB\", \"condominio\": { \"id\": 1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.PUT, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testAlterarIdentificadorParaUmExistente() {
		this.headers.set("Authorization", this.tokenProprietario);

		// O identificador abaixo está no arquivo test-data.sql e caso o mesmo seja
		// alterado esse ponto deve ser avaliado.
		String requestBody = "{\"id\": 1, \"identificador\": \"2402\", \"usuario\": {\"id\": 1},\"condominio\": { \"id\": 1}}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/unidade/1"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
		assertTrue(response.getBody().equals("Tentativa de atualização para um identificador de outra unidade"));
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
