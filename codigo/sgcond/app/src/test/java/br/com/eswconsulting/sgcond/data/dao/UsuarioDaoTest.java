package br.com.eswconsulting.sgcond.data.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.Usuario;

@DataJpaTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test-config.properties")
public class UsuarioDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UsuarioDao usuarioDao;

	@Test
	public void testFindAllNoRelationShips() {
		this.entityManager.persistAndFlush(new Usuario(null, "tst", "Teste", "teste", null, false));
		this.entityManager.persistAndFlush(new Usuario(null, "tst2", "Teste2", "teste2", null, false));

		List<Usuario> usuarios = this.usuarioDao.findAllNoRelationShips();
		assertFalse(usuarios.isEmpty());
		assertNull(usuarios.get(0).getPapeis());
		assertNotNull(usuarios.get(0).getNome());
	}

}
