package br.com.eswconsulting.sgcond.data.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.Aviso;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;

@DataJpaTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test-config.properties")
public class AvisoDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private AvisoDao avisoDao;
	
	@Test
	public void testFindByValidadeGreaterThanEqual() {
		Calendar data = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		data.add(Calendar.YEAR, 1);
		
		Condominio condominio = this.entityManager
				.persistFlushFind(new Condominio(null, "Teste Condominio", "111111111111"));
		this.entityManager.persistAndFlush(new Aviso(null, "Titulo teste", "Descricao teste", new Date(), condominio));
		this.entityManager.persistAndFlush(new Aviso(null, "Titulo teste 2", "Descricao teste 2", data.getTime(), condominio));
		
		Calendar inicio = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		Calendar termino = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		termino.add(Calendar.HOUR, 24);
		
		List<Aviso> avisos = this.avisoDao.findByValidadeGreaterThanEqual(inicio.getTime());
		
		assertTrue(!avisos.isEmpty());
		assertTrue(avisos.size() == 2);
		assertTrue(avisos.get(0).getTitulo().equals("Titulo teste"));
	}

}
