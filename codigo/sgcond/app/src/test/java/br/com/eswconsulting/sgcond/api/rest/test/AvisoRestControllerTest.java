package br.com.eswconsulting.sgcond.api.rest.test;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eswconsulting.sgcond.api.config.ResourceFilter;
import br.com.eswconsulting.sgcond.api.rest.AvisoRestController;
import br.com.eswconsulting.sgcond.data.dao.AvisoDao;
import br.com.eswconsulting.sgcond.domain.entity.Aviso;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { AvisoRestController.class }, secure = false, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { ResourceFilter.class }) })
public class AvisoRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private AvisoDao avisoDao;

	private Aviso aviso;

	@TestConfiguration
	static class ContextConfiguration {

		@Bean
		public ModelMapper modelMapper() {
			return new ModelMapper();
		}
	}

	@Before
	public void setUp() {
		this.aviso = new Aviso(1l, "Titulo teste", "Descrição teste", new Date(),
				new Condominio(1l, "Teste", "123456789"));
	}

	@Test
	public void testBuscarTodos() throws Exception {
		List<Aviso> avisos = new ArrayList<>();
		avisos.add(this.aviso);

		given(avisoDao.findAll()).willReturn(avisos);

		mvc.perform(get("/aviso").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", is(this.aviso.getId().intValue())));
	}

	@Test
	public void testBuscarPorId() throws Exception {
		given(avisoDao.findById(this.aviso.getId())).willReturn(Optional.of(this.aviso));

		mvc.perform(get("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.aviso.getId().intValue())));
	}

	@Test
	public void testBuscarPorIdNotFound() throws Exception {
		given(avisoDao.findById(this.aviso.getId())).willReturn(Optional.empty());

		mvc.perform(get("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void testCriar() throws Exception {
		given(avisoDao.findById(any(Long.class))).willReturn(Optional.empty());
		given(avisoDao.save(any(Aviso.class))).willReturn(this.aviso);

		mvc.perform(post("/aviso").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"titulo\": \"" + this.aviso.getTitulo() + "\", \"descricao\": \""
						+ this.aviso.getDescricao() + "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(this.aviso.getId().intValue())));
	}

	@Test
	public void testCriarCamposObgNaoPreenchidos() throws Exception {
		mvc.perform(post("/aviso").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content(
						"{\"descricao\": \"" + this.aviso.getDescricao() + "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(post("/aviso").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"titulo\": \"" + this.aviso.getTitulo() + "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(post("/aviso").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"titulo\": \"" + this.aviso.getTitulo() + "\", \"descricao\": \""
						+ this.aviso.getDescricao() + "\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testAlterar() throws Exception {
		given(avisoDao.findById(this.aviso.getId())).willReturn(Optional.of(this.aviso));
		given(avisoDao.save(any(Aviso.class))).willReturn(this.aviso);

		mvc.perform(put("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.aviso.getId() + ", \"titulo\": \"" + this.aviso.getTitulo()
						+ "\", \"descricao\": \"" + this.aviso.getDescricao()
						+ "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.aviso.getId().intValue())));
	}

	@Test
	public void testAlterarSemCamposObg() throws Exception {
		given(avisoDao.findById(this.aviso.getId())).willReturn(Optional.of(this.aviso));
		given(avisoDao.save(any(Aviso.class))).willReturn(this.aviso);

		mvc.perform(put("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"titulo\": \"" + this.aviso.getTitulo() + "\", \"descricao\": \""
						+ this.aviso.getDescricao() + "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(put("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.aviso.getId() + ", \"descricao\": \"" + this.aviso.getDescricao()
						+ "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(put("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"titulo\": \"" + this.aviso.getTitulo() + "\", \"id\": " + this.aviso.getId()
						+ ", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(put("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"titulo\": \"" + this.aviso.getTitulo() + "\", \"descricao\": \""
						+ this.aviso.getDescricao() + "\", \"id\": " + this.aviso.getId() + "}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testAlterarAvisoInexistente() throws Exception {
		given(avisoDao.findById(this.aviso.getId())).willReturn(Optional.empty());

		mvc.perform(put("/aviso/" + this.aviso.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.aviso.getId() + ", \"titulo\": \"" + this.aviso.getTitulo()
						+ "\", \"descricao\": \"" + this.aviso.getDescricao()
						+ "\", \"validade\": \"2018-05-10 00:00:00\"}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

}
