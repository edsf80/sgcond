package br.com.eswconsulting.sgcond.data.dao;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.domain.entity.AreaComum;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Reserva;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

@DataJpaTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test-config.properties")
public class ReservaDaoTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private ReservaDao reservaDao;

	@Test
	public void findByData() {
		Usuario usuario = this.entityManager.persistFlushFind(new Usuario(null, "teste", "Teste", "teste", false));
		Condominio condominio = this.entityManager
				.persistFlushFind(new Condominio(null, "Condominio teste", "123456789"));
		AreaComum areaComum = this.entityManager.persistFlushFind(new AreaComum(null, "Area teste", condominio));

		this.entityManager.persistAndFlush(new Reserva(null, new Date(), new Date(), usuario, areaComum));		

		Calendar inicio = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		Calendar termino = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		termino.add(Calendar.HOUR, 24);

		List<Reserva> reservas = this.reservaDao.findByDataHoraInicioBetween(inicio.getTime(), termino.getTime());

		assertTrue(!reservas.isEmpty());
	}
}
