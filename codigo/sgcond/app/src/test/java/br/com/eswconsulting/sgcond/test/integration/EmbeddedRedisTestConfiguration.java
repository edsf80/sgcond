/**
 * 
 */
package br.com.eswconsulting.sgcond.test.integration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import com.github.caryyu.spring.embedded.redisserver.RedisServerConfiguration;

/**
 * @author edsf
 *         https://stackoverflow.com/questions/32524194/embedded-redis-for-spring-boot
 */
@TestConfiguration
public class EmbeddedRedisTestConfiguration {

	@Bean
	public RedisServerConfiguration redisServerConfiguration(){
	    return new RedisServerConfiguration();
	}	
}
