package br.com.eswconsulting.sgcond.api.rest.test;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.eswconsulting.sgcond.api.config.ResourceFilter;
import br.com.eswconsulting.sgcond.api.rest.UnidadeRestController;
import br.com.eswconsulting.sgcond.data.dao.UnidadeDao;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { UnidadeRestController.class }, secure = false, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { ResourceFilter.class }) })
public class UnidadeRestControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UnidadeDao unidadeDao;

	private Unidade unidade;

	@TestConfiguration
	static class ContextConfiguration {

		@Bean
		public ModelMapper modelMapper() {
			// set properties, etc.
			return new ModelMapper();
		}
	}

	@Before
	public void setUp() {
		this.unidade = new Unidade(1l, "teste", new Usuario(1l, "teste", "Teste", "senha", null, false),
				new Condominio(1l, "Teste", "123456789"));
	}

	@Test
	public void testBuscarTodos() throws Exception {
		List<Unidade> unidades = new ArrayList<>();
		unidades.add(this.unidade);

		given(unidadeDao.findAll()).willReturn(unidades);

		mvc.perform(get("/unidade").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$[0].id", is(this.unidade.getId().intValue())));
	}

	@Test
	public void testBuscarPorId() throws Exception {
		given(unidadeDao.findById(this.unidade.getId())).willReturn(Optional.of(this.unidade));

		mvc.perform(get("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.unidade.getId().intValue())));
	}

	@Test
	public void testBuscarPorIdNotFound() throws Exception {
		given(unidadeDao.findById(this.unidade.getId())).willReturn(Optional.empty());

		mvc.perform(get("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	public void testCriar() throws Exception {
		given(unidadeDao.findByIdentificadorAndCondominioId(any(String.class), any(Long.class)))
				.willReturn(Optional.empty());
		given(unidadeDao.save(any(Unidade.class))).willReturn(this.unidade);

		mvc.perform(post("/unidade").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"identificador\": \"" + this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
						+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(this.unidade.getId().intValue())));

		// Sem condominio, deve ser colocado quando tiver mais de um.
		mvc.perform(post("/unidade").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"identificador\": \"" + this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
						+ this.unidade.getUsuario().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(this.unidade.getId().intValue())));
	}

	@Test
	public void testCriarCamposObgNaoPreenchidos() throws Exception {
		mvc.perform(post("/unidade").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"usuario\": {\"id\": " + this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(post("/unidade").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"identificador\": \"" + this.unidade.getIdentificador() + "\", \"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testCriarUnidadeExistenteNoCondominio() throws Exception {
		given(unidadeDao.findByIdentificadorAndCondominioId(any(String.class), any(Long.class)))
				.willReturn(Optional.of(this.unidade));

		mvc.perform(post("/unidade").header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"identificador\": \"" + this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
						+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testAlterar() throws Exception {
		given(unidadeDao.findById(this.unidade.getId())).willReturn(Optional.of(this.unidade));
		given(unidadeDao.save(any(Unidade.class))).willReturn(this.unidade);
		given(unidadeDao.findByIdentificadorAndCondominioId(this.unidade.getIdentificador(),
				this.unidade.getCondominio().getId())).willReturn(Optional.of(this.unidade));

		mvc.perform(
				put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"id\": " + this.unidade.getId() + ", \"identificador\": \""
								+ this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
								+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
								+ this.unidade.getCondominio().getId() + "}}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(this.unidade.getId().intValue())));

		// TODO: Requerer o condominio quando houver mais de um.
		// Sem o condominio por enquanto que só existe 1
		mvc.perform(
				put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"id\": " + this.unidade.getId() + ", \"identificador\": \""
								+ this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
								+ this.unidade.getUsuario().getId() + "}}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(this.unidade.getId().intValue())));

		// Alterar o identificador para um não existente.
		mvc.perform(put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.unidade.getId() + ", \"identificador\": \"XXXXXXX\", \"usuario\": {\"id\": "
						+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(this.unidade.getId().intValue())));
	}

	@Test
	public void testAlterarSemCamposObg() throws Exception {
		given(unidadeDao.findById(this.unidade.getId())).willReturn(Optional.of(this.unidade));
		given(unidadeDao.save(any(Unidade.class))).willReturn(this.unidade);

		mvc.perform(put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"identificador\": \"" + this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
						+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
				.content("{\"id\": " + this.unidade.getId() + ", \"usuario\": {\"id\": "
						+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
						+ this.unidade.getCondominio().getId() + "}}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isUnprocessableEntity());

		mvc.perform(
				put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"id\": " + this.unidade.getId() + ", \"identificador\": \""
								+ this.unidade.getIdentificador() + "\",\"condominio\": { \"id\": "
								+ this.unidade.getCondominio().getId() + "}}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testAlterarIdentificadorParaUmExistente() throws Exception {
		Unidade unidadeErro = new Unidade(20l, this.unidade.getIdentificador(), new Usuario(), new Condominio());

		given(unidadeDao.findById(this.unidade.getId())).willReturn(Optional.of(this.unidade));
		given(unidadeDao.save(any(Unidade.class))).willReturn(this.unidade);
		given(unidadeDao.findByIdentificadorAndCondominioId(any(String.class), any(Long.class)))
				.willReturn(Optional.of(unidadeErro));

		mvc.perform(
				put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"id\": " + this.unidade.getId() + ", \"identificador\": \""
								+ this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
								+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
								+ this.unidade.getCondominio().getId() + "}}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());
	}

	@Test
	public void testAlterarUnidadeInexistente() throws Exception {
		given(unidadeDao.findById(this.unidade.getId())).willReturn(Optional.empty());

		mvc.perform(
				put("/unidade/" + this.unidade.getId()).header("Authorization", "Bearer dajsfkljsaflsalfjas")
						.content("{\"id\": " + this.unidade.getId() + ", \"identificador\": \""
								+ this.unidade.getIdentificador() + "\", \"usuario\": {\"id\": "
								+ this.unidade.getUsuario().getId() + "},\"condominio\": { \"id\": "
								+ this.unidade.getCondominio().getId() + "}}")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

}
