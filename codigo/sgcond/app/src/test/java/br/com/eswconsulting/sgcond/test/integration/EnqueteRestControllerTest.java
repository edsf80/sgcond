package br.com.eswconsulting.sgcond.test.integration;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
		  locations = "classpath:application-test-config.properties")
@Import(EmbeddedRedisTestConfiguration.class)
// tirado de
// https://www.leveluplunch.com/java/tutorials/022-preload-database-execute-sql-spring-testing/
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:test-data.sql")
@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:clean-test-data.sql")
public class EnqueteRestControllerTest {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	private String tokenProprietario;

	@Before
	public void setup() throws Exception {

		this.headers.setContentType(MediaType.APPLICATION_JSON);

		String requestBody = "{\"username\": \"proprietario@condominio.com\", \"senha\": \"123\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/login"), HttpMethod.POST,
				entity, String.class);
		this.tokenProprietario = response.getHeaders().get("Authorization").get(0);
	}

	@Test
	public void testBuscarResultadoEnquete() {
		this.headers.set("Authorization", this.tokenProprietario);

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/enquete/resultado/1"),
				HttpMethod.GET, entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(response.getBody(),
				"[{\"unidade\":{\"identificador\":\"1601\"},\"itemEnquete\":{\"id\":1,\"descricao\":\"Sim\"},\"comentario\":\"Comentario teste\"},{\"unidade\":{\"identificador\":\"2402\"},\"itemEnquete\":{\"id\":1,\"descricao\":\"Sim\"},\"comentario\":\"Comentario teste 2\"}]");
	}

	@Test
	public void testBuscarResultadoEnqueteInexistenteOuSemVoto() {
		this.headers.set("Authorization", this.tokenProprietario);

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/enquete/resultado/10000"),
				HttpMethod.GET, entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(response.getBody(), "[]");
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}

}
