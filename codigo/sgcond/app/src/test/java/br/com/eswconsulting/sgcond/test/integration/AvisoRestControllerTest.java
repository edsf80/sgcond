package br.com.eswconsulting.sgcond.test.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.eswconsulting.sgcond.api.rest.AvisoRestController;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test-config.properties")
@Import(EmbeddedRedisTestConfiguration.class)
// tirado de
// https://www.leveluplunch.com/java/tutorials/022-preload-database-execute-sql-spring-testing/
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:test-data.sql")
@Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:clean-test-data.sql")
public class AvisoRestControllerTest {

	@LocalServerPort
	private int port;

	private TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	private String tokenSindico;

	@Before
	public void setup() throws Exception {

		this.headers.setContentType(MediaType.APPLICATION_JSON);

		String requestBody = "{\"username\": \"sindico@condominio.com\", \"senha\": \"123\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/login"), HttpMethod.POST,
				entity, String.class);
		this.tokenSindico = response.getHeaders().get("Authorization").get(0);
	}

	@Test
	public void testBuscarTodos() {
		this.headers.set("Authorization", this.tokenSindico);

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso"), HttpMethod.GET, entity,
				String.class);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody()
				.equals("[{\"id\":1,\"titulo\":\"Titulo teste\",\"descricao\":\"Descricao teste\",\"validade\":\""
						+ simpleDateFormat.format(new Date()) + "\",\"condominio\":{\"id\":1}}]"));

	}

	@Test
	public void testCriar() {
		this.headers.set("Authorization", this.tokenSindico);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String requestBody = "{\"titulo\": \"Novo titulo\", \"descricao\": \"Descricao nova\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso"), HttpMethod.POST,
				entity, String.class);

		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertTrue(response.getBody()
				.equals("{\"id\":2,\"titulo\":\"Novo titulo\",\"descricao\":\"Descricao nova\",\"validade\":\""
						+ simpleDateFormat.format(new Date()) + "\",\"condominio\":{\"id\":1}}"));
	}

	@Test
	public void testCriarSemCamposObg() {
		this.headers.set("Authorization", this.tokenSindico);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String requestBody = "{\"descricao\": \"Descricao nova\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso"), HttpMethod.POST,
				entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"titulo\": \"Novo titulo\", \"validade\": \"" + simpleDateFormat.format(new Date()) + "\"}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/aviso"), HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"titulo\": \"Novo titulo\", \"descricao\": \"Descricao nova\"}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/aviso"), HttpMethod.POST, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	@Test
	public void testBuscarPorId() {
		this.headers.set("Authorization", this.tokenSindico);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso/1"), HttpMethod.GET,
				entity, String.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody()
				.equals("{\"id\":1,\"titulo\":\"Titulo teste\",\"descricao\":\"Descricao teste\",\"validade\":\""
						+ simpleDateFormat.format(new Date()) + "\",\"condominio\":{\"id\":1}}"));
	}

	@Test
	public void testBuscarPorIdInexistente() {
		this.headers.set("Authorization", this.tokenSindico);

		HttpEntity<String> entity = new HttpEntity<String>(this.headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso/1000"), HttpMethod.GET,
				entity, String.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testAlterar() {
		this.headers.set("Authorization", this.tokenSindico);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String requestBody = "{\"id\": 1, \"titulo\": \"Novo titulo\", \"descricao\": \"Descricao nova\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\"}";

		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<AvisoRestController.AvisoRest> response = restTemplate.exchange(
				createURLWithPort("/api/aviso/1"), HttpMethod.PUT, entity, AvisoRestController.AvisoRest.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().getId().equals(1l));

		requestBody = "{\"id\": 1, \"titulo\": \"Novo titulo\", \"descricao\": \"Descricao alterada\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\", \"condominio\":{\"id\":1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/aviso/1"), HttpMethod.PUT, entity,
				AvisoRestController.AvisoRest.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody().getId().equals(1l));
	}

	@Test
	public void testAlterarNotFound() {
		this.headers.set("Authorization", this.tokenSindico);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String requestBody = "{\"id\": 1000, \"titulo\": \"Novo titulo\", \"descricao\": \"Descricao nova\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\"}";
		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso/1000"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
	}

	@Test
	public void testAlterarSemCamposObg() {
		this.headers.set("Authorization", this.tokenSindico);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String requestBody = "{\"titulo\": \"Novo titulo\", \"descricao\": \"Descricao nova\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\"}";

		HttpEntity<String> entity = new HttpEntity<String>(requestBody, headers);

		ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/aviso/1"), HttpMethod.PUT,
				entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"id\": 1, \"descricao\": \"Descricao alterada\", \"validade\": \""
				+ simpleDateFormat.format(new Date()) + "\", \"condominio\":{\"id\":1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/aviso/1"), HttpMethod.PUT, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"id\": 1, \"titulo\": \"Novo titulo\", \"validade\": \"" + simpleDateFormat.format(new Date())
				+ "\", \"condominio\":{\"id\":1}}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/aviso/1"), HttpMethod.PUT, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());

		requestBody = "{\"id\": 1, \"titulo\": \"Novo titulo\", \"descricao\": \"Descricao alterada\"}";
		entity = new HttpEntity<String>(requestBody, headers);

		response = restTemplate.exchange(createURLWithPort("/api/aviso/1"), HttpMethod.PUT, entity, String.class);

		assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, response.getStatusCode());
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
