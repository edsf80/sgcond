/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import br.com.eswconsulting.sgcond.domain.entity.Voto;
import br.com.eswconsulting.sgcond.domain.entity.VotoId;

/**
 * @author edsf
 *
 */
public interface VotoDao extends CrudRepository<Voto, VotoId> {

	Set<Voto> findByEnqueteId(Long id);	
}
