/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * @author edsf
 *
 */
@Data
public class VotoId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2392685892983339323L;

	/**
	 * 
	 */
	private long enquete;

	/**
	 * 
	 */
	private long unidade;
	
}
