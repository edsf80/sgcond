package br.com.eswconsulting.sgcond.data.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eswconsulting.sgcond.domain.entity.Aviso;

public interface AvisoDao extends JpaRepository<Aviso, Long> {

	/**
	 * @param inicio
	 * @param termino
	 * @return
	 */
	List<Aviso> findByValidadeGreaterThanEqual(Date data);
}
