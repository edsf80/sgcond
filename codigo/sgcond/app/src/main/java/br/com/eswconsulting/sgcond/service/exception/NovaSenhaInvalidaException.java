/**
 * 
 */
package br.com.eswconsulting.sgcond.service.exception;

/**
 * @author edsf
 *
 */
public class NovaSenhaInvalidaException extends BusinessException {

	public NovaSenhaInvalidaException(String mensagem) {
		super(mensagem);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5885110638527812797L;

}
