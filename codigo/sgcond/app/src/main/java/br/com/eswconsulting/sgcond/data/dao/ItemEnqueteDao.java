package br.com.eswconsulting.sgcond.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eswconsulting.sgcond.domain.entity.ItemEnquete;

public interface ItemEnqueteDao extends JpaRepository<ItemEnquete, Long> {

	
}
