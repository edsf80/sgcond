/**
 * 
 */
package br.com.eswconsulting.sgcond.service.exception;

/**
 * @author edsf
 *
 */
public class VotoInvalidoException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3563218837092310655L;
	
	public VotoInvalidoException(String mensagem) {
		super(mensagem);
	}

}
