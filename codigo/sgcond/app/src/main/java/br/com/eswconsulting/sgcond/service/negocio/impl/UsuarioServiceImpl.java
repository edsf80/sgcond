/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio.impl;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.eswconsulting.sgcond.data.dao.UsuarioDao;
import br.com.eswconsulting.sgcond.domain.entity.Papel;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.service.exception.NovaSenhaInvalidaException;
import br.com.eswconsulting.sgcond.service.negocio.UsuarioService;

/**
 * @author edsf
 *
 */
@Service
@Qualifier("usuarioServiceImpl")
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UsuarioDao usuarioDao;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PasswordEncoder passwordEncoder;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.UsuarioService#executarLogin(
	 * java.lang.String, java.lang.String)
	 */
	public Optional<Usuario> executarLogin(String username, String senha) {

		Optional<Authentication> authentication = this.criarAutenticacao(username, senha);

		if (!authentication.isPresent()) {
			return Optional.empty();
		}

		SecurityContextHolder.getContext().setAuthentication(authentication.get());

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.get().getPrincipal();
		
		// if (userDetails != null) {
		Usuario usuario = new Usuario();
		usuario.setId(userDetails.getId());
		usuario.setNome(userDetails.getNome());
		usuario.setUsername(userDetails.getUsername());
		usuario.setMudarSenha(userDetails.getMudarSenha());
		Set<Papel> papeis = new HashSet<>();
		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			Papel papel = new Papel();
			papel.setCodigo(authority.getAuthority());
			papeis.add(papel);
		}
		usuario.setPapeis(papeis);
		// }

		return Optional.of(usuario);
	}

	/**
	 * @param login
	 * @param senha
	 * @return
	 */
	private Optional<Authentication> criarAutenticacao(String login, String senha) {

		Optional<Authentication> authentication = Optional.empty();

		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, senha);

			logger.debug("Criando autenticação do spring security");
			authentication = Optional.ofNullable(this.authenticationManager.authenticate(token));

		} catch (BadCredentialsException bce) {
			logger.debug("Falha na criação da autenticação no spring security");
			return Optional.empty();
		}

		return authentication;
	}	

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.UsuarioService#salvar(br.com.
	 * eswconsulting.sgcond.domain.entity.Usuario)
	 */
	@Override
	@Transactional
	public Usuario atualizarPerfil(@NotNull Usuario user, String senhaAtual) throws NovaSenhaInvalidaException {

		Optional<Usuario> usuario = usuarioDao.findById(user.getId());

		if (user.getNome() != null && !user.getNome().isEmpty()) {
			usuario.get().setNome(user.getNome());
		}

		if (user.getUsername() != null && !user.getUsername().isEmpty()) {
			usuario.get().setUsername(user.getUsername());
		}

		if (user.getSenha() != null && !user.getSenha().isEmpty()) {
			if (passwordEncoder.matches(user.getSenha(), usuario.get().getSenha())) {
				throw new NovaSenhaInvalidaException("Nova senha não deve ser igual a senha atual");
			} else if (!passwordEncoder.matches(senhaAtual, usuario.get().getSenha())) {
				throw new NovaSenhaInvalidaException("Senha atual informada inválida");
			}
			usuario.get().setSenha(passwordEncoder.encode(user.getSenha()));
		}

		return usuarioDao.save(usuario.get());
	}
}
