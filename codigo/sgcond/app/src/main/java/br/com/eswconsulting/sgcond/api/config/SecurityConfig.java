package br.com.eswconsulting.sgcond.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

	@Configuration
	public static class ApiWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {

		@Autowired
		private UserDetailsService userDetailsService;

		/*
		 * @Autowired private JWTAuthorizationFilter jwtFilter;
		 * 
		 * @Autowired private JwtAuthenticationEntryPoint unauthorizedHandler;
		 */

		@Bean
		public PasswordEncoder passwordEncoder() {
			PasswordEncoder encoder = new BCryptPasswordEncoder();
			return encoder;
		}

		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.springframework.security.config.annotation.web.configuration.
		 * WebSecurityConfigurerAdapter#authenticationManagerBean()
		 */
		@Override
		@Bean
		public AuthenticationManager authenticationManagerBean() throws Exception {
			// tirado de
			// https://stackoverflow.com/questions/45391264/spring-boot-2-and-oauth2-jwt-configuration
			// Sem isso o AuthenticationManager não era injetado a partir do spring boot
			// 2.0.
			return super.authenticationManagerBean();
		}

		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED).and()
					.authorizeRequests().antMatchers("/api/login").permitAll().antMatchers("/api/**").authenticated()
					.anyRequest().permitAll().and().exceptionHandling();
		}
	}
}
