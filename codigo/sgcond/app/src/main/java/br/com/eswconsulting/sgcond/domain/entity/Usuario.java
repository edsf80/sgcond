/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = { "id" })
@Entity
public class Usuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 53019769733934419L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usr", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@NotNull(message = "Nome de usuário deve ser informado")
	@Column(name = "unm_usr")
	private String username;

	/**
	 * 
	 */
	@NotNull(message = "Nome do usuário deve ser informado")
	@Column(name = "nme_usr")
	private String nome;

	/**
	 * 
	 */
	@NotNull(message = "Senha do usuário deve ser informada")
	@Column(name = "sna_usr")
	private String senha;

	/**
	 * Atributo para guardar papeis do usuário e evitar a criação de um objeto VO.
	 */
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "usuario_papel", joinColumns = @JoinColumn(name = "id_usr", referencedColumnName = "id_usr"), inverseJoinColumns = @JoinColumn(name = "id_ppl", referencedColumnName = "id_ppl"))
	private Set<Papel> papeis;

	/**
	 * 
	 */
	@Column(name = "msn_usr")
	private Boolean mudarSenha = false;
	
	public Usuario(Long id, String username, String nome, String senha, Boolean mudarSenha) {
		this.id = id;
		this.username = username;
		this.nome = nome;
		this.senha = senha;
		this.mudarSenha = mudarSenha;
	}
}