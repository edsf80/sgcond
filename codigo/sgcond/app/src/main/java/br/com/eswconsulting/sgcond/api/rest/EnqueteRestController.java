/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.eswconsulting.sgcond.data.dao.EnqueteDao;
import br.com.eswconsulting.sgcond.data.dao.VotoDao;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.Enquete;
import br.com.eswconsulting.sgcond.domain.entity.ItemEnquete;
import br.com.eswconsulting.sgcond.domain.entity.Voto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/enquete")
public class EnqueteRestController {

	@Autowired
	private EnqueteDao enqueteDao;

	@Autowired
	private VotoDao votoDao;

	@Autowired
	private ModelMapper modelMapper;

	@Transactional
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody EnqueteRest criar(@RequestBody @Valid EnqueteRest enqueteRest) {

		Enquete enquete = this.convertToEntity(enqueteRest);

		enquete = enqueteDao.save(enquete);

		return this.convertToDTO(enquete);
	}

	@Transactional
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody EnqueteRest alterar(@RequestBody @Valid EnqueteRest enqueteRest) {
		Enquete enquete = this.convertToEntity(enqueteRest);

		enquete = enqueteDao.save(enquete);

		return this.convertToDTO(enquete);
	}

	@RequestMapping(value = "/status/aberta", method = RequestMethod.GET)
	public @ResponseBody List<EnqueteRest> buscarTodasAbertas() {
		return enqueteDao.findTodasAbertas().stream().map(enquete -> this.convertToDTO(enquete))
				.collect(Collectors.toList());
	}

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody List<EnqueteRest> buscarTodos() {
		return enqueteDao.findTodasSemRelacionamentos().stream().map(enquete -> this.convertToDTO(enquete))
				.collect(Collectors.toList());
	}

	@RequestMapping(value = "/{id}")
	public ResponseEntity<EnqueteRest> buscarPorId(@PathVariable Long id) {

		ResponseEntity<EnqueteRest> resultado = null;

		Optional<Enquete> enquete = enqueteDao.findById(id);

		if (enquete.isPresent()) {
			EnqueteRest enqueteRest = this.convertToDTO(enquete.get());
			resultado = new ResponseEntity<>(enqueteRest, HttpStatus.OK);
		} else {
			resultado = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	@GetMapping("/resultado/{id}")
	public ResponseEntity<List<VotoRest>> buscarResultadoEnquete(@PathVariable Long id) {
		ResponseEntity<List<VotoRest>> resultado = null;

		Set<Voto> votos = this.votoDao.findByEnqueteId(id);

		List<VotoRest> votosRest = votos.stream().map(voto -> this.modelMapper.map(voto, VotoRest.class))
				.collect(Collectors.toList());
		resultado = new ResponseEntity<>(votosRest, HttpStatus.OK);
		
		return resultado;
	}

	private Enquete convertToEntity(EnqueteRest enqueteRest) {
		if (enqueteRest == null) {
			return null;
		}

		Enquete enquete = new Enquete();
		enquete.setId(enqueteRest.getId());
		enquete.setStatus(Enquete.StatusEnquete.valueOf(enqueteRest.status.toUpperCase()));
		enquete.setTitulo(enqueteRest.getTitulo());
		Condominio condominio = new Condominio();
		condominio.setId(enqueteRest.getIdCondominio());
		enquete.setCondominio(condominio);
		enqueteRest.getItens().stream().forEach(item -> {
			ItemEnquete itemEnquete = new ItemEnquete();
			itemEnquete.setDescricao(item.getDescricao());
			itemEnquete.setId(item.getId());
			enquete.addItemEnquete(itemEnquete);
		});

		return enquete;
	}

	/**
	 * @param enquete
	 * @return
	 */
	private EnqueteRest convertToDTO(Enquete enquete) {
		if (enquete == null) {
			return null;
		}

		EnqueteRest enqueteRest = new EnqueteRest();
		enqueteRest.setId(enquete.getId());
		enqueteRest.setTitulo(enquete.getTitulo());
		enqueteRest.setStatus(enquete.getStatus().toString());
		Condominio condominio = enquete.getCondominio();
		enqueteRest.setIdCondominio(condominio == null ? null : condominio.getId());

		enquete.getItens().stream().forEach(itemEnquete -> {
			ItemEnqueteRest itemEnqueteRest = new ItemEnqueteRest();
			itemEnqueteRest.setId(itemEnquete.getId());
			itemEnqueteRest.setDescricao(itemEnquete.getDescricao());

			/*
			 * itemEnquete.getVotos().stream().forEach(voto -> { VotoRest votoRest = new
			 * VotoRest(); votoRest.setComentario(voto.getComentario());
			 * votoRest.setUnidade(voto.getUnidade().getIdentificador());
			 * itemEnqueteRest.addVoto(votoRest); });
			 */

			enqueteRest.addItem(itemEnqueteRest);
		});

		return enqueteRest;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class VotoRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1670592707008140496L;

		/**
		 * 
		 */
		private UnidadeRest unidade;

		/**
		 * 
		 */
		private ItemEnqueteRest itemEnquete;

		/**
		 * 
		 */
		private String comentario;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class UnidadeRest implements Serializable {

		/*
		 * 
		 */
		private static final long serialVersionUID = -1725831414075754018L;

		private String identificador;

	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class ItemEnqueteRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -1940757683650860747L;

		/**
		 * 
		 */
		private Long id;

		/**
		 * 
		 */
		@NotNull(message = "A descrição do item deve ser informada")
		private String descricao;

		/**
		 * 
		 */
		// private List<VotoRest> votos = new ArrayList<>();

		/**
		 * @param votoRest
		 */
		/*
		 * public void addVoto(VotoRest votoRest) { this.votos.add(votoRest); }
		 */
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class EnqueteRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -408183349608907118L;

		private Long id;

		@NotNull(message = "Titúlo da enquete é obrigatório")
		private String titulo;

		@NotNull(message = "Status da enquete é obrigatório")
		private String status;

		@Valid
		@NotEmpty(message = "Enquete deve possuir itens")
		@NotNull(message = "Enquete deve possuir itens")
		private Set<ItemEnqueteRest> itens = new HashSet<>();

		@NotNull(message = "Enquete não associa a um condomínio")
		private Long idCondominio;

		/**
		 * @param item
		 */
		public void addItem(ItemEnqueteRest item) {
			this.itens.add(item);
		}
	}
}
