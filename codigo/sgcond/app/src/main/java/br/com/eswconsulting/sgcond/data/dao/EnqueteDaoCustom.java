package br.com.eswconsulting.sgcond.data.dao;

import java.util.List;

import br.com.eswconsulting.sgcond.domain.entity.Enquete;

public interface EnqueteDaoCustom {
	
	List<Enquete> findTodasAbertas();

	List<Object[]> findTodasAbertasSemVotoPorUsuario(Long idUsuario);
}
