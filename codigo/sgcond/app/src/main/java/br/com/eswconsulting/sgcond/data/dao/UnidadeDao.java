/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.eswconsulting.sgcond.domain.entity.Unidade;

/**
 * @author edsf
 *
 */
public interface UnidadeDao extends JpaRepository<Unidade, Long> {

	/**
	 * @param id
	 * @return
	 */
	Set<Unidade> findByUsuarioId(Long id);

	/**
	 * @param identificador
	 * @param idCondominio
	 * @return
	 */
	Optional<Unidade> findByIdentificadorAndCondominioId(String identificador, Long idCondominio);

}
