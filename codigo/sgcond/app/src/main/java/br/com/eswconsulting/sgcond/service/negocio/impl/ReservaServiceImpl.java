package br.com.eswconsulting.sgcond.service.negocio.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.eswconsulting.sgcond.data.dao.ReservaDao;
import br.com.eswconsulting.sgcond.domain.entity.Reserva;
import br.com.eswconsulting.sgcond.service.exception.ReservaInvalidaException;
import br.com.eswconsulting.sgcond.service.exception.ReservaNaoEncontradaException;
import br.com.eswconsulting.sgcond.service.negocio.ReservaService;

@Service
public class ReservaServiceImpl implements ReservaService {

	/**
	 * 
	 */
	@Autowired
	private ReservaDao reservaDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.eswconsulting.sgcond.service.negocio.ReservaService#findAll()
	 */
	@Override
	public List<Reserva> findAll() {

		return StreamSupport.stream(reservaDao.findAll().spliterator(), false).collect(Collectors.toList());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ReservaService#create(br.com.
	 * eswconsulting.sgcond.domain.entity.Reserva)
	 */
	@Override
	@Transactional
	@PreAuthorize("hasAnyRole('SINDICO','MORADOR')")
	public Reserva create(@Valid Reserva reserva) throws ReservaInvalidaException {

		// Verificar se a hora do início do evento é menor que a hora do término, do
		// contrário está inválido.
		// Verificar também se o evento está iniciando depois da hora atual.
		if (!this.validateDateRange(reserva.getDataHoraInicio(), reserva.getDataHoraTermino())) {
			throw new ReservaInvalidaException(
					"Início da reserva após o término ou tentativa de reserva em data passada.");
		}

		// TODO: Não deixar o usuário reservar uma área comum de um condomínio onde ele
		// não mora.
		// Na inserção não precisa verificar o id da reserva uma vez que ela não existe.
		List<Reserva> reservas = reservaDao.findColision(null, reserva.getAreaComum().getId(),
				reserva.getDataHoraInicio(), reserva.getDataHoraTermino());

		// Verificar se não existe um evento no período solicitado.
		if (!reservas.isEmpty()) {
			throw new ReservaInvalidaException("Já existe uma reserva no período solicitado");
		}

		return reservaDao.save(reserva);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ReservaService#findByDate(java.
	 * util.Date)
	 */
	@PreAuthorize("hasAnyRole('SINDICO','MORADOR')")
	public List<Reserva> findByDate(Date data) {
		List<Reserva> reservas = reservaDao.findByData(data);

		return reservas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ReservaService#findOne(java.lang.
	 * Long)
	 */
	@Override
	public Optional<Reserva> findOne(@NotNull Long id) {

		return reservaDao.findById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ReservaService#modify(br.com.
	 * eswconsulting.sgcond.domain.entity.Reserva)
	 */
	@Override
	@PreAuthorize("hasAnyRole('SINDICO','MORADOR')")
	@Transactional
	public Reserva modify(@Valid Reserva reserva) throws ReservaInvalidaException, ReservaNaoEncontradaException {

		Optional<Reserva> reservaAtual = this.reservaDao.findById(reserva.getId());

		if (!reservaAtual.isPresent()) {
			throw new ReservaNaoEncontradaException("Tentativa de alterar reserva inexistente");
		}

		// Verificar se a hora do início do evento é menor que a hora do término, do
		// contrário está inválido.
		// Verificar também se o evento está iniciando depois da hora atual.
		if (!this.validateDateRange(reserva.getDataHoraInicio(), reserva.getDataHoraTermino())) {
			throw new ReservaInvalidaException(
					"Início da reserva após o término ou tentativa de reserva em data passada.");
		}

		// TODO: Não deixar o usuário reservar uma área comum de um condomínio onde ele
		// não mora.
		List<Reserva> reservas = reservaDao.findColision(reserva.getId(), reserva.getAreaComum().getId(),
				reserva.getDataHoraInicio(), reserva.getDataHoraTermino());

		if (!reservas.isEmpty()) {
			throw new ReservaInvalidaException("Já existe uma reserva no período solicitado");
		}

		UserDetailsImpl usuario = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		// Aqui é verificado se o usuário logado pode alterar a reserva. A reserva só
		// pode ser alterada pelo usuário que reservou.
		boolean podeAlterar = reserva.getDono().getId().equals(usuario.getId());

		if (!podeAlterar) {
			throw new ReservaInvalidaException("Apenas o dono da reserva pode alterá-la");
		}

		return reservaDao.save(reserva);
	}

	@Override
	@Transactional
	@PreAuthorize("hasAnyRole('SINDICO','MORADOR')")
	public void remove(@NotNull Long id) throws ReservaInvalidaException, ReservaNaoEncontradaException {
		Optional<Reserva> reserva = this.reservaDao.findById(id);

		if (!reserva.isPresent()) {
			throw new ReservaNaoEncontradaException("Tentativa de exclusão de reserva inexistente");
		}

		UserDetailsImpl usuario = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		// Aqui é verificado se o usuário logado pode apagar a reserva. A reserva só
		// pode ser apagada pelo usuário que reservou se o evento ainda não aconteceu.
		boolean podeExcluir = reserva.get().getDono().getId().equals(usuario.getId());		

		if (!podeExcluir) {
			throw new ReservaInvalidaException("Apenas o dono da reserva pode apagá-la");
		}

		if (reserva.get().getDataHoraInicio().before(new Date())) {
			throw new ReservaInvalidaException("Não é possível excluir reserva passada");
		}

		reservaDao.delete(reserva.get());
	}

	// Valida o periodo de inicio e termino da reserva. Reservas anteriores a data
	// atual não podem ser feitas,
	// assim como reservas onde o inicio é após o término.
	private boolean validateDateRange(Date start, Date end) {
		return start.before(end) && start.after(new Date());
	}

}
