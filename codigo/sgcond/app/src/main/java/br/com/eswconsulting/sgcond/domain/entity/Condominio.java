/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Condominio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7342903988740032322L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cno", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@NotNull(message = "Nome do condomínio deve ser informado")
	@Column(name = "nme_cno")
	private String nome;

	/**
	 * 
	 */
	@NotNull(message = "CNPJ do condomínio deve ser informado")
	@Column(name = "cnpj_cno")
	private String cnpj;	
}
