/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.eswconsulting.sgcond.domain.entity.Chamado;

/**
 * @author edsf
 *
 */
public interface ChamadoDao extends CrudRepository<Chamado, Long> {

	/**
	 * @param condominio
	 * @return
	 */
	List<Chamado> findByCondominioId(Long id);

	/**
	 * @param date
	 * @param id
	 * @return
	 */
	List<Chamado> findTop5ByCondominioIdOrderByDataCriacaoDesc(Long id);
}
