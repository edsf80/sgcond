/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import br.com.eswconsulting.sgcond.data.dao.EnqueteDao;
import br.com.eswconsulting.sgcond.data.dao.UnidadeDao;
import br.com.eswconsulting.sgcond.data.dao.VotoDao;
import br.com.eswconsulting.sgcond.domain.entity.Enquete;
import br.com.eswconsulting.sgcond.domain.entity.ItemEnquete;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.domain.entity.Voto;
import br.com.eswconsulting.sgcond.service.exception.VotoInvalidoException;
import br.com.eswconsulting.sgcond.service.negocio.DashboardService;

/**
 * @author edsf
 *
 */
@Service
@Validated
public class DashboardServiceImpl implements DashboardService {

	/**
	 * 
	 */
	@Autowired
	private EnqueteDao enqueteDao;

	@Autowired
	private UnidadeDao unidadeDao;
	
	@Autowired
	private VotoDao votoDao;

	@Override
	public List<Enquete> buscarTodasAbertas() {
		return this.enqueteDao.findTodasAbertas();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.eswconsulting.sgcond.service.negocio.DashboardService#
	 * buscarEnquetesSemVoto()
	 */
	@Override
	public List<Enquete> buscarEnquetesSemVoto() {
		List<Enquete> enquetes = this.enqueteDao.findTodasAbertas();

		UserDetailsImpl usuario = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		// Aqui são filtradas as enquetes que não tem voto para que o usuário
		// possa votar.
		List<Enquete> enquetesFiltradas = null;/*enquetes.stream()
				.filter(enquete -> enquete.getItens().stream()
						.filter(item -> !item.getVotos().stream()
								.filter(voto -> voto.getUnidade().getUsuario().getId().equals(usuario.getId()))
								.collect(Collectors.toList()).isEmpty())
						.collect(Collectors.toList()).isEmpty())
				.collect(Collectors.toList());*/

		return enquetesFiltradas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.DashboardService#votarEnquete
	 * (java.lang.Long)
	 */
	@Override
	@Transactional
	public Enquete votarEnquete(@NotNull @Valid Voto voto) throws VotoInvalidoException {
		
		UserDetailsImpl usuario = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();
		
		Optional<Enquete> enquete = this.enqueteDao.findById(voto.getEnquete().getId());
		
		if (!enquete.isPresent() || enquete.get().getStatus() == Enquete.StatusEnquete.FECHADA) {
			throw new VotoInvalidoException("Tentativa de votar em enquete fechada ou inexistente");
		}		

		Optional<Unidade> unidade = this.unidadeDao.findById(voto.getUnidade().getId());

		// O trecho abaixo verifica se o usuário está tentando votar em uma unidade
		// que pertence a ele para evitar fraudes.
		if (!unidade.isPresent() || !unidade.get().getUsuario().getId().equals(usuario.getId())) {
			throw new VotoInvalidoException(
					"Tentativa de votar por uma unidade não pertencente ao usuário ou inválida");
		}

		List<ItemEnquete> itens = enquete.get().getItens().stream()
				.filter(item -> item.getId().equals(voto.getItemEnquete().getId())).collect(Collectors.toList());

		if (itens.isEmpty()) {
			throw new VotoInvalidoException("Tentativa de votar em item de enquete inválida");
		}

		this.votoDao.save(voto);

		return enquete.get();
	}

}
