/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.eswconsulting.sgcond.domain.entity.Enquete;

/**
 * @author edsf
 *
 */
public interface EnqueteDao extends CrudRepository<Enquete, Long>, EnqueteDaoCustom {
			
	@Query("select NEW Enquete(e.id, e.titulo, e.status) from Enquete e")
	List<Enquete> findTodasSemRelacionamentos();
}
