/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.eswconsulting.sgcond.data.dao.AvisoDao;
import br.com.eswconsulting.sgcond.data.dao.EnqueteDao;
import br.com.eswconsulting.sgcond.data.dao.ReservaDao;
import br.com.eswconsulting.sgcond.domain.entity.Aviso;
import br.com.eswconsulting.sgcond.domain.entity.Chamado;
import br.com.eswconsulting.sgcond.domain.entity.Enquete;
import br.com.eswconsulting.sgcond.domain.entity.ItemEnquete;
import br.com.eswconsulting.sgcond.domain.entity.Reserva;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.domain.entity.Voto;
import br.com.eswconsulting.sgcond.service.exception.VotoInvalidoException;
import br.com.eswconsulting.sgcond.service.negocio.DashboardService;
import br.com.eswconsulting.sgcond.service.negocio.impl.UserDetailsImpl;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/dashboard")
public class DashboardRestController {

	/**
	 * 
	 */
	private DashboardService dashboardService;

	private EnqueteDao enqueteDao;

	private ReservaDao reservaDao;

	private AvisoDao avisoDao;

	private ModelMapper modelMapper;

	@Autowired
	public DashboardRestController(DashboardService dashboardService, EnqueteDao enqueteDao, ReservaDao reservaDao,
			AvisoDao avisoDao, ModelMapper modelMapper) {
		this.dashboardService = dashboardService;
		this.enqueteDao = enqueteDao;
		this.reservaDao = reservaDao;
		this.avisoDao = avisoDao;
		this.modelMapper = modelMapper;
	}

	/**
	 * @return
	 */
	@GetMapping
	public @ResponseBody DashboardRest buscarDadosDashboard() {

		List<Enquete> enquetes = this.enqueteDao.findTodasAbertas();

		UserDetailsImpl usuario = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		List<Object[]> enquetesNaoVotadas = this.enqueteDao.findTodasAbertasSemVotoPorUsuario(usuario.getId());

		DashboardRest dbRest = new DashboardRest();

		dbRest.setEnquetes(
				enquetes.stream().map(enquete -> convertToEnqueteRest(enquete)).collect(Collectors.toList()));
		dbRest.setEnquetesNaoVotadas(
				enquetesNaoVotadas.stream().map(enquete -> convertToEnqueteRest(enquete)).collect(Collectors.toList()));

		Calendar inicio = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		Calendar termino = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR),
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		termino.add(Calendar.HOUR, 24);

		List<Reserva> reservas = this.reservaDao.findByDataHoraInicioBetween(inicio.getTime(), termino.getTime());

		dbRest.setReservas(reservas.stream().map(reserva -> {
			return new ReservaRest(reserva.getAreaComum().getDescricao(), reserva.getDataHoraInicio(),
					reserva.getDataHoraTermino(), reserva.getDono().getNome());
		}).collect(Collectors.toList()));

		List<Aviso> avisos = this.avisoDao.findByValidadeGreaterThanEqual(inicio.getTime());

		dbRest.setAvisos(avisos.stream().map(aviso -> {
			return new AvisoRest(aviso.getTitulo(), aviso.getDescricao());
		}).collect(Collectors.toList()));

		return dbRest;
	}

	/**
	 * @param votoRest
	 * @throws VotoInvalidoException
	 */
	@PreAuthorize("hasAnyRole('ROLE_MORADOR', 'ROLE_SINDICO')")
	@PostMapping("/voto/{idEnquete}")
	public @ResponseBody ResponseEntity<EnqueteRest> votarEnquete(@RequestBody @Valid VotoRest votoRest,
			@PathVariable @NotNull Long idEnquete) throws VotoInvalidoException {
		Voto voto = new Voto();
		Enquete enquete = new Enquete();
		enquete.setId(idEnquete);

		voto.setEnquete(enquete);

		ItemEnquete itemEnquete = new ItemEnquete();
		itemEnquete.setId(votoRest.getIdItemEnquete());
		voto.setItemEnquete(itemEnquete);

		Unidade unidade = new Unidade();
		unidade.setId(votoRest.getIdUnidade());
		voto.setUnidade(unidade);

		voto.setComentario(votoRest.getComentario());

		Enquete enqueteResultante = dashboardService.votarEnquete(voto);
		ResponseEntity<EnqueteRest> resultado = null;

		resultado = new ResponseEntity<>(this.convertToEnqueteRest(enqueteResultante), HttpStatus.OK);

		return resultado;
	}

	private EnqueteRest convertToEnqueteRest(Object[] objects) {
		EnqueteRest resultado = this.modelMapper.map(objects[0], EnqueteRest.class);
		resultado.setUnidade(this.modelMapper.map(objects[1], UnidadeRest.class));

		return resultado;
	}

	/**
	 * @param enquete
	 * @return
	 */
	private EnqueteRest convertToEnqueteRest(Enquete enquete) {
		if (enquete == null) {
			return null;
		}

		return this.modelMapper.map(enquete, EnqueteRest.class);
	}

	@Data
	@NoArgsConstructor
	static class DashboardRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5139074139757413019L;

		/**
		 * 
		 */
		private List<EnqueteRest> enquetes;

		/**
		 * 
		 */
		private List<EnqueteRest> enquetesNaoVotadas;

		/**
		 * 
		 */
		private List<Chamado> chamados;

		/**
		 * 
		 */
		private List<ReservaRest> reservas;

		/**
		 * 
		 */
		private List<AvisoRest> avisos;
	}

	@Data
	@NoArgsConstructor
	static class EnqueteRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8499769752431512295L;

		private Long id;

		private String titulo;

		private String status;

		private UnidadeRest unidade;

		private Set<ItemEnqueteRest> itens;
	}

	@Data
	@NoArgsConstructor
	static class ItemEnqueteRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3090281508712162922L;

		/**
		 * 
		 */
		private Long id;

		/**
		 * 
		 */
		private String descricao;
	}

	@Data
	@NoArgsConstructor
	protected static class VotoRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7780859145121319282L;

		/**
		 * 
		 */
		@NotNull
		private Long idItemEnquete;

		/**
		 * 
		 */
		@NotNull
		private Long idUnidade;

		/**
		 * 
		 */
		private String comentario;
	}

	@Data
	@NoArgsConstructor
	static class UnidadeRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -7571900992828303949L;

		@NotNull
		private Long id;

		private String identificador;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class ReservaRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 291809057057138810L;

		private String area;

		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss", timezone = "America/Recife")
		private Date inicio;

		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss", timezone = "America/Recife")
		private Date termino;

		private String dono;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class AvisoRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3497214282423706858L;

		private String titulo;

		private String descricao;

	}
}
