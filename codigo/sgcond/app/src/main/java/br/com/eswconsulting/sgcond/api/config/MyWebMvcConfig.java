/**
 * 
 */
package br.com.eswconsulting.sgcond.api.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author edsf
 *
 */
@Configuration
public class MyWebMvcConfig {	
	
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
