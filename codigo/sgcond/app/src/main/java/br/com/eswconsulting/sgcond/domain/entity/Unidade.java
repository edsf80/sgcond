/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
public class Unidade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3231797195253811179L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_und", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@NotNull(message = "Número do apartamento deve ser informado")
	@Column(name = "idt_und")
	private String identificador;

	/**
	 * 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usr", nullable = false)
	private Usuario usuario;	

	/**
	 * 
	 */
	@ManyToOne
	@JoinColumn(name = "id_cno", nullable = false)
	private Condominio condominio;
}
