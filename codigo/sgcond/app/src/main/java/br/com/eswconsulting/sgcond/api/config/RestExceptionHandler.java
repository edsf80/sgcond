/**
 * 
 */
package br.com.eswconsulting.sgcond.api.config;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import br.com.eswconsulting.sgcond.service.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@ControllerAdvice // ("br.com.eswconsulting.sgcond.api.rest")
public class RestExceptionHandler {

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
			WebRequest request) {
		String error = ex.getName() + " deve ser do tipo " + ex.getRequiredType().getName();

		return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	public ResponseEntity<ValidationError> handleErroValidacao(HttpServletRequest req,
			MethodArgumentNotValidException manvex) {

		BindingResult result = manvex.getBindingResult();
		List<FieldError> fieldErrors = result.getFieldErrors();
		ValidationError validationError = new ValidationError();

		for (FieldError fieldError : fieldErrors) {
			validationError.addFieldError(fieldError.getField(), fieldError.getCode());
		}

		return new ResponseEntity<ValidationError>(validationError, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(BusinessException.class)
	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
	@ResponseBody
	public ResponseEntity<String> handleErroNegocio(HttpServletRequest req, BusinessException be) {

		ResponseEntity<String> responseEntity = new ResponseEntity<>(be.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);

		return responseEntity;
	}

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	@ResponseBody
	public ResponseEntity<String> handleAcessoNegado(HttpServletRequest req, AccessDeniedException ade) {

		ResponseEntity<String> responseEntity = new ResponseEntity<>(ade.getMessage(), HttpStatus.FORBIDDEN);

		return responseEntity;
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<String> handleErroServidor(HttpServletRequest req, Exception e) {
		e.printStackTrace();
		ResponseEntity<String> responseEntity = new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

		return responseEntity;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class InvalidField implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4334009978725056328L;

		private String field;

		private String message;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class ValidationError implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6127215226785846457L;

		private List<InvalidField> fieldErrors = new ArrayList<>();

		public void addFieldError(String path, String message) {
			InvalidField error = new InvalidField(path, message);
			fieldErrors.add(error);
		}
	}
}