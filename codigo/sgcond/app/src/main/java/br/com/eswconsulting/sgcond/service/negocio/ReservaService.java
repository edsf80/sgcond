/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import br.com.eswconsulting.sgcond.domain.entity.Reserva;
import br.com.eswconsulting.sgcond.service.exception.ReservaInvalidaException;
import br.com.eswconsulting.sgcond.service.exception.ReservaNaoEncontradaException;

/**
 * @author edsf
 *
 */
public interface ReservaService {

	/**
	 * @return
	 */
	List<Reserva> findAll();

	/**
	 * @param data
	 * @return
	 */
	List<Reserva> findByDate(Date data);

	/**
	 * @param reserva
	 * @return
	 * @throws ReservaInvalidaException
	 */
	Reserva create(Reserva reserva) throws ReservaInvalidaException;

	/**
	 * @param reserva
	 * @return
	 * @throws ReservaInvalidaException
	 * @throws ReservaNaoEncontradaException 
	 */
	Reserva modify(Reserva reserva) throws ReservaInvalidaException, ReservaNaoEncontradaException;

	/**
	 * @param id
	 * @return
	 */
	Optional<Reserva> findOne(Long id);

	/**
	 * Método para remover uma reserva.
	 * 
	 * @param id
	 *            O identificador da reserva. O mesmo não pode ser null.
	 * @throws ReservaInvalidaException
	 *             Lançada nos casos em que o usuário não é dono da reserva ou
	 *             quando na tentativa de excluir reserva cujo evento já iniciou.
	 * @throws ReservaNaoEncontradaException
	 *             Lançada quando na tentativa de se remover uma reserva que não
	 *             existe.
	 */
	void remove(Long id) throws ReservaInvalidaException, ReservaNaoEncontradaException;

}
