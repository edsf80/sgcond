/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.eswconsulting.sgcond.data.dao.AvisoDao;
import br.com.eswconsulting.sgcond.domain.entity.Aviso;
import br.com.eswconsulting.sgcond.service.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping("/aviso")
public class AvisoRestController {

	/**
	 * 
	 */
	private AvisoDao avisoDao;

	/**
	 * 
	 */
	private ModelMapper modelMapper;

	/**
	 * 
	 */
	@Autowired
	public AvisoRestController(AvisoDao avisoDao, ModelMapper modelMapper) {
		this.avisoDao = avisoDao;
		this.modelMapper = modelMapper;
	}

	/**
	 * @return
	 */
	@GetMapping
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	public @ResponseBody List<AvisoRest> buscarTodas() {
		return StreamSupport.stream(this.avisoDao.findAll().spliterator(), false)
				.map(unidade -> this.modelMapper.map(unidade, AvisoRest.class)).collect(Collectors.toList());
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public @ResponseBody ResponseEntity<AvisoRest> buscarPorId(@NotNull @PathVariable Long id) {
		Optional<Aviso> avisoOpt = this.avisoDao.findById(id);
		ResponseEntity<AvisoRest> resultado;

		if (avisoOpt.isPresent()) {
			resultado = new ResponseEntity<>(this.modelMapper.map(avisoOpt.get(), AvisoRest.class), HttpStatus.OK);
		} else {
			resultado = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	/**
	 * @param avisoRest
	 * @return
	 */
	@PostMapping
	@Transactional
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	public @ResponseBody ResponseEntity<AvisoRest> inserir(@NotNull @Valid @RequestBody AvisoRest avisoRest) {

		// TODO: tirar quando tiver mais de um condominio.
		avisoRest.setCondominio(new CondominioRest(1l));

		Aviso aviso = this.avisoDao.save(this.modelMapper.map(avisoRest, Aviso.class));

		return new ResponseEntity<AvisoRest>(this.modelMapper.map(aviso, AvisoRest.class), HttpStatus.CREATED);
	}

	/**
	 * @param id
	 * @param avisoRest
	 * @return
	 * @throws BusinessException
	 */
	@PutMapping("/{id}")
	@Transactional
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	public @ResponseBody ResponseEntity<AvisoRest> alterar(@NotNull @PathVariable Long id,
			@Valid @RequestBody AvisoRest avisoRest) throws BusinessException {
		avisoRest.setCondominio(new CondominioRest(1l));

		if (avisoRest.getId() == null) {
			return new ResponseEntity<AvisoRest>(HttpStatus.UNPROCESSABLE_ENTITY);
		}

		// Verifica se está tentando atualizar um aviso inexistente.
		Optional<Aviso> avisoOpt = this.avisoDao.findById(id);

		if (!avisoOpt.isPresent()) {
			return new ResponseEntity<AvisoRest>(HttpStatus.NOT_FOUND);
		}

		Aviso aviso = this.modelMapper.map(avisoRest, Aviso.class);

		aviso = this.avisoDao.save(aviso);

		return new ResponseEntity<AvisoRest>(this.modelMapper.map(aviso, AvisoRest.class), HttpStatus.OK);
	}

	@Data
	public static class AvisoRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -4517568869804285258L;

		private Long id;

		@NotNull(message = "Título do aviso deve ser informado")
		private String titulo;

		@NotNull(message = "Descrição do aviso deve ser informada")
		private String descricao;

		@NotNull(message = "Validade do chamado deve ser informada")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Brazil/East")
		private Date validade;

		private CondominioRest condominio;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class CondominioRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4588775236882950408L;

		private Long id;
	}
}
