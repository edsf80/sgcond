/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Usuario")
public class UsuarioSemRel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1728063182391555858L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usr", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@NotNull(message = "Nome de usuário deve ser informado")
	@Column(name = "unm_usr")
	private String username;

	/**
	 * 
	 */
	@NotNull(message = "Nome do usuário deve ser informado")
	@Column(name = "nme_usr")
	private String nome;
}
