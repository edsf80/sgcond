/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
public class Enquete implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3112713336007267430L;

	public enum StatusEnquete {
		ABERTA, FECHADA
	}

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_eqt", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@Column(name = "tit_eqt", nullable = false)
	@NotNull(message = "Título da enquete deve ser informado")
	private String titulo;

	/**
	 * 
	 */
	@ManyToOne
	@JoinColumn(name = "id_cno", nullable = false)
	private Condominio condominio;

	/**
	 * 
	 */
	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy="enquete")
	private Set<ItemEnquete> itens = new HashSet<>();

	/**
	 * 
	 */
	@Column(name = "dti_eqt")
	private Date dataInicio;

	/**
	 * 
	 */
	@Column(name = "dtf_eqt")
	private Date dataFim;

	/**
	 * 
	 */
	@Column(name = "sts_eqt")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Status da enquete deve ser informado")
	private StatusEnquete status;
	
	public Enquete(Long id, String titulo, StatusEnquete status) {
		this.id = id;
		this.titulo = titulo;
		this.status = status;
	}
	
	/**
	 * @param itemEnquete
	 */
	public void addItemEnquete(ItemEnquete itemEnquete) {
		itemEnquete.setEnquete(this);
		this.itens.add(itemEnquete);
	}
}
