/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import br.com.eswconsulting.sgcond.data.dao.ChamadoDao;
import br.com.eswconsulting.sgcond.domain.entity.Chamado;
import br.com.eswconsulting.sgcond.service.negocio.ChamadoService;

/**
 * @author edsf
 *
 */
@Service
public class ChamadoServiceImpl implements ChamadoService {

	/**
	 * 
	 */
	@Autowired
	private ChamadoDao chamadoDao;

	/**
	 * 
	 */
	/*@Autowired
	private ChamadoArquivoDao arquivoDao;*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see br.com.eswconsulting.sgcond.service.negocio.ChamadoService#
	 * findByCondominioId(java.lang.Long)
	 */
	@Override
	public List<Chamado> findByCondominioId(Long id) {

		return chamadoDao.findByCondominioId(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ChamadoService#findOne(java.
	 * lang.Long)
	 */
	@Override
	public Optional<Chamado> findOne(Long id) {
		Optional<Chamado> chamado = chamadoDao.findById(id);

		if (chamado.isPresent()) {
			Set<String> arquivos = new HashSet<>();		

			chamado.get().setArquivos(arquivos);
		}

		return chamado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ChamadoService#create(br.com.
	 * eswconsulting.sgcond.domain.entity.Chamado)
	 */
	@Override
	@Transactional
	@PreAuthorize("hasAnyRole('ROLE_SINDICO', 'ROLE_MORADOR')")
	public Chamado create(Chamado chamado) {

		return chamadoDao.save(chamado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ChamadoService#modify(br.com.
	 * eswconsulting.sgcond.domain.entity.Chamado)
	 */
	@Override
	@Transactional
	@PostAuthorize("returnObject.criador.username == principal.username OR hasRole('ROLE_SINDICO')")
	public Chamado modify(Chamado chamado) {
		return chamadoDao.save(chamado);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ChamadoService#getFile(java.
	 * lang.Long, java.lang.String)
	 */
	@Override
	public byte[] getFile(Long id, String fileName) {

		return null;//arquivoDao.get(id, fileName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ChamadoService#addFile(java.
	 * lang.Long, java.lang.String, byte[])
	 */
	@Override
	public void addFile(Long id, String fileName, byte[] bytes) {
		//arquivoDao.save(id, fileName, bytes);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.service.negocio.ChamadoService#removeFile(
	 * java.lang.Long, java.lang.String)
	 */
	@Override
	public void removeFile(Long id, String fileName) {
		 //arquivoDao.delete(id, fileName);
	}

}
