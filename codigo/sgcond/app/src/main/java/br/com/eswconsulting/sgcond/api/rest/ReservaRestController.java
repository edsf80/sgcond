/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.eswconsulting.sgcond.domain.entity.Reserva;
import br.com.eswconsulting.sgcond.service.exception.ReservaInvalidaException;
import br.com.eswconsulting.sgcond.service.exception.ReservaNaoEncontradaException;
import br.com.eswconsulting.sgcond.service.negocio.ReservaService;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/reserva")
public class ReservaRestController {

	@Autowired
	private ReservaService reservaService;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<ReservaRest> buscarTodas() {
		List<ReservaRest> reservas = this.reservaService.findAll().stream().map(reserva -> this.convertToDTO(reserva))
				.collect(Collectors.toList());

		return reservas;
	}

	/**
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/data/{data}")
	public @ResponseBody ResponseEntity<List<ReservaRest>> buscarPorData(@PathVariable String data) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

		ResponseEntity<List<ReservaRest>> resultado = null;

		List<ReservaRest> reservas;
		try {
			reservas = reservaService.findByDate(simpleDateFormat.parse(data)).stream()
					.map(reserva -> this.convertToDTO(reserva)).collect(Collectors.toList());
			resultado = new ResponseEntity<>(reservas, HttpStatus.OK);
		} catch (ParseException e) {
			resultado = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			e.printStackTrace();
		}
		return resultado;
	}

	/**
	 * @param areaComum
	 * @return
	 * @throws ReservaInvalidaException
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ReservaRest criar(@RequestBody @NotNull @Valid ReservaRest reservaRest)
			throws ReservaInvalidaException {

		Reserva reserva = this.convertToEntity(reservaRest);

		reserva = this.reservaService.create(reserva);

		return this.convertToDTO(reserva);
	}

	/**
	 * @param areaComum
	 * @return
	 * @throws ReservaInvalidaException
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public @ResponseBody ResponseEntity<ReservaRest> alterar(@PathVariable Long id,
			@RequestBody @NotNull @Valid ReservaRest reservaRest) throws ReservaInvalidaException {
		ResponseEntity<ReservaRest> resultado = null;

		Reserva reserva = this.convertToEntity(reservaRest);

		try {
			reserva = this.reservaService.modify(reserva);
			resultado = new ResponseEntity<>(this.convertToDTO(reserva), HttpStatus.OK);
		} catch (ReservaNaoEncontradaException e) {
			resultado = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public @ResponseBody ResponseEntity<ReservaRest> buscarPorId(@PathVariable Long id) {
		ResponseEntity<ReservaRest> resultado = null;

		Optional<Reserva> reserva = this.reservaService.findOne(id);

		if (reserva.isPresent()) {
			resultado = new ResponseEntity<>(this.convertToDTO(reserva.get()), HttpStatus.OK);
		} else {
			resultado = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<ReservaRest> excluir(@PathVariable Long id) throws ReservaInvalidaException {
		ResponseEntity<ReservaRest> resultado = null;

		try {
			this.reservaService.remove(id);
			ReservaRest reservaRest = new ReservaRest();
			reservaRest.setId(id);
			resultado = new ResponseEntity<>(reservaRest, HttpStatus.OK);
		} catch (ReservaNaoEncontradaException e) {
			resultado = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	private ReservaRest convertToDTO(Reserva reserva) {
		ReservaRest reservaRest = this.modelMapper.map(reserva, ReservaRest.class);
		return reservaRest;
	}

	private Reserva convertToEntity(ReservaRest reservaRest) {
		Reserva reserva = this.modelMapper.map(reservaRest, Reserva.class);
		return reserva;
	}

	@Data
	@NoArgsConstructor
	static class ReservaRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = -8725990256219568040L;

		private Long id;

		@NotNull(message = "Data e horário de início da reserva deve ser informado")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
		private Date dataHoraInicio;

		@NotNull(message = "Data e horário de término da reserva deve ser informado")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
		private Date dataHoraTermino;

		@NotNull(message = "Usuário dono da reserva deve ser informado")
		private UsuarioRest dono;

		@NotNull(message = "Área comum da reserva deve ser informada")
		@Valid
		private AreaComumRest areaComum;
	}
	
	@Data
	@NoArgsConstructor
	static class UsuarioRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 3242360226156800287L;
		
		private Long id;
		
		private String nome;
		
	}

	@Data
	@NoArgsConstructor
	static class AreaComumRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2337375099452899362L;

		@NotNull(message = "Identificador da área comum deve ser fornecida")
		private Long id;

		private String descricao;

	}

}
