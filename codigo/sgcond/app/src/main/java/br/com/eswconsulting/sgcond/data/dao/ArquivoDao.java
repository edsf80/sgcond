/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.Set;

/**
 * @author edsf
 *
 */
public interface ArquivoDao {

	void save(Long id, String fileName, byte[] file);

	byte[] get(Long id, String fileName);

	long size(Long id);

	void delete(Long id, String fileName);

	Set<Object> getKeys(Long id);
}
