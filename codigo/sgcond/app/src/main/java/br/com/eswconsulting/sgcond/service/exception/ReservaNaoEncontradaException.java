/**
 * 
 */
package br.com.eswconsulting.sgcond.service.exception;

/**
 * @author edsf
 *
 */
public class ReservaNaoEncontradaException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1069060129586883645L;

	/**
	 * @param mensagem
	 */
	public ReservaNaoEncontradaException(String mensagem) {
		super(mensagem);		
	}

}
