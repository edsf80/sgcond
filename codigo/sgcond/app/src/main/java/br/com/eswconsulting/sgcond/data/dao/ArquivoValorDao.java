/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

/**
 * @author edsf
 *
 */
public interface ArquivoValorDao {

	void save(Long id, byte[] file);

	byte[] get(Long id);

	long size(Long id);

	void delete(Long id);
}
