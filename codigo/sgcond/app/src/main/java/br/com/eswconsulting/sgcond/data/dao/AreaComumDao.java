/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import org.springframework.data.repository.CrudRepository;

import br.com.eswconsulting.sgcond.domain.entity.AreaComum;

/**
 * @author edsf
 *
 */
public interface AreaComumDao extends CrudRepository<AreaComum, Long> {

}
