/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.eswconsulting.sgcond.data.dao.UsuarioDao;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

/**
 * @author edsf
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioDao usuarioDao;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetailsService#
	 * loadUserByUsername(java.lang.String)
	 */
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Optional<Usuario> usuario = usuarioDao.findByUsername(username);
		
		UserDetailsImpl userDetails = new UserDetailsImpl(usuario.isPresent() ? usuario.get() : null);		
		
		return userDetails;
	}

}
