/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.eswconsulting.sgcond.domain.entity.Usuario;

/**
 * @author edsf
 *
 */
public interface UsuarioDao extends CrudRepository<Usuario, Long> {

	/**
	 * Busca o usuario pelo seu nome de usuário. Método pode ser usado no login.
	 * 
	 * @param username
	 *            O nome de usuário a ser pesquisado.
	 * @return O usuário encontrado pelo nome de usuário.
	 */
	Optional<Usuario> findByUsername(String username);

	/**
	 * Busca todos os usuários do banco de seus relacionamentos.
	 * 
	 * @return Uma lista de usuários do banco de dados.
	 */
	@Query("SELECT NEW Usuario(u.id, u.username, u.nome, u.senha, u.mudarSenha) FROM Usuario u")
	List<Usuario> findAllNoRelationShips();

}
