/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Aviso {
	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_avs", insertable = false, updatable = false, nullable = false)
	private Long id;
	
	/**
	 * 
	 */
	@Column(name = "tit_avs")
	@NotNull(message = "Título do aviso deve ser informado")
	private String titulo;
	
	/**
	 * 
	 */
	@Column(name = "dsc_avs")
	private String descricao;
	
	/**
	 * 
	 */
	@Column(name = "dat_val")
	@NotNull(message = "Validade do chamado deve ser informada")
	private Date validade;
	
	/**
	 * 
	 */
	@ManyToOne
	@JoinColumn(name = "id_cno", nullable = false)
	private Condominio condominio;

}
