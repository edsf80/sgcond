/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = { "id", "descricao" })
@Entity
@Table(name = "item_enquete")
public class ItemEnquete implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1236235284642502669L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ieq", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@Column(name = "dsc_it_eqt", nullable = false)
	@NotNull(message = "Descrição do item da enquete deve ser informada")
	private String descricao;

	/**
	 * Não dá pra tirar isso por que causa problema na hora de inserir os itens
	 * relacionados a uma enquete.
	 */
	@Valid
	@ManyToOne
	@JoinColumn(name = "id_eqt")
	private Enquete enquete;

	/**
	 * 
	 */
	/*
	 * @Valid
	 * 
	 * @OneToMany(mappedBy = "itemEnquete", cascade = { CascadeType.PERSIST,
	 * CascadeType.REMOVE }) private Set<Voto> votos = new HashSet<>();
	 */

	/**
	 * @param voto
	 */
	/*
	 * public void addVoto(Voto voto) { voto.setItemEnquete(this);
	 * this.votos.add(voto); }
	 */
}
