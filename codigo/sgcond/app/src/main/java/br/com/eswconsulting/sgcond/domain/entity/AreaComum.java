/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "area_comum")
public class AreaComum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2196641076680813142L;
	
	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_aco", updatable = false, nullable = false)
	private Long id;
	
	/**
	 * 
	 */
	@Column(name = "dsc_aco")
	@NotNull(message = "Descrição da área comum deve ser informada")
	private String descricao;
	
	/**
	 * 
	 */	
	@ManyToOne
	@JoinColumn(name = "id_cno", nullable = false)
	@NotNull(message = "Condomínio da área comum deve ser informado")
	private Condominio condominio;
}
