package br.com.eswconsulting.sgcond.service.negocio;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import br.com.eswconsulting.sgcond.domain.entity.Enquete;
import br.com.eswconsulting.sgcond.domain.entity.Voto;
import br.com.eswconsulting.sgcond.service.exception.VotoInvalidoException;

public interface DashboardService {
	
	List<Enquete> buscarEnquetesSemVoto();
	
	Enquete votarEnquete(@NotNull @Valid Voto voto) throws VotoInvalidoException;

	List<Enquete> buscarTodasAbertas();
}
