/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.eswconsulting.sgcond.data.dao.UsuarioDao;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.service.exception.BusinessException;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(path = "/usuario", produces = MediaType.APPLICATION_JSON_VALUE)
public class UsuarioRestController {

	private UsuarioDao usuarioDao;

	private ModelMapper modelMapper;

	private PasswordEncoder passwordEncoder;

	@Autowired
	public UsuarioRestController(UsuarioDao usuarioDao, ModelMapper modelMapper, PasswordEncoder encoder) {
		this.usuarioDao = usuarioDao;
		this.modelMapper = modelMapper;
		this.passwordEncoder = encoder;
	}

	@PostMapping
	@Transactional
	@PreAuthorize("hasRole('ROLE_ADM')")
	public @ResponseBody ResponseEntity<UsuarioRest> criarUsuario(@NotNull @Valid @RequestBody UsuarioRest usuarioRest)
			throws BusinessException {
		Optional<Usuario> usuarioOpt = this.usuarioDao.findByUsername(usuarioRest.getUsername());

		if (usuarioOpt.isPresent()) {
			throw new BusinessException("Nome de usuário já existe");
		}

		Usuario usuario = this.modelMapper.map(usuarioRest, Usuario.class);
		usuario.setSenha(this.passwordEncoder.encode(usuario.getSenha()));

		usuario = this.usuarioDao.save(usuario);

		return new ResponseEntity<>(this.modelMapper.map(usuario, UsuarioRest.class), HttpStatus.CREATED);
	}

	/**
	 * @param id
	 * @param usuarioRest
	 * @return
	 */
	@Transactional
	@PreAuthorize("#usr.username == principal.username")
	@PutMapping("/{id}/senha")
	public @ResponseBody ResponseEntity<?> alterarSenha(@PathVariable @NotNull Long id,
			@P("usr") @NotNull @Valid @RequestBody UsuarioMudaSenhaRest usuarioRest) {

		Optional<ResponseEntity<?>> resultadoOpt = Optional.empty();

		Optional<Usuario> usuarioOpt = this.usuarioDao.findById(id);

		if (!usuarioOpt.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		Usuario usuario = usuarioOpt.get();

		if (!this.passwordEncoder.matches(usuarioRest.getSenhaAtual(), usuario.getSenha())) {
			resultadoOpt = Optional.of(new ResponseEntity<>("Senha informada não corresponde a senha atual do usuário",
					HttpStatus.UNPROCESSABLE_ENTITY));
		} else {
			usuario.setSenha(this.passwordEncoder.encode(usuarioRest.getNovaSenha()));
			usuario.setMudarSenha(Boolean.FALSE);
			resultadoOpt = Optional.of(new ResponseEntity<>(HttpStatus.OK));
		}

		return resultadoOpt.get();
	}

	@GetMapping("/{id}")
	@PostAuthorize("returnObject.body.username == principal.username OR hasRole('ROLE_ADM')")
	public @ResponseBody ResponseEntity<UsuarioRest> buscarPorId(@PathVariable @NotNull Long id) {

		Optional<Usuario> usuario = this.usuarioDao.findById(id);
		ResponseEntity<UsuarioRest> resultado = null;

		if (usuario.isPresent()) {
			resultado = new ResponseEntity<>(this.modelMapper.map(usuario.get(), UsuarioRest.class), HttpStatus.OK);
		} else {
			resultado = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	@Transactional
	@PutMapping("/{id}")
	@PostAuthorize("returnObject.body.username == principal.username OR hasRole('ROLE_ADM')")
	public @ResponseBody ResponseEntity<UsuarioRest> alterarUsuario(@NotNull @PathVariable Long id,
			@NotNull @Valid @RequestBody UsuarioRest usuarioRest) throws BusinessException {
		// TODO: Verificar como exigir o id do usuário apenas nesse método.
		Optional<Usuario> usuario = this.usuarioDao.findById(id);

		if (!usuario.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		Usuario user = usuario.get();

		usuario = this.usuarioDao.findByUsername(usuarioRest.getUsername());

		if (usuario.isPresent() && !usuario.get().getId().equals(id)) {
			throw new BusinessException("Tentativa de atualização para um nome de usuário já existente");
		}

		user.setNome(usuarioRest.getNome());
		user.setUsername(usuarioRest.getUsername());

		if (usuarioRest.getMudarSenha() != null) {
			user.setMudarSenha(usuarioRest.getMudarSenha());
		}

		if (usuarioRest.getSenha() != null && !usuarioRest.getSenha().isEmpty()) {
			user.setSenha(this.passwordEncoder.encode(usuarioRest.getSenha()));
		}

		return new ResponseEntity<>(this.modelMapper.map(user, UsuarioRest.class), HttpStatus.OK);
	}

	@GetMapping
	@PreAuthorize("hasRole('ROLE_ADM')")
	public @ResponseBody List<UsuarioRest> listarTodos() {
		List<UsuarioRest> usuarios = StreamSupport.stream(this.usuarioDao.findAllNoRelationShips().spliterator(), false)
				.map(usuario -> this.modelMapper.map(usuario, UsuarioRest.class)).collect(Collectors.toList());

		return usuarios;
	}

	@Data
	@NoArgsConstructor
	public static class UsuarioRest implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4143421296806786192L;

		private Long id;

		@NotEmpty(message = "O nome do usuário deve ser informado")
		@NotNull(message = "O nome do usuário deve ser informado")
		private String nome;

		@NotEmpty(message = "O e-mail do usuário deve ser informado")
		@NotNull(message = "O e-mail do usuário deve ser informado")
		private String username;

		// Essa anotação foi utilizada para que a senha não seja enviada para o cliente
		// mas seja recebida dele para inserção e atualização.
		@JsonProperty(access = Access.WRITE_ONLY)
		private String senha;

		private Set<String> papeis = new HashSet<>();

		private Boolean mudarSenha;

		@AssertTrue(message = "Alteração de senha requer a senha atual e nova senha")
		private boolean getSenhaObrigatoriaValida() {
			return (this.id != null) || (this.senha != null);
		}
	}

	@Data
	@NoArgsConstructor
	static class UsuarioMudaSenhaRest implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4143421296806786192L;

		/**
		 * 
		 */
		@NotEmpty(message = "O login do usuário deve ser informado")
		@NotNull(message = "O login do usuário deve ser informado")
		private String username;

		/**
		 * 
		 */
		@NotEmpty(message = "A senha atual do usuário deve ser informada")
		@NotNull(message = "A senha atual do usuário deve ser informada")
		private String senhaAtual;

		/**
		 * 
		 */
		@NotEmpty(message = "A nova senha do usuário deve ser informada")
		@NotNull(message = "A nova senha do usuário deve ser informada")
		private String novaSenha;

		/**
		 * @return
		 */
		@AssertTrue(message = "Nova senha não deve ser igual a senha atual")
		public boolean isSenhasDiferentes() {
			return !this.senhaAtual.equals(this.novaSenha);
		}
	}

}
