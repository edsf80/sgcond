/**
 * 
 */
package br.com.eswconsulting.sgcond.service.exception;

/**
 * @author edsf
 *
 */
public class ReservaInvalidaException extends BusinessException {

	public ReservaInvalidaException(String mensagem) {
		super(mensagem);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5233450710104534724L;

}
