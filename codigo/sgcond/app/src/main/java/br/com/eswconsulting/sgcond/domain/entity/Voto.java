/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@IdClass(VotoId.class)
@EqualsAndHashCode(of = {"enquete", "unidade"})
public class Voto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7476528981445818664L;
	
	@Id
	@ManyToOne
	@NotNull(message = "Enquete na qual está se votando deve ser informada")
	@JoinColumn(name = "id_eqt", nullable = false)
	private Enquete enquete;
	
	/**
	 * 
	 */
	@Id
	@ManyToOne
	@NotNull(message = "Unidade que está votando deve ser informada")
	@JoinColumn(name = "id_und", nullable = false)
	private Unidade unidade;

	/**
	 * 
	 */
	@ManyToOne
	@NotNull(message = "Item da enquete na qual está se votando deve ser informado")
	@JoinColumn(name = "id_ieq", nullable = false)
	private ItemEnquete itemEnquete;

	

	/**
	 * 
	 */
	@Column(name = "vot_cmt")
	private String comentario;
}
