/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.eswconsulting.sgcond.data.dao.AreaComumDao;
import br.com.eswconsulting.sgcond.domain.entity.AreaComum;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/areacomum")
public class AreaComumRestController {

	/**
	 * 
	 */
	private AreaComumDao areaComumDao;

	/**
	 * @param areaComumDao
	 */
	@Autowired
	public AreaComumRestController(AreaComumDao areaComumDao) {
		this.areaComumDao = areaComumDao;
	}

	/**
	 * @return
	 */
	@GetMapping
	public @ResponseBody List<AreaComum> buscarTodos() {
		return StreamSupport.stream(areaComumDao.findAll().spliterator(), false).collect(Collectors.toList());
	}

	/**
	 * @param areaComum
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	@Transactional
	@PostMapping
	public @ResponseBody ResponseEntity<AreaComum> criar(@RequestBody @NotNull @Valid AreaComum areaComum) {
		return new ResponseEntity<AreaComum>(this.areaComumDao.save(areaComum), HttpStatus.OK);
	}

	/**
	 * @param areaComum
	 * @return
	 */
	@Transactional
	@PutMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_SINDICO')")
	public @ResponseBody ResponseEntity<?> alterar(@NotNull @PathVariable Long id,
			@RequestBody @NotNull @Valid AreaComum areaComum) {

		if (areaComum.getId() == null) {
			return new ResponseEntity<String>(HttpStatus.UNPROCESSABLE_ENTITY);
		}

		AreaComum area = this.areaComumDao.save(areaComum);

		ResponseEntity<AreaComum> resposta = new ResponseEntity<>(area, HttpStatus.OK);

		return resposta;
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public @ResponseBody ResponseEntity<AreaComum> buscarPorId(@PathVariable @NotNull Long id) {
		ResponseEntity<AreaComum> resultado = null;

		Optional<AreaComum> areaComum = this.areaComumDao.findById(id);

		if (areaComum.isPresent()) {
			resultado = new ResponseEntity<>(areaComum.get(), HttpStatus.OK);
		} else {
			resultado = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return resultado;
	}
}
