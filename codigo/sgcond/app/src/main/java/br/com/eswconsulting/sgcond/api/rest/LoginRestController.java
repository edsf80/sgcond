/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.service.negocio.UsuarioService;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
public class LoginRestController {

	private UsuarioService usuarioService;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private ModelMapper modelMapper;

	@Autowired
	public LoginRestController(UsuarioService usuarioService, ModelMapper modelMapper) {
		this.usuarioService = usuarioService;
		this.modelMapper = modelMapper;
	}

	@PostMapping("/login")
	public @ResponseBody ResponseEntity<Object> executarLogin(@RequestBody @NotNull @Valid UsuarioLoginRest usuario) {

		ResponseEntity<Object> responseEntity = null;

		Optional<Usuario> usuarioSessao = usuarioService.executarLogin(usuario.getUsername(), usuario.getSenha());

		if (usuarioSessao.isPresent()) {
			UsuarioRest usuarioRest = this.convertToDto(usuarioSessao.get());
			responseEntity = new ResponseEntity<Object>(usuarioRest, HttpStatus.OK);
			logger.debug("Sucesso na tentantiva de login do usuário.");
		} else {
			responseEntity = new ResponseEntity<Object>("Usuário ou senha inválida", HttpStatus.UNPROCESSABLE_ENTITY);
			logger.debug("Falha na tentativa de login do usuário.");
		}

		return responseEntity;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Object> executarLogout(HttpServletRequest request) {

		ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(HttpStatus.OK);
		SecurityContextHolder.clearContext();
		this.logger.debug("Executando a logout do sistema.");		

		return responseEntity;
	}

	@Data
	@NoArgsConstructor
	public static class UsuarioLoginRest implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4143421296806786192L;

		@NotNull(message = "O e-mail do usuário deve ser informado")
		private String username;

		@NotNull(message = "A senha do usuário deve ser informada")
		private String senha;
	}

	@Data
	@NoArgsConstructor
	public static class UsuarioRest implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 4143421296806786192L;

		private Long id;

		private String nome;

		private String username;

		private Set<String> papeis = new HashSet<>();

		private Boolean mudarSenha;

		public void addPapel(String papel) {
			this.papeis.add(papel);
		}
	}

	private UsuarioRest convertToDto(Usuario usuario) {
		UsuarioRest usuarioRest = modelMapper.map(usuario, UsuarioRest.class);

		return usuarioRest;
	}
}