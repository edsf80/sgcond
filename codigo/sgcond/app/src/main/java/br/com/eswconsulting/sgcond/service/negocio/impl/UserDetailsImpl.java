package br.com.eswconsulting.sgcond.service.negocio.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.eswconsulting.sgcond.domain.entity.Papel;
import br.com.eswconsulting.sgcond.domain.entity.Usuario;

public class UserDetailsImpl implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private List<GrantedAuthority> authorities = new ArrayList<>();

	private String username;

	private transient String password;

	private String nome;

	private Boolean mudarSenha;

	public UserDetailsImpl() {
	}

	public UserDetailsImpl(Usuario usuario) {
		if (usuario != null) {
			this.setId(usuario.getId());
			this.username = usuario.getUsername() == null ? null : usuario.getUsername();
			this.password = usuario.getSenha() == null ? null : usuario.getSenha();
			this.mudarSenha = usuario.getMudarSenha();
			this.nome = usuario.getNome();

			if (usuario.getPapeis() != null) {
				for (Papel papel : usuario.getPapeis()) {
					this.authorities.add(new SimpleGrantedAuthority(papel.getCodigo()));
				}
			}
		}
	}

	public void setAuthorities(List<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return
	 */
	public Boolean getMudarSenha() {
		return this.mudarSenha;
	}

	/**
	 * @param mudarSenha
	 */
	public void setMudarSenha(Boolean mudarSenha) {
		this.mudarSenha = mudarSenha;
	}

}
