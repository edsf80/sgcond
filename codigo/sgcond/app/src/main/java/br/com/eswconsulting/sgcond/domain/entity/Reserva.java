/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Entity
public class Reserva implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7292174741249819917L;	

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rva", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@Column(name = "ini_rva")
	@NotNull(message = "Hora de início da reserva deve ser informada")
	private Date dataHoraInicio;

	/**
	 * 
	 */
	@Column(name = "fim_rva")
	@NotNull(message = "Hora de término da reserva deve ser informada")
	private Date dataHoraTermino;

	/**
	 * 
	 */
	@NotNull(message = "Dono da reserva deve ser informado")
	@ManyToOne
	@JoinColumn(name = "id_usr", nullable = false, updatable = false)
	private Usuario dono;

	/**
	 * 
	 */
	@ManyToOne
	@JoinColumn(name = "id_aco", nullable = false, updatable = false)
	private AreaComum areaComum;
}
