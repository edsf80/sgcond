/**
 * 
 */
package br.com.eswconsulting.sgcond.api.config;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author edsf
 *
 */
@Component // Mudei pra component para funcionar no jar, mas no war trocar por
			// @WebFilter("/*")
public class ResourceFilter implements Filter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${server.servlet.path}")
	private String uriServices;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;

		if (request.getRequestURI().startsWith(this.uriServices)
				|| request.getRequestURI().matches(".*(css|jpg|png|gif|js|json)$")) {
			chain.doFilter(req, res);
		} else {
			logger.debug("Chamado o filtro para redirecionamento para a página simples do sistema.");
			RequestDispatcher dispatcher = req.getRequestDispatcher("/index.html");
			dispatcher.forward(req, res);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}
