/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Chamado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7222642563162803181L;

	/**
	 * As transicoes sao de acordo com a posicao o item no enumeration.
	 */
	private transient StatusChamado[][] transicoesStatus = {
			{ StatusChamado.NOVO, StatusChamado.EM_ANDAMENTO, StatusChamado.RESOLVIDO },
			{ StatusChamado.EM_ANDAMENTO, StatusChamado.RESOLVIDO }, { StatusChamado.RESOLVIDO } };

	public enum StatusChamado {
		NOVO, EM_ANDAMENTO, RESOLVIDO
	}

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cho", insertable = false, updatable = false, nullable = false)
	private Long id;

	/**
	 * 
	 */
	@Column(name = "tit_cho")
	@NotNull(message = "Título do chamado deve ser informado")
	private String titulo;

	/**
	 * 
	 */
	@Column(name = "dsc_cho")
	@NotNull(message = "Descrição do chamado deve ser informada")
	private String descricao;

	/**
	 * 
	 */
	@Column(name = "sts_cho")
	@Enumerated(EnumType.STRING)
	@NotNull(message = "Status do chamado deve ser informado")
	private StatusChamado status;

	/**
	 * 
	 */
	@ManyToOne
	@JoinColumn(name = "id_cno", nullable = false)
	@NotNull(message = "Condomínio do chamado deve ser informado")
	private Condominio condominio;

	@Transient
	private Set<String> arquivos;
	
	@ManyToOne
	@JoinColumn(name = "id_usr", nullable = false, updatable = false)
	@NotNull(message = "Usuário criador do chamado deve ser informado")
	private UsuarioSemRel criador;
	
	@Column(name = "dt_cri", nullable = false, updatable = false)
	private Date dataCriacao;
	

	/**
	 * @return
	 */
	public StatusChamado[] getTransicoes() {
		StatusChamado[] resultado = {};

		resultado = transicoesStatus[this.getStatus().ordinal()];

		return resultado;
	}

}
