package br.com.eswconsulting.sgcond.data.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.eswconsulting.sgcond.domain.entity.Reserva;

public interface ReservaDao extends CrudRepository<Reserva, Long>, ReservaDaoCustom {

	/**
	 * Método deve ser revisto por apresentar problemas em testes unitários com
	 * banco em memória.
	 * 
	 * @param dataHora
	 * @return
	 */
	@Query("select r from Reserva r where date(r.dataHoraInicio) = ?1 or date(r.dataHoraTermino) = ?1")
	List<Reserva> findByData(Date dataHora);

	/**
	 * Método para buscar reservas em determinado dia.
	 * 
	 * @param inicio
	 * @param terminio
	 * @return
	 */
	List<Reserva> findByDataHoraInicioBetween(Date inicio, Date terminio);
}
