/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio;

import java.util.Optional;

import br.com.eswconsulting.sgcond.domain.entity.Usuario;
import br.com.eswconsulting.sgcond.service.exception.NovaSenhaInvalidaException;

/**
 * @author edsf
 *
 */
public interface UsuarioService {

	/**
	 * @param ussername
	 * @param senha
	 * @return
	 */
	Optional<Usuario> executarLogin(String ussername, String senha);

	/**
	 * @param usuario
	 * @return
	 */
	Usuario atualizarPerfil(Usuario usuario, String senhaAtual) throws NovaSenhaInvalidaException;
}
