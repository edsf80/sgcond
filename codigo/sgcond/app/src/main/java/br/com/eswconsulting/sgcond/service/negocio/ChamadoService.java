/**
 * 
 */
package br.com.eswconsulting.sgcond.service.negocio;

import java.util.List;
import java.util.Optional;

import br.com.eswconsulting.sgcond.domain.entity.Chamado;

/**
 * @author edsf
 *
 */
public interface ChamadoService {
	
	/**
	 * @param id
	 * @return
	 */
	List<Chamado> findByCondominioId(Long id);
	
	/**
	 * @param id
	 * @return
	 */
	Optional<Chamado> findOne(Long id);
	
	/**
	 * @param chamado
	 * @return
	 */
	Chamado create(Chamado chamado);
	
	/**
	 * @param chamado
	 * @return
	 */
	Chamado modify(Chamado chamado);
	
	/**
	 * @param id
	 * @param fileName
	 * @return
	 */
	byte[] getFile(Long id, String fileName);
	
	/**
	 * @param id
	 * @param fileName
	 * @param bytes
	 */
	void addFile(Long id, String fileName, byte[] bytes);
	
	/**
	 * @param id
	 * @param fileName
	 */
	void removeFile(Long id, String fileName);

}
