/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao;

import java.util.Date;
import java.util.List;

import br.com.eswconsulting.sgcond.domain.entity.Reserva;

/**
 * @author edsf
 *
 */
public interface ReservaDaoCustom {

	/**
	 * @param id
	 * @param idAreaComun
	 * @param begin
	 * @param end
	 * @return
	 */
	List<Reserva> findColision(Long id, Long idAreaComun, Date begin, Date end);
}
