/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.eswconsulting.sgcond.data.dao.UnidadeDao;
import br.com.eswconsulting.sgcond.domain.entity.Unidade;
import br.com.eswconsulting.sgcond.service.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(path = "/unidade", produces = MediaType.APPLICATION_JSON_VALUE)
public class UnidadeRestController {

	private UnidadeDao unidadeDao;

	private ModelMapper modelMapper;

	/**
	 * @param unidadeDao
	 * @param modelMapper
	 */
	@Autowired
	public UnidadeRestController(UnidadeDao unidadeDao, ModelMapper modelMapper) {
		this.unidadeDao = unidadeDao;
		this.modelMapper = modelMapper;
	}

	/**
	 * @return
	 */
	@GetMapping
	@PreAuthorize("hasRole('ROLE_ADM')")
	public @ResponseBody List<UnidadeRest> buscarTodas() {
		return StreamSupport.stream(this.unidadeDao.findAll().spliterator(), false)
				.map(unidade -> this.modelMapper.map(unidade, UnidadeRest.class)).collect(Collectors.toList());
	}

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	@PostAuthorize("returnObject.body.usuario.username == principal.username OR hasRole('ROLE_ADM')")
	public @ResponseBody ResponseEntity<UnidadeRest> buscarPorId(@PathVariable Long id) {
		Optional<Unidade> unidadeOpt = this.unidadeDao.findById(id);
		ResponseEntity<UnidadeRest> resultado;

		if (unidadeOpt.isPresent()) {
			resultado = new ResponseEntity<>(this.modelMapper.map(unidadeOpt.get(), UnidadeRest.class), HttpStatus.OK);
		} else {
			// Criado esse objeto apenas para não dar problema na comparação do
			// postauthorize.
			UnidadeRest unidadeRest = new UnidadeRest(null, null, new UsuarioRest(null, null, null), null);
			resultado = new ResponseEntity<>(unidadeRest, HttpStatus.NOT_FOUND);
		}

		return resultado;
	}

	/**
	 * @param unidadeRest
	 * @return
	 * @throws BusinessException
	 */
	@PostMapping
	@Transactional
	@PreAuthorize("hasRole('ROLE_ADM')")
	public @ResponseBody ResponseEntity<UnidadeRest> inserir(@NotNull @Valid @RequestBody UnidadeRest unidadeRest)
			throws BusinessException {

		// TODO: tirar quando tiver mais de um condominio.
		unidadeRest.setCondominio(new CondominioRest(1l));

		Optional<Unidade> unidadeExiste = this.unidadeDao.findByIdentificadorAndCondominioId(
				unidadeRest.getIdentificador(), unidadeRest.getCondominio().getId());

		if (unidadeExiste.isPresent()) {
			throw new BusinessException("Identificador da unidade já existe no condomínio");
		}

		Unidade unidade = this.unidadeDao.save(this.modelMapper.map(unidadeRest, Unidade.class));

		return new ResponseEntity<UnidadeRest>(this.modelMapper.map(unidade, UnidadeRest.class), HttpStatus.CREATED);
	}

	/**
	 * @param id
	 * @param unidadeRest
	 * @return
	 * @throws BusinessException
	 */
	@PutMapping("/{id}")
	@Transactional
	@PreAuthorize("hasRole('ROLE_ADM')")
	public @ResponseBody ResponseEntity<UnidadeRest> alterar(@PathVariable Long id,
			@Valid @RequestBody UnidadeRest unidadeRest) throws BusinessException {
		unidadeRest.setCondominio(new CondominioRest(1l));

		if (unidadeRest.getId() == null) {
			return new ResponseEntity<UnidadeRest>(HttpStatus.UNPROCESSABLE_ENTITY);
		}

		// Verifica se está tentando atualizar uma undiade existente.
		Optional<Unidade> unidadeOpt = this.unidadeDao.findById(id);

		if (!unidadeOpt.isPresent()) {
			return new ResponseEntity<UnidadeRest>(HttpStatus.NOT_FOUND);
		}

		// Verifica se está tentando atualizar o identificador para um já existente no
		// condominio.
		unidadeOpt = this.unidadeDao.findByIdentificadorAndCondominioId(unidadeRest.getIdentificador(),
				unidadeRest.getCondominio().getId());

		if (unidadeOpt.isPresent() && !unidadeOpt.get().getId().equals(unidadeRest.getId())) {
			throw new BusinessException("Tentativa de atualização para um identificador de outra unidade");
		}

		Unidade unidade = this.modelMapper.map(unidadeRest, Unidade.class);

		unidade = this.unidadeDao.save(unidade);

		return new ResponseEntity<UnidadeRest>(this.modelMapper.map(unidade, UnidadeRest.class), HttpStatus.OK);
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class UnidadeRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4588775236882950408L;

		private Long id;

		@NotNull(message = "Identificador da unidade deve ser informado")
		private String identificador;

		@Valid
		@NotNull(message = "Proprietário da unidade deve ser informado")
		private UsuarioRest usuario;

		// TODO: atualizar quando tiver mais de um condominio.
		private CondominioRest condominio;

	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class UsuarioRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4588775236882950408L;

		@NotNull(message = "Proprietário da unidade deve ser informado")
		private Long id;

		private String username;

		private String nome;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class CondominioRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 4588775236882950408L;

		private Long id;
	}
}
