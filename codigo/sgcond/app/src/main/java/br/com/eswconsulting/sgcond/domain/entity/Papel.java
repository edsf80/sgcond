/**
 * 
 */
package br.com.eswconsulting.sgcond.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author edsf
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = { "codigo" })
@Entity
public class Papel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7856105223861024914L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_ppl", insertable = false, updatable = false)
	private Long id;

	/**
	 * 
	 */
	@NotNull(message = "Descrição do pappel deve ser informada")
	@Column(name = "dsc_ppl")
	private String descricao;

	/**
	 * 
	 */
	@NotNull(message = "Código do pappel deve ser informado")
	@Column(name = "cod_ppl")
	private String codigo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		// Isso aqui foi feito para no loginRestcontroller ele traduzir o papel no seu
		// codigo e isso ser passado para a aplicação.
		return this.codigo;
	}
}
