/**
 * 
 */
package br.com.eswconsulting.sgcond.api.rest;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.eswconsulting.sgcond.domain.entity.Chamado;
import br.com.eswconsulting.sgcond.domain.entity.Condominio;
import br.com.eswconsulting.sgcond.domain.entity.UsuarioSemRel;
import br.com.eswconsulting.sgcond.service.negocio.ChamadoService;
import br.com.eswconsulting.sgcond.service.negocio.impl.UserDetailsImpl;

/**
 * @author edsf
 *
 */
@RestController
@RequestMapping(value = "/chamado")
public class ChamadoRestController {

	/**
	 * 
	 */
	@Autowired
	private ChamadoService chamadoService;

	/**
	 * 
	 */
	@Autowired
	private ModelMapper modelMapper;

	/**
	 * @param idCondominio
	 * @return
	 */
	@RequestMapping(value = "/condominio/{idCondominio}")
	public @ResponseBody List<ChamadoRest> buscarTodosPorCondominio(@PathVariable Long idCondominio) {

		List<Chamado> chamados = chamadoService.findByCondominioId(1l);

		return chamados.stream().map(chamado -> convertToDto(chamado)).collect(Collectors.toList());
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}")
	public @ResponseBody ResponseEntity<ChamadoRest> buscarPorId(@PathVariable Long id) {

		ResponseEntity<ChamadoRest> resultado = null;

		Optional<Chamado> chamado = chamadoService.findOne(id);

		if (chamado.isPresent()) {
			ChamadoRest chamadoRest = convertToDto(chamado.get());
			resultado = new ResponseEntity<>(chamadoRest, HttpStatus.OK);			
		} else {
			resultado = new ResponseEntity<>(HttpStatus.NOT_FOUND);			
		}

		return resultado;
	}

	@RequestMapping(value = "/{id}/imagem/{nomeArquivo}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageAsResponseEntity(@PathVariable("id") Long id,
			@PathVariable("nomeArquivo") String nomeArquivo) {
		HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl(CacheControl.noCache().getHeaderValue());
		headers.setContentType(MediaType.IMAGE_JPEG);
		ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(chamadoService.getFile(id, nomeArquivo), headers,
				HttpStatus.OK);
		return responseEntity;
	}

	@RequestMapping(value = "/{id}/imagem/{nomeArquivo}", method = RequestMethod.POST)
	public void updaloadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") Long id,
			@PathVariable("nomeArquivo") String nomeArquivo) throws IOException {
		chamadoService.addFile(id, nomeArquivo, file.getBytes());
	}

	@RequestMapping(value = "/{id}/imagem/{nomeArquivo}", method = RequestMethod.DELETE)
	public void deletedFile(@PathVariable("id") Long id, @PathVariable("nomeArquivo") String nomeArquivo) {
		chamadoService.removeFile(id, nomeArquivo);
	}

	/**
	 * @param chamado
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody ChamadoRest criar(@RequestBody ChamadoRest chamadoRest) {

		Chamado chamado = this.convertToEntity(chamadoRest);

		UserDetailsImpl criador = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
				.getPrincipal();

		UsuarioSemRel usuario = new UsuarioSemRel();
		usuario.setId(criador.getId());

		chamado.setCriador(usuario);
		chamado.setDataCriacao(new Date());
		chamado = chamadoService.create(chamado);

		return this.convertToDto(chamado);
	}

	/**
	 * @param chamado
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.PUT)
	public @ResponseBody ChamadoRest alterar(@RequestBody ChamadoRest chamadoRest) {

		Chamado chamado = this.convertToEntity(chamadoRest);
		chamado = chamadoService.modify(chamado);
		return this.convertToDto(chamado);
	}

	/**
	 * @param chamado
	 * @return
	 */
	private ChamadoRest convertToDto(Chamado chamado) {
		ChamadoRest chamadoRest = modelMapper.map(chamado, ChamadoRest.class);
		chamadoRest.setIdCriador(chamado.getCriador().getId());
		chamadoRest.setIdCondominio(chamado.getCondominio().getId());

		return chamadoRest;
	}

	/**
	 * @param chamado
	 * @return
	 */
	private Chamado convertToEntity(ChamadoRest chamadoRest) {
		Chamado chamado = modelMapper.map(chamadoRest, Chamado.class);
		UsuarioSemRel usuario = new UsuarioSemRel();
		usuario.setId(chamadoRest.getIdCriador());
		chamado.setCriador(usuario);
		Condominio condominio = new Condominio();
		condominio.setId(chamadoRest.getIdCondominio());
		chamado.setCondominio(condominio);

		return chamado;
	}

	static class ChamadoRest implements Serializable {

		/**
		 * 
		 */
		private static final long serialVersionUID = 5301533552568524237L;

		private Long id;

		@NotNull(message = "Título do chamado deve ser informado")
		private String titulo;

		@NotNull(message = "Descrição do chamado deve ser informada")
		private String descricao;

		@NotNull(message = "Status do chamado deve ser informado")
		private String status;

		private String transicoes[];

		@NotNull(message = "Usuário criador do chamado deve ser informado")
		private Long idCriador;

		@NotNull(message = "Condomínio do chamado deve ser informado")
		private Long idCondominio;

		private Date dataCriacao;

		/**
		 * @return the id
		 */
		public Long getId() {
			return id;
		}

		/**
		 * @param id
		 *            the id to set
		 */
		public void setId(Long id) {
			this.id = id;
		}

		/**
		 * @return the titulo
		 */
		public String getTitulo() {
			return titulo;
		}

		/**
		 * @param titulo
		 *            the titulo to set
		 */
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}

		/**
		 * @return the descricao
		 */
		public String getDescricao() {
			return descricao;
		}

		/**
		 * @param descricao
		 *            the descricao to set
		 */
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		/**
		 * @return the status
		 */
		public String getStatus() {
			return status;
		}

		/**
		 * @param status
		 *            the status to set
		 */
		public void setStatus(String status) {
			this.status = status;
		}

		/**
		 * @return the criador
		 */
		public Long getIdCriador() {
			return idCriador;
		}

		/**
		 * @param criador
		 *            the criador to set
		 */
		public void setIdCriador(Long criador) {
			this.idCriador = criador;
		}

		/**
		 * @return the transicoes
		 */
		public String[] getTransicoes() {
			return transicoes;
		}

		/**
		 * @param transicoes
		 *            the transicoes to set
		 */
		public void setTransicoes(String[] transicoes) {
			this.transicoes = transicoes;
		}

		/**
		 * @return the idCondominio
		 */
		public Long getIdCondominio() {
			return idCondominio;
		}

		/**
		 * @param idCondominio
		 *            the idCondominio to set
		 */
		public void setIdCondominio(Long idCondominio) {
			this.idCondominio = idCondominio;
		}

		/**
		 * @return the dataCriacao
		 */
		public Date getDataCriacao() {
			return dataCriacao;
		}

		/**
		 * @param dataCriacao
		 *            the dataCriacao to set
		 */
		public void setDataCriacao(Date dataCriacao) {
			this.dataCriacao = dataCriacao;
		}

	}
}
