/**
 * 
 */
package br.com.eswconsulting.sgcond.service.exception;

/**
 * @author edsf
 *
 */
public class BusinessException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6122714764286519451L;
	
	public BusinessException(String mensagem) {
		super(mensagem);
	}

}
