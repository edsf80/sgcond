/**
 * 
 */
package br.com.eswconsulting.sgcond.data.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.eswconsulting.sgcond.data.dao.ReservaDaoCustom;
import br.com.eswconsulting.sgcond.domain.entity.Reserva;

/**
 * @author edsf
 *
 */
public class ReservaDaoImpl implements ReservaDaoCustom {

	@PersistenceContext
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * br.com.eswconsulting.sgcond.data.dao.ReservaDaoCustom#findColision(java.lang.
	 * Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<Reserva> findColision(Long id, Long idAreaComun, Date begin, Date end) {
		StringBuffer jpql = new StringBuffer("select r from Reserva r ");
		jpql.append("where r.areaComum.id = :idAreaComum AND");
		jpql.append(" (:inicio < r.dataHoraInicio AND");
		jpql.append(" :termino > r.dataHoraInicio) OR");
		jpql.append(" (:inicio >= r.dataHoraInicio AND :inicio < r. dataHoraTermino)");
		if(id != null) {
			jpql.append(" AND r.id != :id");
		}
		TypedQuery<Reserva> query = this.em.createQuery(jpql.toString(), Reserva.class);
		query.setParameter("idAreaComum", idAreaComun);
		query.setParameter("inicio", begin);
		query.setParameter("termino", end);		
		if(id != null) {
			query.setParameter("id", id);
		}

		return query.getResultList();
	}

}
