package br.com.eswconsulting.sgcond.data.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.eswconsulting.sgcond.data.dao.EnqueteDaoCustom;
import br.com.eswconsulting.sgcond.domain.entity.Enquete;

@Repository
public class EnqueteDaoImpl implements EnqueteDaoCustom {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Enquete> findTodasAbertas() {
		String jpql = "select e from Enquete e where e.status = :status";
		TypedQuery<Enquete> query = this.em.createQuery(jpql, Enquete.class);
		query.setParameter("status", Enquete.StatusEnquete.ABERTA);

		return query.getResultList();
	}

	@Override
	public List<Object[]> findTodasAbertasSemVotoPorUsuario(Long idUsuario) {
		StringBuffer jpql = new StringBuffer(
				"SELECT e, u from Enquete e INNER JOIN Unidade u on e.condominio = u.condominio\n");
		//jpql.append("INNER JOIN FETCH e.itens ie LEFT OUTER JOIN ie.votos v\n");
		jpql.append("WHERE u.usuario.id = :idUsuario AND e.status = :status\n");
		jpql.append("AND NOT EXISTS (SELECT 1 FROM Voto v WHERE v.enquete = e AND v.unidade = u)\n");
		//jpql.append("AND v.itemEnquete IS NULL");

		Query query = this.em.createQuery(jpql.toString());
		query.setParameter("idUsuario", idUsuario);
		query.setParameter("status", Enquete.StatusEnquete.ABERTA);

		return query.getResultList();
	}

}
