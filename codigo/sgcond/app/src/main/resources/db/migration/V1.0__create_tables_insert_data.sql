--DROP TABLE public.flyway_schema_history;
--DROP TABLE public.aviso;
--DROP TABLE public.chamado;
--DROP TABLE public.convidado;
--DROP TABLE public.reserva;
--DROP TABLE public.area_comum;
--DROP TABLE public.voto;
--DROP TABLE public.unidade;
--DROP TABLE public.item_enquete;
--DROP TABLE public.enquete;
--DROP TABLE public.condominio;
--DROP TABLE public.usuario_papel;
--DROP TABLE public.papel;
--DROP TABLE public.usuario;

CREATE TABLE public.usuario (
                id_usr BIGSERIAL,
                msn_usr BOOLEAN DEFAULT TRUE NOT NULL,
                sna_usr VARCHAR(100) NOT NULL,
                unm_usr VARCHAR(50) NOT NULL,
                nme_usr VARCHAR(50) NOT NULL,
                CONSTRAINT pk_usuario PRIMARY KEY (id_usr)
);


CREATE TABLE public.papel (
                id_ppl BIGSERIAL,
                dsc_ppl VARCHAR(50) NOT NULL,
                cod_ppl VARCHAR(50) NOT NULL,
                CONSTRAINT pk_papel PRIMARY KEY (id_ppl)
);


CREATE TABLE public.usuario_papel (
                id_usr BIGINT NOT NULL,
                id_ppl BIGINT NOT NULL,
                CONSTRAINT pk_usuario_papel PRIMARY KEY (id_usr, id_ppl)
);


CREATE TABLE public.condominio (
                id_cno BIGSERIAL,
                nme_cno VARCHAR(50) NOT NULL,
                cnpj_cno VARCHAR(21) NOT NULL,
                CONSTRAINT pk_condominio PRIMARY KEY (id_cno)
);


CREATE TABLE public.enquete (
                id_eqt BIGSERIAL,
                id_cno BIGINT NOT NULL,
                tit_eqt VARCHAR(100) NOT NULL,
                dtf_eqt TIMESTAMP,
                dti_eqt TIMESTAMP,
                sts_eqt VARCHAR(255) NOT NULL,
                CONSTRAINT pk_enquete PRIMARY KEY (id_eqt)
);


CREATE TABLE public.item_enquete (
                id_ieq BIGSERIAL,
                id_eqt BIGINT NOT NULL,
                dsc_it_eqt VARCHAR(100) NOT NULL,
                CONSTRAINT pk_item_enquete PRIMARY KEY (id_ieq)
);


CREATE TABLE public.unidade (
                id_und BIGSERIAL,
                idt_und VARCHAR(10) NOT NULL,
                id_cno BIGINT NOT NULL,
                id_usr BIGINT NOT NULL,
                CONSTRAINT pk_unidade PRIMARY KEY (id_und)
);


CREATE TABLE public.voto (
                id_eqt BIGINT NOT NULL,
                id_und BIGINT NOT NULL,
                vot_cmt VARCHAR(100),
                id_ieq BIGINT NOT NULL,
                CONSTRAINT pk_voto PRIMARY KEY (id_eqt, id_und)
);


CREATE TABLE public.area_comum (
                id_aco BIGSERIAL,
                id_cno BIGINT NOT NULL,
                dsc_aco VARCHAR(30) NOT NULL,
                CONSTRAINT pk_area_comum PRIMARY KEY (id_aco)
);


CREATE TABLE public.reserva (
                id_rva BIGSERIAL,
                fim_rva TIMESTAMP NOT NULL,
                ini_rva TIMESTAMP NOT NULL,
                id_aco BIGINT NOT NULL,
                id_usr BIGINT NOT NULL,
                CONSTRAINT pk_reserva PRIMARY KEY (id_rva)
);


CREATE TABLE public.convidado (
                id_cvo BIGSERIAL,
                tpd_cvo CHAR(1) NOT NULL,
                dco_cvo VARCHAR(30) NOT NULL,
                nme_cvo VARCHAR(50) NOT NULL,
                id_rva BIGINT NOT NULL,
                pre_cvo BOOLEAN DEFAULT FALSE NOT NULL,
                CONSTRAINT pk_convidado PRIMARY KEY (id_cvo)
);


CREATE TABLE public.chamado (
                id_cho BIGSERIAL,
                tit_cho VARCHAR(50) NOT NULL,
                dsc_cho VARCHAR(200) NOT NULL,
                id_cno BIGINT NOT NULL,
                sts_cho CHAR(12) NOT NULL,
                id_usr BIGINT NOT NULL,
                dt_cri DATE DEFAULT ('now'::text)::date NOT NULL,
                CONSTRAINT pk_chamado PRIMARY KEY (id_cho)
);

ALTER TABLE public.chamado ADD CONSTRAINT fk_usuario_chamado
FOREIGN KEY (id_usr)
REFERENCES public.usuario (id_usr)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.unidade ADD CONSTRAINT fk_usuario_unidade
FOREIGN KEY (id_usr)
REFERENCES public.usuario (id_usr)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario_papel ADD CONSTRAINT fk_usuario_usuario_papel
FOREIGN KEY (id_usr)
REFERENCES public.usuario (id_usr)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reserva ADD CONSTRAINT fk_usuario_reserva
FOREIGN KEY (id_usr)
REFERENCES public.usuario (id_usr)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario_papel ADD CONSTRAINT fk_papel_usuario_papel
FOREIGN KEY (id_ppl)
REFERENCES public.papel (id_ppl)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.chamado ADD CONSTRAINT fk_condominio_chamado
FOREIGN KEY (id_cno)
REFERENCES public.condominio (id_cno)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.area_comum ADD CONSTRAINT fk_condominio_area_comum
FOREIGN KEY (id_cno)
REFERENCES public.condominio (id_cno)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.unidade ADD CONSTRAINT fk_condominio_unidade
FOREIGN KEY (id_cno)
REFERENCES public.condominio (id_cno)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.enquete ADD CONSTRAINT fk_condominio_enquete
FOREIGN KEY (id_cno)
REFERENCES public.condominio (id_cno)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.item_enquete ADD CONSTRAINT fk_enquete_item_equente
FOREIGN KEY (id_eqt)
REFERENCES public.enquete (id_eqt)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.voto ADD CONSTRAINT fk_enquete_voto
FOREIGN KEY (id_eqt)
REFERENCES public.enquete (id_eqt)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.voto ADD CONSTRAINT fk_item_enquete_voto
FOREIGN KEY (id_ieq)
REFERENCES public.item_enquete (id_ieq)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.voto ADD CONSTRAINT fk_unidade_voto
FOREIGN KEY (id_und)
REFERENCES public.unidade (id_und)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.reserva ADD CONSTRAINT fk_area_comum_reserva
FOREIGN KEY (id_aco)
REFERENCES public.area_comum (id_aco)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.convidado ADD CONSTRAINT fk_reserva_convidado
FOREIGN KEY (id_rva)
REFERENCES public.reserva (id_rva)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

INSERT INTO usuario (nme_usr, sna_usr, unm_usr, msn_usr) VALUES ('morador', '$2a$10$6A6cew8H0m.Fg5sL3JKIcuCX9Vw1QzRReIDns8o9KL3CnQ0Wk6kZ6','morador@teste.com', false);
INSERT INTO usuario (nme_usr, sna_usr, unm_usr, msn_usr) VALUES ('sindico', '$2a$10$6A6cew8H0m.Fg5sL3JKIcuCX9Vw1QzRReIDns8o9KL3CnQ0Wk6kZ6','sindico@teste.com', true);
INSERT INTO usuario (nme_usr, sna_usr, unm_usr, msn_usr) VALUES ('funcionario', '$2a$10$6A6cew8H0m.Fg5sL3JKIcuCX9Vw1QzRReIDns8o9KL3CnQ0Wk6kZ6','funcionario@teste.com', true);
INSERT INTO condominio (cnpj_cno, nme_cno) VALUES ('111111111111111', 'Terrazzo Imperial');
INSERT INTO papel (cod_ppl, dsc_ppl) VALUES ('ROLE_SINDICO', 'Sindico do condominio');
INSERT INTO papel (cod_ppl, dsc_ppl) VALUES ('ROLE_MORADOR', 'Morador do condominio');
INSERT INTO papel (cod_ppl, dsc_ppl) VALUES ('ROLE_ADM', 'Administrador do Sistema');
INSERT INTO papel (cod_ppl, dsc_ppl) VALUES ('ROLE_PORTEIRO', 'Porteiro do condominio');
INSERT INTO usuario_papel (id_usr, id_ppl) VALUES (1, 2);
INSERT INTO usuario_papel (id_usr, id_ppl) VALUES (1, 3);
INSERT INTO usuario_papel (id_usr, id_ppl) VALUES (2, 1);
INSERT INTO usuario_papel (id_usr, id_ppl) VALUES (2, 2);
INSERT INTO unidade (idt_und, id_cno, id_usr) VALUES ('1601', 1, 1);
INSERT INTO unidade (idt_und, id_cno, id_usr) VALUES ('2402', 1, 2);