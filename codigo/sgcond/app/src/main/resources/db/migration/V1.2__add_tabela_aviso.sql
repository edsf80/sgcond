CREATE TABLE public.aviso (
                id_avs BIGSERIAL,
                id_cno BIGINT NOT NULL,
                tit_avs VARCHAR(50) NOT NULL,
                dsc_avs VARCHAR(200),
                dat_val TIMESTAMP NOT NULL,
                CONSTRAINT pk_aviso PRIMARY KEY (id_avs)
);

ALTER TABLE public.aviso ADD CONSTRAINT fk_aviso_condominio
FOREIGN KEY (id_cno)
REFERENCES public.condominio (id_cno)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;