import {Directive, HostBinding, Input, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';


@Directive({
  selector: '[appHasPermisssion]'
})
// Essa diretiva foi tirada da pasta do serviço auth por que os módulos filhos não devem importar o módulo de segurança
// e o uso dessa diretiva necessita o import do modulo ao qual ela pertence.
export class AuthDirective implements OnInit {

  @Input('appHasPermisssion')
  permissionRoles: string[];

  @HostBinding('style.display')
  visible = 'none';

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    const userRoles = this.authService.getUsuario().roles;

    if (userRoles === []) {
      return;
    }

    for (const permissao of this.permissionRoles) {
      for (const userRole of userRoles) {
        if (userRole === permissao) {
          this.visible = '';
        }
      }
    }
  }
}
