import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class SignoutService {

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

  signout(): Observable<boolean> {
    return new Observable(observer => {

      this.http.get<any>('api/logout').subscribe(resposta => {
        this.authService.clearSession();
        observer.next(true);
      }, erro => {
        observer.next(false);
      });
    });
  }
}
