import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './auth.service';
import {Injectable} from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService, private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authReq = req.clone();

    return next.handle(authReq).catch((error: HttpErrorResponse) => {
      if (error.status === 404) {
        this.router.navigate(['/sessions', '404']);
      } else if (error.status === 500) {
        this.router.navigate(['/sessions', 'error']);
      } else if (error.status === 403) {
        this.router.navigate(['/sessions', 'signin']);
      }

      return Observable.throwError(error);
    });
  }
}
