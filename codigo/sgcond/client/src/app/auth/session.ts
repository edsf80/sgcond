import {Usuario} from './usuario';

export class Session {

  private user: Usuario;

  attribute: any;

  get usuario(): Usuario {
    return Object.assign(new Usuario(), this.user);
  }

  atualizarUsuario(usuario: Usuario) {
    this.user = usuario;
  }
}
