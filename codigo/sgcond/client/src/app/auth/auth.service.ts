import {Session} from './session';
import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {HttpHeaders} from '@angular/common/http';
import {Usuario} from './usuario';

const sessionKey = 'currentSession';

@Injectable()
export class AuthService implements CanActivate {
  private session: Session;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.getSession()) {
      return true;
    }
    this.router.navigate(['/sessions/signin']);
    return false;
  }

  setSession(session: Session) {
    this.session = session;
    localStorage.setItem(sessionKey, JSON.stringify(this.session));
  }

  clearSession() {
    localStorage.removeItem(sessionKey);
    this.session = null;
  }

  getSession(): Session {
    if (!this.session) {
      const recover = localStorage.getItem('currentSession');
      if (recover) {
        this.session = new Session();
        this.session.atualizarUsuario(JSON.parse(recover).user);
      }
    }
    return this.session;
  }

  getUsuario(): Usuario {
    const sessao = this.getSession();

    return sessao ? sessao.usuario : null;
  }
}
