import {Routes} from '@angular/router';
import {AdminLayoutComponent} from './shared/components/layouts/admin-layout/admin-layout.component';
import {AuthLayoutComponent} from './shared/components/layouts/auth-layout/auth-layout.component';
import {AuthService} from './auth/auth.service';

export const rootRouterConfig: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'sessions',
        loadChildren: './views/sessions/sessions.module#SessionsModule',
        data: {title: 'Session'}
      }
    ]
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthService],
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule',
        data: { title: 'Dashboard', breadcrumb: 'DASHBOARD'}
      },
      {
        path: 'chamado',
        loadChildren: './views/chamado/chamado.module#ChamadoModule',
        data: { title: 'Chamados do Condomínio', breadcrumb: 'Chamados Cadastrados'}
      },
      {
        path: 'unidade',
        loadChildren: './views/unidade/unidade.module#UnidadeModule',
        data: { title: 'Unidades do Condomínio', breadcrumb: 'Unidades Cadastradas'}
      },
      {
        path: 'areacomum',
        loadChildren: './views/area-comum/area-comum-shared.module#AreaComumSharedModule',
        data: { title: 'Áreas Comuns do Condomínio', breadcrumb: 'Áreas Cadastradas'}
      },
      {
        path: 'aviso',
        loadChildren: './views/aviso/aviso.module#AvisoModule',
        data: { title: 'Avisos', breadcrumb: 'Avisos'}
      },
      {
        path: 'conta',
        loadChildren: './views/conta/conta.module#ContaModule',
        data: { title: 'Conta', breadcrumb: 'Conta'}
      },
      {
        path: 'usuario',
        loadChildren: './views/usuario/usuario.module#UsuarioModule',
        data: { title: 'Usuários', breadcrumb: 'Usuários'}
      },
      {
        path: 'enquete',
        loadChildren: './views/enquete/enquete.module#EnqueteModule',
        data: { title: 'Enquetes', breadcrumb: 'Enquetes'}
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'sessions/404'
  }
];

