import {Routes} from '@angular/router';
import {ChamadoComponent} from './chamado.component';
import {ChamadoChangeComponent} from './chamado-change/chamado-change.component';
import {ChamadoResolver} from './chamado-change/chamado-resolver.service';
import {ChamadoService} from './chamado.service';

export const ChamadoRoutes: Routes = [
  {path: '', component: ChamadoComponent, canActivate: [ChamadoService]},
  {path: 'add', component: ChamadoChangeComponent, data: {title: 'Novo Chamado', breadcrumb: 'Novo Chamado'}, canActivate: [ChamadoService]},
  {
    path: ':id',
    component: ChamadoChangeComponent,
    resolve: {chamado: ChamadoResolver},
    data: {title: 'Alterar Chamado', breadcrumb: 'Alterar Chamado'},
    canActivate: [ChamadoService]
  }
];
