import {Chamado} from './chamado';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class ChamadoService implements CanActivate {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_SINDICO' || role === 'ROLE_MORADOR') {
        resultado = true;
      }
    });

    if (!resultado) {
      this.router.navigate(['/', 'sessions', 'denied']);
    }

    return resultado;
  }

  buscarPorId(id: number): Observable<Chamado> {
    return this.http.get<Chamado>('api/chamado/' + id);
  }

  buscarTodos(): Observable<Chamado[]> {
    return this.http.get<Chamado[]>('api/chamado/condominio/1');
  }

  inserir(chamado: Chamado): Observable<Chamado> {
    return this.http.post<Chamado>('api/chamado', chamado);
  }

  alterar(id: number, chamado: Chamado): Observable<Chamado> {
    return this.http.put<Chamado>('api/chamado', chamado);
  }
}
