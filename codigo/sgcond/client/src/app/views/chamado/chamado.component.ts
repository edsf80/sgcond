import {Component, OnInit} from '@angular/core';
import {ChamadoService} from './chamado.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-chamado',
  templateUrl: './chamado.component.html'
})
export class ChamadoComponent implements OnInit {

  temp = [];
  rows = [];
  loaded = false;
  columns = [
    {
      prop: 'id'
    },
    {
      prop: 'titulo',
      name: 'Título'
    },
    {
      prop: 'status',
      name: 'Status'
    }
  ];

  constructor(private chamadoService: ChamadoService,
              private router: Router) {
  }

  ngOnInit() {
    this.chamadoService.buscarTodos().subscribe(
      resposta => {
        this.loaded = true;
        this.rows = this.temp = resposta;
      }, error2 => {
        this.router.navigate(['/', 'sessions', 'error']);
      }
    );
  }

  updateFilter(event) {
    if (this.temp.length < 1) {
      return;
    }

    const val = event.target.value.toLowerCase();
    const columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length) {
      return;
    }

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        const column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;
  }

  onSelect({selected}) {
    this.router.navigate(['/', 'chamado', selected[0].id]);
  }

  onClick() {
    this.router.navigate(['/', 'chamado', 'add']);
  }
}
