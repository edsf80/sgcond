import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChamadoChangeComponent } from './chamado-change.component';

describe('ChamadoChangeComponent', () => {
  let component: ChamadoChangeComponent;
  let fixture: ComponentFixture<ChamadoChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChamadoChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChamadoChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
