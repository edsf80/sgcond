import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Chamado} from '../chamado';
import {ChamadoService} from '../chamado.service';

@Injectable()
export class ChamadoResolver implements Resolve<Chamado> {
  constructor(private chamadoService: ChamadoService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Chamado> | Promise<Chamado> | Chamado {
    return this.chamadoService.buscarPorId(+route.params['id']);
  }
}
