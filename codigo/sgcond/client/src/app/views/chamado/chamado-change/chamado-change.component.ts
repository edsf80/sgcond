import {Component, OnInit} from '@angular/core';
import {ChamadoService} from '../chamado.service';
import {Chamado} from '../chamado';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-chamado-change',
  templateUrl: './chamado-change.component.html'
})
export class ChamadoChangeComponent implements OnInit {

  chamado: Chamado;
  edit = false;
  notificacao: Observable<boolean>;

  constructor(private chamadoService: ChamadoService, private router: Router,
              private route: ActivatedRoute, private loader: AppLoaderService,
              private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.chamado = data['chamado'];
        if (this.chamado !== undefined) {
          this.edit = true;

        } else {
          this.chamado = new Chamado();
          this.chamado.idCondominio = 1;
        }
      }
    );
  }

  onSalvar(chamado: Chamado) {
    this.notificacao = this.loader.open('Salvando dados');
    if (this.edit) {
      this.chamadoService.alterar(chamado.id, chamado).subscribe(
        resposta => this.posSalvar(resposta),
        error => this.posErro(error));
    } else {
      this.chamadoService.inserir(chamado).subscribe(
        resposta => this.posSalvar(resposta),
        error => this.posErro(error));
    }
  }

  posSalvar(resposta: Chamado) {
    this.chamado = resposta;
    this.notificacao.subscribe(res => {
      this.snack.open('Chamado salvo com sucesso!', 'OK', {duration: 4000});
      this.router.navigate(['/', 'chamado']);
    });
    this.loader.close();
  }

  posErro(erro: any) {
    this.loader.close();
    if (erro.status === 422) {
      this.snack.open(erro.error, 'OK', {duration: 4000});
    }
  }
}
