export class Chamado {

  id: number;

  descricao: string;

  titulo: string;

  status: string;

  transicoes: string[];

  idCondominio: number;

  arquivos?: string[];

  idCriador: number;

  constructor() {
    this.transicoes = ['NOVO'];
  }
}
