import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ChamadoRoutes} from './chamado.routing';
import {RouterModule} from '@angular/router';
import {ChamadoComponent} from './chamado.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {
  MatButtonModule, MatCardModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule,
  MatProgressSpinnerModule,
  MatSelectModule, MatSnackBarModule
} from '@angular/material';
import { ChamadoChangeComponent } from './chamado-change/chamado-change.component';
import {ChamadoService} from './chamado.service';
import {FormsModule} from '@angular/forms';
import {ChamadoResolver} from './chamado-change/chamado-resolver.service';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    SharedModule,
    MatSnackBarModule,
    RouterModule.forChild(ChamadoRoutes)
  ],
  declarations: [
    ChamadoComponent,
    ChamadoChangeComponent
  ],
  providers: [
    ChamadoService, ChamadoResolver
  ]
})
export class ChamadoModule { }
