import {Component, OnInit} from '@angular/core';
import {AvisoService} from '../aviso.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-aviso',
  templateUrl: './aviso.component.html'
})
export class AvisoComponent implements OnInit {

  temp = [];
  rows = [];
  loaded = false;
  columns = [
    {
      prop: 'id'
    },
    {
      prop: 'titulo',
      name: 'Título'
    },
    {
      prop: 'validade',
      name: 'Validade'
    }
  ];

  constructor(private avisoService: AvisoService, private router: Router) {
  }

  ngOnInit() {
    this.avisoService.buscarTodos().subscribe(
      resposta => {
        this.loaded = true;
        this.rows = this.temp = resposta;
      }
    );
  }

  updateFilter(event) {
    if (this.temp.length < 1) {
      return;
    }
    const val = event.target.value.toLowerCase();
    const columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length) {
      return;
    }

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        const column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;
  }

  onSelect({selected}) {
    this.router.navigate(['/', 'aviso', selected[0].id]);
  }

  onClick() {
    this.router.navigate(['/', 'aviso', 'add']);
  }
}
