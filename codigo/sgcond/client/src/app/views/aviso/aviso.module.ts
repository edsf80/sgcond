import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AvisoRoutes} from './aviso.routing';
import {AvisoComponent} from './aviso/aviso.component';
import {AvisoChangeComponent} from './aviso-change/aviso-change.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {
  DateAdapter, MAT_DATE_FORMATS,
  MatButtonModule,
  MatCardModule, MatDatepickerModule,
  MatDividerModule,
  MatInputModule, MatNativeDateModule,
  MatProgressSpinnerModule
} from '@angular/material';
import {AvisoService} from './aviso.service';
import {AvisoResolverService} from './aviso-change/aviso-resolver.service';
import {FormsModule} from '@angular/forms';
import {APP_DATE_FORMATS, DateFormat} from './aviso-change/data-format';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatNativeDateModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatDatepickerModule,
    SharedModule,
    RouterModule.forChild(AvisoRoutes)
  ],
  declarations: [AvisoComponent, AvisoChangeComponent],
  providers: [AvisoService, AvisoResolverService, {provide: DateAdapter, useClass: DateFormat},
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }]
})
export class AvisoModule {
}
