export class Aviso {

  id: number;

  titulo: string;

  descricao: string;

  validade: string;
}
