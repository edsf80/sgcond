import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Aviso} from '../aviso';
import {MatSnackBar} from '@angular/material';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {AvisoService} from '../aviso.service';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-aviso-change',
  templateUrl: './aviso-change.component.html'
})
export class AvisoChangeComponent implements OnInit {

  aviso: Aviso;
  edit = false;
  notificacao: Observable<boolean>;

  constructor(private avisoService: AvisoService, private router: Router,
              private route: ActivatedRoute, private loader: AppLoaderService,
              private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.aviso = data['aviso'];
        if (this.aviso !== undefined) {
          this.edit = true;
        } else {
          this.aviso = new Aviso();
        }
      }
    );
  }

  onSalvar(aviso: Aviso) {
    this.notificacao = this.loader.open('Salvando dados');
    if (this.edit) {
      this.avisoService.alterar(aviso.id, aviso).subscribe(
        resposta =>  this.posSalvar(resposta),
        error => this.posErro(error));
    } else {
      this.avisoService.inserir(aviso).subscribe(
        resposta => this.posSalvar(resposta),
        error => this.posErro(error));
    }
  }

  posSalvar(resposta: Aviso) {
    this.aviso = resposta;
    this.notificacao.subscribe(res => {
      this.snack.open('Aviso salvo com sucesso!', 'OK', { duration: 4000 });
      this.router.navigate(['/', 'aviso']);
    });
    this.loader.close();
  }

  posErro(erro: any) {
    this.loader.close();
    if (erro.status === 422) {
      this.snack.open(erro.error, 'OK', { duration: 4000 });
    }
  }
}
