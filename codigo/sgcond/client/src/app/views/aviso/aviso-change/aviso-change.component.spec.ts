import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvisoChangeComponent } from './aviso-change.component';

describe('AvisoChangeComponent', () => {
  let component: AvisoChangeComponent;
  let fixture: ComponentFixture<AvisoChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvisoChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvisoChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
