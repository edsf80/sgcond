import { TestBed, inject } from '@angular/core/testing';

import { AvisoResolverService } from './aviso-resolver.service';

describe('AvisoResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AvisoResolverService]
    });
  });

  it('should be created', inject([AvisoResolverService], (service: AvisoResolverService) => {
    expect(service).toBeTruthy();
  }));
});
