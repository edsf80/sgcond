import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Aviso} from '../aviso';
import {Observable} from 'rxjs/Observable';
import {AvisoService} from '../aviso.service';

@Injectable()
export class AvisoResolverService implements Resolve<Aviso> {

  constructor(private avisoService: AvisoService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Aviso> | Promise<Aviso> | Aviso {
    return this.avisoService.buscarPorId(+route.params['id']);
  }
}
