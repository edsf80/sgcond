import {Routes} from '@angular/router';
import {AvisoComponent} from './aviso/aviso.component';
import {AvisoService} from './aviso.service';
import {AvisoChangeComponent} from './aviso-change/aviso-change.component';
import {AvisoResolverService} from './aviso-change/aviso-resolver.service';

export const AvisoRoutes: Routes = [
  {path: '', component: AvisoComponent, canActivate: [AvisoService]},
  {path: 'add', component: AvisoChangeComponent, data: {title: 'Novo Aviso', breadcrumb: 'Novo Aviso'}, canActivate: [AvisoService]},
  {
    path: ':id', component: AvisoChangeComponent,
    resolve: {aviso: AvisoResolverService},
    data: {title: 'Alterar Aviso', breadcrumb: 'Alterar Aviso'},
    canActivate: [AvisoService]
  }
];
