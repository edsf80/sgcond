import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Aviso} from './aviso';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class AvisoService implements CanActivate {

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_SINDICO') {
        resultado = true;
      }
    });

    return resultado;
  }

  buscarPorId(id: number): Observable<Aviso> {
    return this.http.get<Aviso>('api/aviso/' + id);
  }

  buscarTodos():  Observable<Aviso[]> {
    return this.http.get<Aviso[]>('api/aviso');
  }

  inserir(aviso: Aviso): Observable<Aviso> {
    return this.http.post<Aviso>('api/aviso', aviso);
  }

  alterar(id: number, aviso: Aviso): Observable<Aviso> {
    return this.http.put<Aviso>('api/aviso/' + id, aviso);
  }

  buscarTodosUsuarios(): Observable<Aviso> {
    return this.http.get<Aviso>('api/aviso');
  }
}
