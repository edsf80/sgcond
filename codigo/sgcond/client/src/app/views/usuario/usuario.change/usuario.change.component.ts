import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {UsuarioService} from '../usuario.service';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Usuario} from '../usuario';
import {MatSnackBar} from '@angular/material';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';


@Component({
  selector: 'app-usuario-change',
  templateUrl: './usuario.change.component.html'
})
export class UsuarioChangeComponent implements OnInit {

  usuario: Usuario;
  edit = false;
  notificacao: Observable<boolean>;

  constructor(private usuarioService: UsuarioService, private router: Router,
              private route: ActivatedRoute, private loader: AppLoaderService,
              private snack: MatSnackBar) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.usuario = data['usuario'];
        if (this.usuario !== undefined) {
          this.edit = true;
        } else {
          this.usuario = new Usuario();
          this.usuario.mudarSenha = true;
        }
      }
    );
  }

  onSalvar(usuario: Usuario) {
    this.notificacao = this.loader.open('Salvando dados');
    if (this.edit) {
      this.usuarioService.alterar(usuario.id, usuario).subscribe(
        resposta =>  this.posSucesso(),
        error => this.posErro(error));
    } else {
      this.usuarioService.inserir(usuario).subscribe(
        resposta => this.posSucesso(),
        error => this.posErro(error));
    }
  }

  posSucesso() {
    this.notificacao.subscribe(resposta => {
      this.snack.open('Usuário salvo com sucesso!', 'OK', { duration: 4000 });
      this.router.navigate(['/', 'usuario']);
    });
    this.loader.close();
  }

  posErro(erro: any) {
    this.loader.close();
    if (erro.status === 422) {
      this.snack.open(erro.error, 'OK', { duration: 4000 });
    }
  }
}
