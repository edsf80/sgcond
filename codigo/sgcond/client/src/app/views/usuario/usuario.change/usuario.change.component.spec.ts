import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Usuario.ChangeComponent } from './usuario.change.component';

describe('Usuario.ChangeComponent', () => {
  let component: Usuario.ChangeComponent;
  let fixture: ComponentFixture<Usuario.ChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Usuario.ChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Usuario.ChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
