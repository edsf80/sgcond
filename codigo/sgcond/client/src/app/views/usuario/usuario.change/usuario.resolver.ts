import {Usuario} from '../usuario';
import {UsuarioService} from '../usuario.service';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UsuarioResolver implements Resolve<Usuario> {
  constructor(private usuarioService: UsuarioService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Usuario> | Promise<Usuario> | Usuario {
    return this.usuarioService.buscarPorId(+route.params['id']);
  }
}
