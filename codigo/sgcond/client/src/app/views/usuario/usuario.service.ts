import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Usuario} from './usuario';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class UsuarioService implements CanActivate {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_ADM') {
        resultado = true;
      }
    });

    if (!resultado) {
      this.router.navigate(['/', 'sessions', 'denied']);
    }

    return resultado;
  }

  buscarPorId(id: number): Observable<Usuario> {
    return this.http.get<Usuario>('api/usuario/' + id);
  }

  buscarTodos(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>('api/usuario');
  }

  inserir(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>('api/usuario', usuario);
  }

  alterar(id: number, usuario: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>('api/usuario/' + id, usuario);
  }
}
