import {Routes} from '@angular/router';
import {UsuarioComponent} from './usuario.component';
import {UsuarioService} from './usuario.service';
import {UsuarioChangeComponent} from './usuario.change/usuario.change.component';
import {UsuarioResolver} from './usuario.change/usuario.resolver';

export const UsuarioRoutes: Routes = [
  {path: '', component: UsuarioComponent, canActivate: [UsuarioService]},
  {path: 'add', component: UsuarioChangeComponent, data: {title: 'Novo Usuário', breadcrumb: 'Novo Usuário'}, canActivate: [UsuarioService]},
  {
    path: ':id',
    component: UsuarioChangeComponent,
    resolve: {usuario: UsuarioResolver},
    data: {title: 'Alterar Usuário', breadcrumb: 'Alterar Usuário'},
    canActivate: [UsuarioService]
  }
];
