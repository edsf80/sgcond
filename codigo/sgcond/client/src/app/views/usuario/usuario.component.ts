import { Component, OnInit } from '@angular/core';
import {UsuarioService} from './usuario.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html'
})
export class UsuarioComponent implements OnInit {

  temp = [];
  rows = [];
  loaded = false;
  columns = [
    {
      prop: 'nome',
      name: 'Nome'
    },
    {
      prop: 'username',
      name: 'E-Mail'
    }
  ];

  constructor(private usuarioService: UsuarioService,
              private router: Router) {
  }

  ngOnInit() {
    this.usuarioService.buscarTodos().subscribe(
      resposta => {
        this.loaded = true;
        this.rows = this.temp = resposta;
      }, error2 => {
        this.router.navigate(['/', 'sessions', 'error']);
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length) {
      return;
    }

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        const column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;
  }

  onSelect({selected}) {
    this.router.navigate(['/', 'usuario', selected[0].id]);
  }

  onClick() {
    this.router.navigate(['/', 'usuario', 'add']);
  }

}
