export class Usuario {

  id: number;

  nome: string;

  username: string;

  senha: string;

  mudarSenha: Boolean;
}
