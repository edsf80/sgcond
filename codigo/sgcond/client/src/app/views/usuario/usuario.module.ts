import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FormsModule} from '@angular/forms';
import {UsuarioComponent} from './usuario.component';
import {UsuarioService} from './usuario.service';
import {
  MatButtonModule, MatCardModule, MatCheckboxModule, MatDividerModule, MatInputModule,
  MatProgressSpinnerModule, MatSnackBarModule
} from '@angular/material';
import {UsuarioRoutes} from './usuario.routing';
import {RouterModule} from '@angular/router';
import {UsuarioChangeComponent} from './usuario.change/usuario.change.component';
import {UsuarioResolver} from './usuario.change/usuario.resolver';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatCheckboxModule,
    SharedModule,
    MatSnackBarModule,
    RouterModule.forChild(UsuarioRoutes)
  ],
  declarations: [UsuarioComponent, UsuarioChangeComponent],
  providers: [UsuarioService, UsuarioResolver]
})
export class UsuarioModule { }
