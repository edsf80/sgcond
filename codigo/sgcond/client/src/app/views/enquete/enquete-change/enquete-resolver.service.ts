import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {Enquete} from '../enquete';
import {EnqueteService} from '../enquete.service';

@Injectable()
export class EnqueteResolver implements Resolve<Enquete> {
  constructor(private enqueteService: EnqueteService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Enquete> | Promise<Enquete> | Enquete {
    return this.enqueteService.buscarPorId(+route.params['id']);
  }
}
