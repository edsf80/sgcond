import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnqueteChangeComponent } from './enquete-change.component';

describe('EnqueteChangeComponent', () => {
  let component: EnqueteChangeComponent;
  let fixture: ComponentFixture<EnqueteChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnqueteChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnqueteChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
