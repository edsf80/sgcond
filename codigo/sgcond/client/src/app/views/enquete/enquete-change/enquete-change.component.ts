import { Enquete } from '../enquete';
import { EnqueteService } from '../enquete.service';
import { ItemEnquete } from '../item-enquete';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-enquete-change',
  templateUrl: './enquete-change.component.html',
  styleUrls: ['./enquete-change.component.css']
})
export class EnqueteChangeComponent implements OnInit {

  enquete: Enquete;
  edit = false;
  columns = [
    {
      prop: 'descricao',
      name: 'Descrição'
    }
  ];

  loadingIndicator = false;
  reorderable = true;

  constructor(private enqueteService: EnqueteService,
    private router: Router, private route: ActivatedRoute,
    private loader: AppLoaderService) {

  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.enquete = data['enquete'];
        if (this.enquete !== undefined) {
          this.edit = true;

        } else {
          this.enquete = new Enquete();
          this.enquete.idCondominio = 1;
          this.enquete.itens = [];
        }

      }
    );
  }

  onAddItem(item: string) {
    this.loadingIndicator = true;
    const newItem = new ItemEnquete();
    newItem.descricao = item;
    // Esse passo abaixo foi necessário pra funcionar a atulização no ngx-datatable.
    this.enquete.itens = [...this.enquete.itens, newItem];
    this.loadingIndicator = false;
  }

  onDelItem(item: Enquete) {
    const index = this.enquete.itens.findIndex(
      (s) => s.id === item.id
    );
    this.enquete.itens.splice(index, 1);
  }

  onSalvar() {
    this.loader.open('Salvando enquete');
    if (this.edit) {
      this.enqueteService.alterar(this.enquete.id, this.enquete).subscribe(res => {
        this.posSalvar();
      }, error => this.loader.close());
    } else {
      this.enqueteService.inserir(this.enquete).subscribe(res => {
        this.posSalvar();
      }, error => this.loader.close());
    }
  }

  posSalvar() {
    this.loader.close();
    this.router.navigate(['/', 'enquete']);
  }
}
