import { ItemEnquete } from './item-enquete';

export class Enquete {
    id: number;

    titulo: string;

    status: string;

    idCondominio: number;

    itens: ItemEnquete[];
}
