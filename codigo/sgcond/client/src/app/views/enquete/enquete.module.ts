import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EnqueteRoutes} from './enquete.routing';
import {RouterModule} from '@angular/router';
import {EnqueteComponent} from './enquete.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {
  MatButtonModule, MatCardModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule,
  MatProgressSpinnerModule,
  MatSelectModule
} from '@angular/material';
import {EnqueteService} from './enquete.service';
import {FormsModule} from '@angular/forms';
import {EnqueteChangeComponent} from './enquete-change/enquete-change.component';
import {EnqueteResolver} from './enquete-change/enquete-resolver.service';
import { EnqueteResultadoComponent } from './enquete-resultado/enquete-resultado.component';
import {ChartsModule} from 'ng2-charts';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxDatatableModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    ChartsModule,
    SharedModule,
    RouterModule.forChild(EnqueteRoutes)
  ],
  declarations: [
    EnqueteComponent,
    EnqueteChangeComponent,
    EnqueteResultadoComponent
  ],
  providers: [
    EnqueteService,
    EnqueteResolver
  ]
})
export class EnqueteModule { }
