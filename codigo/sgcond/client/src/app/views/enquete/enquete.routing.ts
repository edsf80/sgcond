import { Routes } from '@angular/router';
import { EnqueteComponent } from './enquete.component';
import { EnqueteChangeComponent } from './enquete-change/enquete-change.component';
import { EnqueteResolver } from './enquete-change/enquete-resolver.service';
import {EnqueteResultadoComponent} from './enquete-resultado/enquete-resultado.component';
import {EnqueteService} from './enquete.service';

export const EnqueteRoutes: Routes = [
    { path: '', component: EnqueteComponent, canActivate: [EnqueteService] },
    {path: 'add', component: EnqueteChangeComponent, data: {title: 'Nova Enquete', breadcrumb: 'Nova Enquete'}},
    {
      path: ':id',
      component: EnqueteChangeComponent,
      resolve: {enquete: EnqueteResolver},
      data: {title: 'Alterar Enquete', breadcrumb: 'Alterar Enquete'}
    },
    {
      path: 'resultado/:id',
      component: EnqueteResultadoComponent,
      resolve: {enquete: EnqueteResolver},
      data: {title: 'Resultado da Enquete', breadcrumb: 'Resultado Enquete'},
      canActivate: [EnqueteService]
    }

];
