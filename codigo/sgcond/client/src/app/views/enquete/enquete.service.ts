import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Enquete} from './enquete';
import {Voto} from './voto';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class EnqueteService  implements CanActivate {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_SINDICO') {
        resultado = true;
      }
    });

    if (!resultado) {
      this.router.navigate(['/', 'sessions', 'denied']);
    }

    return resultado;
  }

  buscarPorId(id: number): Observable<Enquete> {
    return this.http.get<Enquete>('api/enquete/' + id);
  }

  buscarTodos(): Observable<Enquete[]> {
    return this.http.get<Enquete[]>('api/enquete');
  }

  inserir(enquete: Enquete): Observable<Enquete> {
    return this.http.post<Enquete>('api/enquete', enquete);
  }

  alterar(id: number, enquete: Enquete): Observable<Enquete> {
    return this.http.put<Enquete>('api/enquete', enquete);
  }

  buscarResutladoPorId(id: number): Observable<Voto[]> {
    return this.http.get<Voto[]>('api/enquete/resultado/' + id);
  }

}
