import { EnqueteService } from './enquete.service';
import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';

@Component({
  selector: 'app-enquete',
  templateUrl: './enquete.component.html'
})
export class EnqueteComponent implements OnInit {

  temp = [];
  rows = [];
  loaded = false;
  columns = [
    {
      prop: 'id'
    },
    {
      prop: 'titulo',
      name: 'Título'
    },
    {
      prop: 'status',
      name: 'Status'
    }
  ];

  constructor(private enqueteService: EnqueteService,
              private router: Router) {
  }

  ngOnInit() {
    this.enqueteService.buscarTodos().subscribe(
      resposta => {
        this.loaded = true;
        this.rows = this.temp = resposta;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length) {
      return;
    }

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        const column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;
  }

  onSelect({selected}) {
    this.router.navigate(['/', 'enquete', selected[0].id]);
  }

  onClick() {
    this.router.navigate(['/', 'enquete', 'add']);
  }

}
