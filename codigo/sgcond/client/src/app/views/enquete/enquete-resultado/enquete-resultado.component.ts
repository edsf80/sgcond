import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {EnqueteService} from '../enquete.service';
import {Enquete} from '../enquete';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-enquete-resultado',
  templateUrl: './enquete-resultado.component.html'
})
export class EnqueteResultadoComponent implements OnInit {

  enquete: Enquete;
  rows = [];

  sharedChartOptions: any = {
    responsive: true,
    // maintainAspectRatio: false,
    legend: {
      display: false,
      position: 'bottom'
    }
  };

  // Doughnut
  doughnutChartColors: any[] = [{
    backgroundColor: ['#f44336', '#3f51b5', '#ffeb3b', '#4caf50', '#2196f']
  }];
  doughnutChartLabels: string[] = [];
  doughnutChartData: number[] = [];
  doughnutChartType = 'doughnut';
  doughnutOptions: any = Object.assign({
    elements: {
      arc: {
        borderWidth: 0
      }
    }
  }, this.sharedChartOptions);

  constructor(private enqueteService: EnqueteService,
              private router: Router, private route: ActivatedRoute,
              private loader: AppLoaderService) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.enquete = data['enquete'];
        if (this.enquete === undefined) {
          this.router.navigate(['/', 'sessions', '404']);
        } else {
          let valores = [];

          this.enquete.itens.forEach((item, i) => {
            this.doughnutChartLabels = [...this.doughnutChartLabels, item.descricao];
            valores = [...valores, 0];
          });

          this.enqueteService.buscarResutladoPorId(this.enquete.id).subscribe(votos => {
            votos.forEach((voto, i, array) => {
              this.rows = [...this.rows, {
                unidade: voto.unidade.identificador,
                item: voto.itemEnquete.descricao,
                comentario: voto.comentario
              }];

              this.doughnutChartLabels.forEach((label, j) => {
                if (voto.itemEnquete.descricao === label) {
                  valores[j]++;
                }
              });
            });

            valores.forEach((label, j) => {
                this.doughnutChartData = [...this.doughnutChartData, ((label / votos.length) * 100)];
            });
          });
        }
      }
    );
  }
}
