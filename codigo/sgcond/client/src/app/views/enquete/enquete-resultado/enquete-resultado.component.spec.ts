import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnqueteResultadoComponent } from './enquete-resultado.component';

describe('EnqueteResultadoComponent', () => {
  let component: EnqueteResultadoComponent;
  let fixture: ComponentFixture<EnqueteResultadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnqueteResultadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnqueteResultadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
