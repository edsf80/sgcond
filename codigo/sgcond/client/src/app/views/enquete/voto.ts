import {ItemEnquete} from './item-enquete';
import {Unidade} from '../unidade/unidade';

export class Voto {

  unidade: Unidade;

  itemEnquete: ItemEnquete;

  comentario: string;
}
