import {Condominio} from '../condominio/condominio';

export class AreaComum {

  id: number;

  descricao: string;

  condominio: Condominio;
}
