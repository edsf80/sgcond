import { AreaComumSharedModule } from '../area-comum-shared.module';
import { ReservaComponent } from './reserva.component';
import { ReservaRoutes } from './reserva.routing';
import { ReservaService } from './reserva.service';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatIconModule, MatDialogModule} from '@angular/material';
import { RouterModule } from '@angular/router';
import { CalendarModule } from 'angular-calendar';

@NgModule({
  imports: [
    AreaComumSharedModule,
    MatIconModule,
    MatDialogModule,
    FlexLayoutModule,
    CalendarModule.forRoot(),
    RouterModule.forChild(ReservaRoutes)
  ],
  declarations: [ReservaComponent],
  providers: [ReservaService]
})
export class ReservaModule { }
