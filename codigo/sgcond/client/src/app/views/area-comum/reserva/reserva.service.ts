import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Reserva} from './reserva';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../../../auth/auth.service';

@Injectable()
export class ReservaService implements CanActivate {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_SINDICO' || role === 'ROLE_MORADOR') {
        resultado = true;
      }
    });

    if (!resultado) {
      this.router.navigate(['/', 'sessions', 'denied']);
    }

    return resultado;
  }

  buscarTodos(): Observable<Reserva[]> {
    return this.http.get<Reserva[]>('api/reserva');
  }

  salvar(reserva: Reserva): Observable<Reserva> {
    return this.http.post<Reserva>('api/reserva', reserva);
  }

  atualizar(reserva: Reserva): Observable<Reserva> {
    return this.http.put<Reserva>('api/reserva/' + reserva.id, reserva);
  }

  remover(id: number): Observable<string> {
    return this.http.delete<string>('api/reserva/' + id);
  }
}
