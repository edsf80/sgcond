import {AreaComum} from '../area-comum';
import 'rxjs/add/operator/map';
import {AreaComumService} from '../area-comum.service';
import {Component, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent} from 'angular-calendar';
import {Subject} from 'rxjs/Subject';
import {MatDialog, MatSnackBar} from '@angular/material';
import {
  startOfDay,
  isSameDay,
  isSameMonth,
  addMinutes,
  differenceInMinutes,
  parse,
  format,
  getHours,
  getMinutes
} from 'date-fns';
import {ReservaService} from './reserva.service';
import {Reserva} from './reserva';
import {AppConfirmService} from '../../../shared/services/app-confirm/app-confirm.service';
import {AuthService} from '../../../auth/auth.service';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html'
})
export class ReservaComponent implements OnInit {
  view = 'month';
  viewDate = new Date();
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  areasComuns: AreaComum[];
  dialogRef;
  horasTermino = [];
  reserva: Reserva;
  horasInicio = [];
  edit = false;

  modalData: {
    formData: {
      titulo: string;
      data: Date;
      id: number;
      horaInicio: number;
      horaTermino: number;
      idAreaComum: number;
    },
    date: Date
  };

  colors: any = {
    red: {
      primary: '#f44336',
      secondary: '#FAE3E3'
    },
    blue: {
      primary: '#247ba0',
      secondary: '#D1E8FF'
    },
    yellow: {
      primary: '#ffd97d',
      secondary: '#FDF1BA'
    },
    green: {
      primary: '#00ff00',
      secondary: '#e6ffe6'
    }
  };

  actions: CalendarEventAction[] = [{
    label: '<i class="material-icons icon-sm">edit</i>',
    onClick: ({event}: { event: CalendarEvent }): void => {
      this.handleEvent(event.start, event);
    }
  }, {
    label: '<i class="material-icons icon-sm">close</i>',
    onClick: ({event}: { event: CalendarEvent }): void => {
      this.confirm.confirm({title: 'Confirmação', message: 'Deseja realmente excluir a reserva?'}).subscribe(ok => {
        if (ok) {
          this.loader.open('Excluíndo reserva');
          this.reservaService.remover(event.meta.id).subscribe(resultado => {
            let index = 0;
            for (const item of this.events) {
              if (item.meta.id === event.meta.id) {
                break;
              }
              index++;
            }
            this.events.splice(index, 1);
            this.refresh.next();
            this.loader.close();
            this.snack.open('Reserva excluída com sucesso!', 'OK', {duration: 4000});
          }, erro => {
            this.loader.close();
            if (erro.status === 422) {
              this.snack.open(erro.error, 'OK', {duration: 4000});
            }
          });
        }
      });
    }
  }];

  refresh: Subject<any> = new Subject();

  activeDayIsOpen = true;

  events: CalendarEvent<Reserva>[];

  constructor(public dialogBox: MatDialog, private reservaService: ReservaService,
              private areaComumService: AreaComumService, private authService: AuthService,
              private confirm: AppConfirmService, private loader: AppLoaderService,
              private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.horasInicio = this.horasTermino =
      ['00:00', '00:30', '01:00', '01:30',
        '02:00', '02:30', '03:00', '03:30',
        '04:00', '04:30', '05:00', '05:30',
        '06:00', '06:30', '07:00', '07:30',
        '08:00', '08:30', '09:00', '09:30',
        '10:00', '10:30', '11:00', '11:30',
        '12:00', '12:30', '13:00', '13:30',
        '14:00', '14:30', '15:00', '15:30',
        '16:00', '16:30', '17:00', '17:30',
        '18:00', '18:30', '19:00', '19:30',
        '20:00', '20:30', '21:00', '21:30',
        '22:00', '22:30', '23:00', '23:30',
        '23:59'];

    this.reservaService.buscarTodos().map(
      (reservas: Reserva[]) => {
        return reservas.map((reserva: Reserva) => {
            return {
              start: parse(reserva.dataHoraInicio),
              end: parse(reserva.dataHoraTermino),
              title: reserva.dono.nome,
              color: this.getColor(reserva.areaComum.id),
              actions: this.actions,
              resizable: {
                beforeStart: true,
                afterEnd: true
              },
              draggable: true,
              meta: reserva
            };
          }
        );
      }
    ).subscribe(eventos => {
        this.events = eventos;
      }
    );

    this.areaComumService.buscarTodos().subscribe(resposta => {
      this.areasComuns = resposta;
    });
  }

  getColor(id: number) {
    let color: any;

    switch (id) {
      case 1:
        color = this.colors.green;
        break;
      case 2:
        color = this.colors.red;
        break;
      case 3:
        color = this.colors.blue;
        break;
      case 4:
        color = this.colors.yellow;
        break;
      default:
        color = this.colors.green;
        break;
    }

    return color;
  }

  handleEvent(date: Date, event: CalendarEvent): void {
    let formData: any;

    if (event) {
      const dataHoraInicio: Date = parse(event.meta.dataHoraInicio);
      const dataHoraTermino: Date = parse(event.meta.dataHoraTermino);
      const indiceInicio = (getHours(dataHoraInicio) * 2) + (getMinutes(dataHoraInicio) > 0 ? 1 : 0);
      this.horasTermino = this.horasInicio.filter((item, index) => {
        return (indiceInicio !== -1 && index >= indiceInicio);
      });
      const indiceTermino = differenceInMinutes(dataHoraTermino, dataHoraInicio) / 30;

      formData = {
        titulo: 'Editar Evento',
        data: event.start,
        id: event.meta.id,
        horaInicio: indiceInicio,
        horaTermino: indiceTermino,
        idAreaComum: event.meta.areaComum.id
      };
      this.edit = true;
    } else {
      formData = {
        titulo: 'Adicionar Evento',
        data: date,
        id: 0,
        horaInicio: 0,
        horaTermino: 0,
        idAreaComum: 0
      };
      this.edit = false;
    }

    this.modalData = {formData: formData, date: date};
    this.dialogRef = this.dialogBox.open(this.modalContent);
  }

  dayClicked({date, events}: { date: Date, events: CalendarEvent[] }): void {
    this.handleEvent(date, undefined);
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent(new Date(), event);
    this.refresh.next();
  }

  closeDialog() {
    this.dialogBox.closeAll();
  }

  onHoraInicioChange(event) {
    const indice = event.value;
    this.horasTermino = this.horasInicio.filter((item, index) => {
      return (indice !== -1 && index >= indice);
    });
  }

  onSalvar(formValue: any) {
    const reserva: Reserva = new Reserva();
    reserva.id = formValue.id;
    reserva.areaComum = new AreaComum();
    reserva.areaComum.id = formValue.areaComum;
    reserva.dataHoraInicio = format(addMinutes(startOfDay(this.modalData.date), formValue.horaInicio * 30), 'YYYY-MM-DD HH:mm');
    reserva.dataHoraTermino = format(addMinutes(reserva.dataHoraInicio, formValue.horaTermino * 30), 'YYYY-MM-DD HH:mm');
    reserva.dono = this.authService.getUsuario();
    console.log(JSON.stringify(reserva));

    if (this.edit) {
      this.reservaService.atualizar(reserva).subscribe(resposta => {
        this.events.forEach(event => {
          if (event.meta.id === resposta.id) {
            event.start = parse(resposta.dataHoraInicio);
            event.end = parse(resposta.dataHoraTermino);
            event.color = this.getColor(resposta.areaComum.id);
            event.meta = resposta;
          }
        });
        this.refresh.next();
        this.closeDialog();
        this.snack.open('Reserva atualizada com sucesso!', 'OK', {duration: 4000});
      }, error => {
        if (error.status === 422) {
          this.snack.open(error.error, 'OK', {duration: 4000});
        } else {
          this.closeDialog();
        }
      });
    } else {
      this.reservaService.salvar(reserva).subscribe(resposta => {
        const evento = {
          start: parse(resposta.dataHoraInicio),
          end: parse(resposta.dataHoraTermino),
          title: resposta.dono.nome,
          color: this.getColor(resposta.areaComum.id),
          actions: this.actions,
          resizable: {
            beforeStart: true,
            afterEnd: true
          },
          draggable: true,
          meta: resposta
        };
        this.events.push(evento);
        this.refresh.next();
        this.closeDialog();
        this.snack.open('Reserva registrada com sucesso!', 'OK', {duration: 4000});
      }, error => {
        if (error.status === 422) {
          this.snack.open(error.error, 'OK', {duration: 4000});
        } else {
          this.closeDialog();
        }
      });
    }
  }
}
