import { ReservaComponent } from './reserva.component';
import { Routes } from '@angular/router';
import {ReservaService} from './reserva.service';

export const ReservaRoutes: Routes = [
  { path: '', component: ReservaComponent, data: { title: 'Reserva de Áreas Comuns' }, canActivate: [ReservaService] }
];
