import {AreaComum} from '../area-comum';
import {Usuario} from '../../../auth/usuario';

export class Reserva {

  id: number;

  dataHoraInicio: string;

  dataHoraTermino: string;

  horaInicio: string;

  horaTermino: string;

  dono: Usuario;

  areaComum: AreaComum;

  status: string;
}
