import {Component, OnInit} from '@angular/core';
import {AreaComum} from '../../area-comum';
import {AreaComumService} from '../../area-comum.service';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Condominio} from '../../../condominio/condominio';
import {MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs/Observable';
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-area-comum-change',
  templateUrl: './area-comum-change.component.html'
})
export class AreaComumChangeComponent implements OnInit {

  areaComum: AreaComum;
  edit = false;
  notificacao: Observable<boolean>;

  constructor(private areaComumService: AreaComumService,
    private router: Router, private route: ActivatedRoute,
    private loader: AppLoaderService,
    private snack: MatSnackBar) {}

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.areaComum = data['areaComum'];
        if (this.areaComum !== undefined) {
          this.edit = true;

        } else {
          const condominio: Condominio = new Condominio();
          condominio.id = 1;
          this.areaComum = new AreaComum();
          this.areaComum.condominio = condominio;
        }
      }
    );
  }

  onSalvar() {
    this.notificacao = this.loader.open('Salvando dados');

    if (this.edit) {
      this.areaComumService.alterar(this.areaComum.id, this.areaComum).subscribe(
        resposta => this.posSalvar(resposta), error => this.posErro(error)
      );
    } else {
      this.areaComumService.inserir(this.areaComum).subscribe(
        resposta => this.posSalvar(resposta), error => this.posErro(error)
      );
    }
  }

  posSalvar(resposta: AreaComum) {
    this.areaComum = resposta;
    this.notificacao.subscribe(res => {
      this.snack.open('Área comum salva com sucesso!', 'OK', { duration: 4000 });
      this.router.navigate(['/', 'areacomum', 'cadastro']);
    })
    this.loader.close();
  }

  posErro(erro: any) {
    this.loader.close();
    if (erro.status === 422) {
      this.snack.open(erro.error, 'OK', { duration: 4000 });
    }
  }
}
