import { AreaComumChangeComponent } from './area-comum-change/area-comum-change.component';
import { AreaComumResolverService } from './area-comum-change/area-comum-resolver.service';
import { AreaComumComponent } from './area-comum.component';
import {Routes} from '@angular/router';
import {AreaComumService} from '../area-comum.service';

export const AreaComumRoutes: Routes = [
  {path: '', component: AreaComumComponent, canActivate: [AreaComumService]},
  {
    path: 'add',
    component: AreaComumChangeComponent,
    data: {title: 'Nova Área Comum', breadcrumb: 'Nova Área'},
    canActivate: [AreaComumService]
  },
  {
    path: ':id',
    component: AreaComumChangeComponent,
    resolve: {areaComum: AreaComumResolverService},
    data: {title: 'Alterar Área Comum', breadcrumb: 'Alterar Área'},
    canActivate: [AreaComumService]
  }
];
