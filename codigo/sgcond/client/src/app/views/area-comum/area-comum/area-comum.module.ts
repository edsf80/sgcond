import { AreaComumSharedModule } from '../area-comum-shared.module';
import { NgModule } from '@angular/core';
import {AreaComumComponent} from './area-comum.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { AreaComumChangeComponent } from './area-comum-change/area-comum-change.component';
import {AreaComumResolverService} from './area-comum-change/area-comum-resolver.service';
import { AreaComumRoutes } from './area-comum.routing';
import { RouterModule } from '@angular/router';


@NgModule({
  imports: [
    AreaComumSharedModule,
    NgxDatatableModule,
    RouterModule.forChild(AreaComumRoutes)
  ],
  declarations: [AreaComumComponent, AreaComumChangeComponent],
  providers: [AreaComumResolverService]
})
export class AreaComumModule { }
