import {Component, OnInit} from '@angular/core';
import {AreaComumService} from '../area-comum.service';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../auth/auth.service';

@Component({
  selector: 'app-area-comum',
  templateUrl: './area-comum.component.html'
})
export class AreaComumComponent implements OnInit {

  temp = [];
  rows = [];
  columns = [
    {
      prop: 'id'
    },
    {
      prop: 'descricao',
      name: 'Descrição'
    }
  ];
  loaded = false;

  constructor(private areaComumService: AreaComumService,
              private auth: AuthService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.areaComumService.buscarTodos().subscribe(
      resposta => {
        this.rows = this.temp = resposta;
      }
    );
    this.loaded = true;
  }

  updateFilter(event) {
    if (this.temp.length < 1) {
      return;
    }

    const val = event.target.value.toLowerCase();
    const columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length) {
      return;
    }

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        const column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;
  }

  onSelect({selected}) {
    this.router.navigate([selected[0].id], {relativeTo: this.route});
  }

  onClick() {
    this.router.navigate(['add'], {relativeTo: this.route});
  }
}
