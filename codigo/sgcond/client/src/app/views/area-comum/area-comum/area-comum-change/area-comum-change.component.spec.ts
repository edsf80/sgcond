import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaComumChangeComponent } from './area-comum-change.component';

describe('AreaComumChangeComponent', () => {
  let component: AreaComumChangeComponent;
  let fixture: ComponentFixture<AreaComumChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaComumChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaComumChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
