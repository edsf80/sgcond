import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AreaComumService} from '../../area-comum.service';
import {AreaComum} from '../../area-comum';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class AreaComumResolverService implements Resolve<AreaComum> {

  constructor(private areaComumService: AreaComumService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AreaComum> | Promise<AreaComum> | AreaComum {
    return this.areaComumService.buscarPorId(+route.params['id']);
  }
}
