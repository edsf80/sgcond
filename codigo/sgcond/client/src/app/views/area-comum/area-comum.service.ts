
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AreaComum} from './area-comum';
import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../../auth/auth.service';

@Injectable()
export class AreaComumService implements CanActivate {

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_SINDICO') {
        resultado = true;
      }
    });

    if (!resultado) {
      this.router.navigate(['/', 'sessions', 'denied']);
    }

    return resultado;
  }

  buscarTodos(): Observable<AreaComum[]> {
    return this.http.get<AreaComum[]>('api/areacomum');
  }

  buscarPorId(id: number): Observable<AreaComum> {
    return this.http.get<AreaComum>('api/areacomum/' + id);
  }

  inserir(areaComum: AreaComum): Observable<AreaComum> {
    return this.http.post<AreaComum>('api/areacomum', areaComum);
  }

  alterar(id: number, areaComum: AreaComum): Observable<AreaComum> {
    return this.http.put<AreaComum>('api/areacomum/' + id, areaComum);
  }
}
