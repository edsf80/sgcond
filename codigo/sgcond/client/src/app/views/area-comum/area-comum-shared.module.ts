import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {
  MatButtonModule, MatCardModule, MatDividerModule, MatInputModule, MatProgressSpinnerModule,
  MatSelectModule, MatSnackBarModule
} from '@angular/material';
import {AreaComumService} from './area-comum.service';
import {AreaComumSharedRoutes} from './area-comum-shared.routing';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatSelectModule,
    MatDividerModule,
    MatSnackBarModule,
    SharedModule,
    RouterModule.forChild(AreaComumSharedRoutes)
  ],
  exports: [
    CommonModule,
    FormsModule,
    MatInputModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDividerModule,
    MatSelectModule,
    SharedModule
  ],
  declarations: [],
  providers: [AreaComumService]
})
export class AreaComumSharedModule { }
