import {Routes} from '@angular/router';

export const AreaComumSharedRoutes: Routes = [
  {path: 'cadastro', loadChildren: './area-comum/area-comum.module#AreaComumModule'},
  {path: 'reserva', loadChildren: './reserva/reserva.module#ReservaModule'}
];
