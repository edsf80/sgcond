import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Unidade} from '../unidade';
import {Observable} from 'rxjs/Observable';
import {UnidadeService} from '../unidade.service';

@Injectable()
export class UnidadeResolverService implements Resolve<Unidade> {

  constructor(private unidadeService: UnidadeService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Unidade> | Promise<Unidade> | Unidade {
    return this.unidadeService.buscarPorId(+route.params['id']);
  }

}
