import { TestBed, inject } from '@angular/core/testing';

import { UnidadeResolverService } from './unidade-resolver.service';

describe('UnidadeResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UnidadeResolverService]
    });
  });

  it('should be created', inject([UnidadeResolverService], (service: UnidadeResolverService) => {
    expect(service).toBeTruthy();
  }));
});
