import {Component, OnInit} from '@angular/core';
import {Unidade} from '../unidade';
import {UnidadeService} from '../unidade.service';
import {ActivatedRoute, Data, Router} from '@angular/router';
import {Observable} from 'rxjs/Rx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';
import {Usuario} from '../../../auth/usuario';
import {UsuarioService} from '../../usuario/usuario.service';


@Component({
  selector: 'app-unidade-change',
  templateUrl: './unidade-change.component.html'
})
export class UnidadeChangeComponent implements OnInit {

  edit = false;
  notificacao: Observable<boolean>;
  usuarios: Usuario[];
  options = [];
  frmUnidade: FormGroup;

  constructor(private unidadeService: UnidadeService, private route: ActivatedRoute,
              private loader: AppLoaderService, private usuarioService: UsuarioService,
              private fb: FormBuilder, private snack: MatSnackBar,
              private router: Router) {
    this.frmUnidade = this.fb.group({
      id: [0, Validators.required],
      identificador: ['', Validators.required],
      usuario: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        const unidade: Unidade = data['unidade'];
        if (unidade !== undefined) {
          this.edit = true;
          this.frmUnidade.setValue({
            id: unidade.id,
            identificador: unidade.identificador,
            usuario: unidade.usuario
          });
        }
      }
    );

    this.usuarioService.buscarTodos().subscribe(resposta => {
      this.usuarios = this.options = resposta;
    });

    this.frmUnidade.get('usuario').valueChanges.subscribe(value => {
      this.options = this.filter(value);
    });
  }

  filter(val: any): Usuario[] {
    const realval = val && typeof val === 'object' ? val.nome : val;

    return this.usuarios.filter(option => option.nome.toLowerCase().indexOf(realval.toLowerCase()) === 0);
  }

  onSalvar(unidade: Unidade) {
    this.notificacao = this.loader.open('Salvando dados');
    if (this.edit) {
      this.unidadeService.alterar(unidade.id, unidade).subscribe(
        resposta => this.posSucesso(resposta),
        error => this.posErro(error));
    } else {
      this.unidadeService.inserir(unidade).subscribe(
        resposta => this.posSucesso(resposta),
        error => this.posErro(error));
    }
  }

  posErro(erro: any) {
    this.loader.close();
    if (erro.status === 422) {
      this.snack.open(erro.error, 'OK', { duration: 4000 });
    }
  }

  posSucesso(resposta: Unidade) {
    this.notificacao.subscribe(res => {
      this.snack.open('Unidade salva com sucesso!', 'OK', { duration: 4000 });
      this.router.navigate(['/', 'unidade']);
    });
    this.loader.close();
  }

  displayFn(usuario?: Usuario): string | undefined {
    return usuario ? usuario.nome : undefined;
  }
}
