import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidadeChangeComponent } from './unidade-change.component';

describe('UnidadeChangeComponent', () => {
  let component: UnidadeChangeComponent;
  let fixture: ComponentFixture<UnidadeChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnidadeChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnidadeChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
