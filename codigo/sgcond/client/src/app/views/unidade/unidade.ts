import {Usuario} from '../../auth/usuario';

export class Unidade {

  id: number;

  identificador: string;

  usuario: Usuario;
}
