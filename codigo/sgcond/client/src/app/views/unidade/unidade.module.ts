import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UnidadeComponent} from './unidade.component';
import {RouterModule} from '@angular/router';
import {UnidadeRoutes} from './unidade.routing';
import {UnidadeService} from './unidade.service';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatDividerModule,
  MatInputModule,
  MatProgressSpinnerModule, MatSnackBarModule
} from '@angular/material';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {UnidadeChangeComponent} from './unidade-change/unidade-change.component';
import {ReactiveFormsModule} from '@angular/forms';
import {UnidadeResolverService} from './unidade-change/unidade-resolver.service';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    SharedModule,
    RouterModule.forChild(UnidadeRoutes)
  ],
  declarations: [
    UnidadeComponent,
    UnidadeChangeComponent
  ],
  providers: [
    UnidadeService, UnidadeResolverService
  ]
})
export class UnidadeModule {
}
