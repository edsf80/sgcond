import {Component, OnInit} from '@angular/core';
import {UnidadeService} from './unidade.service';
import {Router} from '@angular/router';
import {Unidade} from './unidade';

@Component({
  selector: 'app-unidade',
  templateUrl: './unidade.component.html'
})
export class UnidadeComponent implements OnInit {

  temp = [];
  rows = [];
  loaded = false;
  columns = [
    {
      prop: 'id'
    },
    {
      prop: 'identificador',
      name: 'Identificador'
    },
    {
      prop: 'usuario',
      name: 'Usuário'
    }
  ];

  constructor(private unidadeService: UnidadeService, private router: Router) {
  }

  ngOnInit() {
    this.unidadeService.buscarTodos().subscribe(
      resposta => {
        this.loaded = true;
        this.rows = this.temp = resposta.map((unidade: Unidade) => {
          return {id: unidade.id, identificador: unidade.identificador, usuario: unidade.usuario.nome}
        });
      }
    );
  }

  updateFilter(event) {
    if (this.temp.length < 1) {
      return;
    }

    const val = event.target.value.toLowerCase();
    const columns = Object.keys(this.temp[0]);

    if (!columns.length) {
      return;
    }

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        const column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;
  }

  onSelect({selected}) {
    this.router.navigate(['/', 'unidade', selected[0].id]);
  }

  onClick() {
    this.router.navigate(['/', 'unidade', 'add']);
  }

}
