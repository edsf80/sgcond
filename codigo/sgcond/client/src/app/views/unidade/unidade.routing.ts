import {Routes} from '@angular/router';
import {UnidadeComponent} from './unidade.component';
import {UnidadeChangeComponent} from './unidade-change/unidade-change.component';
import {UnidadeResolverService} from './unidade-change/unidade-resolver.service';
import {UnidadeService} from './unidade.service';


export const UnidadeRoutes: Routes = [
  {path: '', component: UnidadeComponent, canActivate: [UnidadeService]},
  {path: 'add', component: UnidadeChangeComponent, data: {title: 'Nova Unidade', breadcrumb: 'Nova Unidade'}, canActivate: [UnidadeService]},
  {
    path: ':id', component: UnidadeChangeComponent,
    resolve: {unidade: UnidadeResolverService},
    data: {title: 'Alterar Unidade', breadcrumb: 'Alterar Unidade'},
    canActivate: [UnidadeService]
  }
];
