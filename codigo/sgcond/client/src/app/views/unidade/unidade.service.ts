import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Unidade} from './unidade';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {Usuario} from '../../auth/usuario';


@Injectable()
export class UnidadeService implements CanActivate {


  constructor(private http: HttpClient, private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let resultado = false;

    this.authService.getUsuario().roles.forEach(role => {
      if (role === 'ROLE_ADM') {
        resultado = true;
      }
    });

    if (!resultado) {
      this.router.navigate(['/', 'sessions', 'denied']);
    }

    return resultado;
  }

  buscarPorId(id: number): Observable<Unidade> {
    return this.http.get<Unidade>('api/unidade/' + id);
  }

  buscarTodos(): Observable<Unidade[]> {
    return this.http.get<Unidade[]>('api/unidade');
  }

  inserir(unidade: Unidade): Observable<Unidade> {
    return this.http.post<Unidade>('api/unidade', unidade);
  }

  alterar(id: number, unidade: Unidade): Observable<Unidade> {
    return this.http.put<Unidade>('api/unidade/' + id, unidade);
  }

  buscarTodosUsuarios(): Observable<Usuario> {
    return this.http.get<Usuario>('api/usuario');
  }
}
