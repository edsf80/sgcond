import {Component, OnInit, ViewChild} from '@angular/core';
import {MatProgressBar, MatButton, MatSnackBar} from '@angular/material';
import {Validators, FormGroup, FormControl, ValidationErrors} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {SigninService} from './signin.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html'
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signinForm: FormGroup;

  submitted = false;

  mensagens_validacao = {
    'username': [
      {type: 'required', message: 'Login é requerido', map: 'NotEmpty'},
      {type: 'minlength', message: 'Login deve ter pelo menos 3 caracteres', map: 'Length'},
      {type: 'maxlength', message: 'Login não deve ter mais de 50 caracteres', map: 'Length'},
      {type: 'email', message: 'Formato de e-mail inválido', map: 'Email'}
    ],
    'senha': [
      {type: 'required', message: 'Senha é requerida', map: 'NotEmpty'},
      {type: 'maxlength', message: 'Login não deve ter mais de 50 caracteres', map: 'Length'}
    ]
  };

  constructor(private signinService: SigninService, private router: Router, private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.signinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      senha: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      rememberMe: new FormControl(false)
    });
  }

  signin() {
    this.submitted = true;
    const signinData = this.signinForm.value;
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    this.signinService.signin(signinData).subscribe(resposta => this.posSalvar(resposta));
  }

  posSalvar(resposta: any) {
    if (resposta instanceof HttpErrorResponse) {
      this.posErro(resposta);
    } else {
      this.router.navigate(['/']);
    }
  }

  posErro(erro: any) {
    this.progressBar.mode = 'determinate';
    this.progressBar.value = 100;
    this.submitButton.disabled = false;
    if (erro.status === 422) {
      let erroFormulario = false;
      const fieldErrors = erro.error.fieldErrors;
      if (fieldErrors) {
        fieldErrors.forEach((fieldError) => {
          this.mensagens_validacao[fieldError.field].forEach(mensagem => {
            if (mensagem.map === fieldError.message) {
              this.signinForm.controls[fieldError.field].setErrors(
                {[mensagem.type]: mensagem.type});
              erroFormulario = true;
            }
          });
        });
      }

      if (erroFormulario) {
        this.snack.open('Falha na validação do formulário', 'OK', {duration: 4000});
      } else {
        this.snack.open('Login ou senha inválida!', 'OK', {duration: 4000});
        this.submitted = false;
        this.signinForm.reset();
      }
    }
  }
}
