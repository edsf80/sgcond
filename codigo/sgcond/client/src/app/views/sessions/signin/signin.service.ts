import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AuthService} from '../../../auth/auth.service';
import {Session} from '../../../auth/session';

@Injectable()
export class SigninService {

  constructor(private http: HttpClient,
              private authService: AuthService) {
  }

  signin(user: { username: string, senha: string }): Observable<boolean> {
    return new Observable(observer => {

      this.http.post<{ id: number, nome: string, username: string, papeis: string[] }>('api/login', user, {observe: 'response'}).subscribe(resposta => {
        const session: Session = new Session();
        session.atualizarUsuario({
          id: resposta.body.id,
          nome: resposta.body.nome,
          login: resposta.body.username,
          roles: resposta.body.papeis
        });

        this.authService.setSession(session);

        observer.next(true);
      }, erro => {
        observer.next(erro);
      });
    });
  }
}
