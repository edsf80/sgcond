import {Component, OnInit} from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import {ContaService} from './conta.service';
import {Observable} from 'rxjs/Rx';
import {MatSnackBar} from '@angular/material';
import {AuthService} from '../../auth/auth.service';
import {Usuario} from '../../auth/usuario';
import {AppLoaderService} from '../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-conta',
  templateUrl: './conta.component.html'
})
export class ContaComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: 'upload_url'});
  public hasBaseDropZoneOver = false;

  usuario: Usuario;
  notificacao: Observable<boolean>;
  erroValidacao = false;
  msgValidacao: string;

  constructor(private contaService: ContaService, private authService: AuthService,
              private loader: AppLoaderService, private snack: MatSnackBar) {
  }

  ngOnInit() {
    this.usuario = this.authService.getUsuario();
  }

  onSalvar() {
    this.notificacao = this.loader.open('Salvando dados');
    this.contaService.salvar(this.usuario.id, this.usuario).subscribe(resposta => {
      this.usuario = resposta;
      this.authService.getSession().atualizarUsuario(this.usuario);
      this.notificacao.subscribe(res => {
        this.snack.open('Dados do usuário salvos com sucesso!', 'OK', { duration: 4000 });
      });
      this.loader.close();
    }, erro => this.loader.close());
  }

  onSalvarSenha(form: any) {
    this.notificacao = this.loader.open('Alterando senha do usuário');
    this.contaService.alterarSenha(this.usuario.id, this.usuario.login, form.senhaAtual, form.novaSenha).subscribe(resposta => {
      this.notificacao.subscribe(res => {
        this.snack.open('Senha do usuário alterada com sucesso!', 'OK', { duration: 4000 });
      });
      this.loader.close();
    }, erro => {
      this.loader.close();
      this.erroValidacao = true;
      this.msgValidacao = erro.error;
    });
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
}
