import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Usuario} from '../../auth/usuario';


@Injectable()
export class ContaService {

  constructor(private http: HttpClient) {
  }

  buscarPorId(id: number) {
  }

  salvar(id: number, conta: Usuario): Observable<Usuario> {
    return this.http.put<Usuario>('/api/usuario/' + id, conta);
  }

  alterarSenha(id: number, username: string, senhaAtual: string, novaSenha: string): Observable<any> {
    return this.http.put<any>('api/usuario/' + id + '/senha', {
      username: username,
      senhaAtual: senhaAtual,
      novaSenha: novaSenha
    });
  }

}
