import {Routes} from '@angular/router';
import {ContaComponent} from './conta.component';


export const ContaRoutes: Routes = [
  {path: '', component: ContaComponent}
];
