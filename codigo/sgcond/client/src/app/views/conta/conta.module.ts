import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContaComponent} from './conta.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ContaRoutes} from './conta.routing';
import {FileUploadModule} from 'ng2-file-upload';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatProgressBarModule, MatSnackBarModule,
  MatTabsModule
} from '@angular/material';
import {ContaService} from './conta.service';
import {CustomFormsModule} from 'ng2-validation';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CustomFormsModule,
    FileUploadModule,
    MatTabsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatProgressBarModule,
    SharedModule,
    MatSnackBarModule,
    RouterModule.forChild(ContaRoutes)
  ],
  declarations: [
    ContaComponent
  ],
  providers: [ContaService]
})
export class ContaModule {
}
