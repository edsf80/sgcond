import {Component, EventEmitter, Input, Output} from '@angular/core';
import {IEnquete} from '../dashboard';
import {DashboardService} from '../dashboard.service';
import {Observable} from 'rxjs/Rx';
import {AppLoaderService} from '../../../shared/services/app-loader/app-loader.service';
import {AppConfirmService} from '../../../shared/services/app-confirm/app-confirm.service';

@Component({
  selector: 'app-enquete-item',
  templateUrl: './enquete-item.component.html'
})
export class EnqueteItemComponent {

  @Input('enquete')
  enquete: IEnquete;

  notificacao: Observable<boolean>;

  @Output() computed: EventEmitter<number> = new EventEmitter();

  constructor(private dashboardService: DashboardService,
              private loader: AppLoaderService,
              private confirm: AppConfirmService) {
  }

  onSalvar(value: { idEnquete: number, idUnidade: number, idItemEnquete: number }) {
    this.notificacao = this.loader.open('Computando voto');
    this.dashboardService.votarEnquete(value.idEnquete, value.idItemEnquete, value.idUnidade).subscribe(
      resposta => {
        this.notificacao.subscribe(dado => {
          this.confirm.confirm({title: 'Confirmação', message: 'Voto computado com sucesso!'}).subscribe(ok => {
            if (ok) {
              this.computed.emit(resposta.id);
            }
          });
        });
        this.loader.close();
      }, error2 => this.loader.close()
    );
  }
}
