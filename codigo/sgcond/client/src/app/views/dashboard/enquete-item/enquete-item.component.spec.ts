import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnqueteItemComponent } from './enquete-item.component';

describe('EnqueteItemComponent', () => {
  let component: EnqueteItemComponent;
  let fixture: ComponentFixture<EnqueteItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnqueteItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnqueteItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
