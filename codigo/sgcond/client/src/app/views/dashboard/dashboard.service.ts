
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Dashboard, IEnquete} from './dashboard';

@Injectable()
export class DashboardService {

  constructor(private http: HttpClient) {}

  buscarDados(): Observable<Dashboard> {
    return this.http.get<Dashboard>('api/dashboard');
  }

  votarEnquete(idEnquete: number, idItemEnquete: number, idUnidade: number): Observable<IEnquete> {
    return this.http.post<IEnquete>('api/dashboard/voto/' + idEnquete, {idItemEnquete: idItemEnquete, idUnidade: idUnidade});
  }
}
