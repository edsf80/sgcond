import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  MatListModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatChipsModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTabsModule,
  MatInputModule,
  MatProgressBarModule
 } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import {DashboardComponent} from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import {DashboardService} from './dashboard.service';
import { EnqueteItemComponent } from './enquete-item/enquete-item.component';
import { ResultadosEnqueteComponent } from './resultados-enquete/resultados-enquete.component';
import { AvisosComponent } from './avisos/avisos.component';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatGridListModule,
    MatChipsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTabsModule,
    MatInputModule,
    MatProgressBarModule,
    FlexLayoutModule,
    NgxDatatableModule,
    ChartsModule,
    SharedModule,
    FileUploadModule,
    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [DashboardComponent, EnqueteItemComponent, ResultadosEnqueteComponent, AvisosComponent],
  providers: [DashboardService]
})
export class DashboardModule {

  voto = true;

  onVotar() {
    this.voto = false;
  }
}
