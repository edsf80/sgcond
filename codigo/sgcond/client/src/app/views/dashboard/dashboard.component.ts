import { Component, OnInit } from '@angular/core';
import {DashboardService} from './dashboard.service';
import {Dashboard} from './dashboard';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  dashboardData: Dashboard = new Dashboard();

  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.dashboardService.buscarDados().subscribe(resposta => {
      this.dashboardData = resposta;
    });
  }

  onVotoComputado(event) {
    const index = this.dashboardData.enquetesNaoVotadas.findIndex(
      (s) => s.id === event
    );
    this.dashboardData.enquetesNaoVotadas.splice(index, 1);
  }

}
