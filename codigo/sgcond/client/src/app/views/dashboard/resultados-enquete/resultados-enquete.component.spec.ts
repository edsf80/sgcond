import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadosEnqueteComponent } from './resultados-enquete.component';

describe('ResultadosEnqueteComponent', () => {
  let component: ResultadosEnqueteComponent;
  let fixture: ComponentFixture<ResultadosEnqueteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadosEnqueteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadosEnqueteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
