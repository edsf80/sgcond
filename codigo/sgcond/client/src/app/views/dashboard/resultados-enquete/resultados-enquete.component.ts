import {Component, Input} from '@angular/core';
import {IEnquete} from '../dashboard';
import {Router} from '@angular/router';

@Component({
  selector: 'app-resultados-enquete',
  templateUrl: './resultados-enquete.component.html'
})
export class ResultadosEnqueteComponent {

  @Input()
  enquetes: IEnquete[];

  constructor(private router: Router) { }

  onEnqueteClick(idEnquete: number) {
    this.router.navigate(['/', 'enquete', 'resultado', idEnquete]);
  }
}
