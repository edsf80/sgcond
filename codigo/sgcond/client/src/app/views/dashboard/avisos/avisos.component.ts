import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {IAviso} from '../dashboard';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-avisos',
  templateUrl: './avisos.component.html'
})
export class AvisosComponent implements OnInit {

  dialogRef;

  @Input()
  avisos: IAviso[];

  modalData: IAviso;

  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  constructor(public dialogBox: MatDialog) { }

  ngOnInit() {
  }

  onAvisoClick(aviso: IAviso) {
    this.modalData = aviso;
    this.dialogRef = this.dialogBox.open(this.modalContent);
  }

}
