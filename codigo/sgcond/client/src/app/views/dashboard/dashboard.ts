export interface IAviso {

  titulo: string;

  descricao: string;
}

export interface IUnidade {

  id: number;

  identificador: string;
}

export interface IItemEnquete {

  id: number;

  descricao: string;
}

export interface IReserva {

  area: string;

  inicio: string;

  termino: string;

  dono: string;
}

export interface IEnquete {

  id: number;

  titulo: string;

  status: string;

  unidade: IUnidade;

  itens: IItemEnquete[];
}

export class Dashboard {

  enquetes: IEnquete[];

  enquetesNaoVotadas: IEnquete[];

  reservas: IReserva;

  avisos: IAviso[];
}
