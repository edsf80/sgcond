package br.com.eswconsulting.sgcond.test.steps;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.ChamadoAddPage;
import br.com.eswconsulting.sgcond.test.pages.ChamadosListPage;
import br.com.eswconsulting.sgcond.test.pages.MainPage;
import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;


public class ChamadoSteps {

	private MainPage mainPage;

	private ChamadosListPage chamadosListPage;
	
	private ChamadoAddPage chamadoAddPage;
	
	private static final String TITULO_CHAMADO_BUSCA = "Chamado teste";
	private static final String TITULO_CHAMADO_ALTERAR = "Chamado para alterar";
	private static final String TITULO_CHAMADO_ALTERADO = "Chamado já alterado";
	
	@Before
	public void inicializar() {
		this.mainPage = PageFactory.getInstance().getMainPage();
		this.chamadosListPage = PageFactory.getInstance().getChamadosListPage();
		this.chamadoAddPage = PageFactory.getInstance().getChamadosAddPage();
	}	

	@Quando("^acesso a tela de chamados$")
	public void acessarATelaDeChamados() throws Throwable {
		this.mainPage.clicarMenuChamados();
	}

	@Então("^todos os chamados cadastrados devem ser exibidos$")
	public void todosOsChamadosCadastradosDevemSerExibidos() throws Throwable {
		Assert.assertTrue(this.chamadosListPage.listaEhExibida());
	}
	
	@Quando("^digito no filtro exatamente um valor existente na lista$")
	public void digitarNoFiltroExatamenteUmValorExistenteNaLista() throws Throwable {
	    this.chamadosListPage.inserirValorFiltro(TITULO_CHAMADO_BUSCA);
	}

	@Então("^apenas esse valor deverá ser exibido na lista$")
	public void apenasEsseValorDeveráSerExibidoNaLista() throws Throwable {
		Assert.assertTrue(this.chamadosListPage.listaPossuiValorUnico(TITULO_CHAMADO_BUSCA));
	}
	
	@Quando("^clico no botão de adicionar$")
	public void clicoNoBotãoDeAdicionar() throws Throwable {
	    this.chamadosListPage.clicarBotaoAdicionar();
	}

	@Quando("^insiro campos obrigatórios da tela$")
	public void insiroCamposObrigatóriosDaTela() throws Throwable {
	    
	}
	
	@Quando("^insiro campos \"([^\"]*)\"$")
	public void insiroCampos(String campos) throws Throwable {
		String camposInd[] = campos.split(",");
		String titulo = "", descricao = "", status = "";
		for (String campo: camposInd) {
			if (campo.trim().equals("título")) {
				titulo = "Chamado teste automatico";
			} else if (campo.trim().equals("descrição")) {
				descricao = "Descricao chamado teste automatico";
			} else if (campo.trim().equals("status")) {
				status = "NOVO";
			}
		}
		this.chamadoAddPage.inserirDados(titulo, descricao, status);
	}

	@Quando("^clico no botão salvar$")
	public void clicoNoBotãoSalvar() throws Throwable {
	    this.chamadoAddPage.clicarBotaoSalvar();
	}

	@Então("^o chamado adicionado deve aparecer na lista$")
	public void oChamadoAdicionadoDeveAparecerNaLista() throws Throwable {
		this.chamadosListPage.inserirValorFiltro("Chamado teste automatico");
		Assert.assertTrue(this.chamadosListPage.listaPossuiValorUnico("Chamado teste automatico"));
	}
	
	@Quando("^não insiro campos obrigatórios da tela$")
	public void insiroNãoCamposObrigatóriosDaTela() throws Throwable {
		this.chamadoAddPage.inserirDados("", "Descricao chamado teste automatico", "NOVO");
	}

	@Então("^o botão salvar não deve estar habilitado$")
	public void oBotãoSalvarNãoDeveEstarHabilitado() throws Throwable {
	    Assert.assertFalse(this.chamadoAddPage.botaoSalvarHabilitado());
	    // Aqui como é feito sem passar pelo after dessa classe estou finalizando por aqui.
	    if (this.mainPage.ehExibida()) {
			this.mainPage.clicarLogout();
		}
	}	

	@Quando("^acesso a tela principal$")
	public void acessoATelaPrincipal() throws Throwable {
	    
	}

	@Então("^o item de menu de chamados não deve ser exibido$")
	public void oItemDeMenuDeChamadosNãoDeveSerExibido() throws Throwable {
	    Assert.assertFalse(this.mainPage.menuChamadoVisivel());
	}		
	
	@Quando("^seleciono um chamado da lista$")
	public void clicoEmUmChamadoDaLista() throws Throwable {
		this.chamadosListPage.inserirValorFiltro(TITULO_CHAMADO_ALTERAR);
		this.chamadosListPage.selecionarNaLista(TITULO_CHAMADO_ALTERAR);
	}

	@Quando("^altero os campos do chamado$")
	public void alteroOsCamposDoChamado() throws Throwable {
		this.chamadoAddPage.alterarDados(TITULO_CHAMADO_ALTERADO, TITULO_CHAMADO_ALTERADO, "NOVO");
	}

	@Então("^o item aparece com novo valor na lista$")
	public void oItemApareceComNovoValorNaLista() throws Throwable {
		this.chamadosListPage.inserirValorFiltro(TITULO_CHAMADO_ALTERADO);
		Assert.assertTrue(this.chamadosListPage.listaPossuiValorUnico(TITULO_CHAMADO_ALTERADO));
	}
}
