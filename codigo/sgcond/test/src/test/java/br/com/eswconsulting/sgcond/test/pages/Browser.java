package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.WebDriver;

public class Browser extends AbstractPage {

	private String url;

	public Browser(WebDriver driver) {
		super(driver);
	}

	public void fechar() {
		this.driver.quit();
	}

	public void acessar(String uri) {
		this.driver.get(this.url + uri);
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

}
