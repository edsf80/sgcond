package br.com.eswconsulting.sgcond.test.steps;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class GenericSteps {
	
	@After
	public void finalizar() {
		if (PageFactory.getInstance().getMainPage().ehExibida()) {
			PageFactory.getInstance().getMainPage().clicarLogout();
		}
	}
	
	@Dado("^que estou logado como funcionário$")
	public void queEstouLogadoComoFuncionário() throws Throwable {
		PageFactory.getInstance().getLoginPage().inserirLogin("porteiro@teste.com");
		PageFactory.getInstance().getLoginPage().inserirSenha("123");
		PageFactory.getInstance().getLoginPage().clicarEntrar();
	}
	
	@Dado("^que estou logado como morador$")
	public void queEstouLogadoComoMorador() throws Throwable {
		PageFactory.getInstance().getLoginPage().inserirLogin("morador@teste.com");
		PageFactory.getInstance().getLoginPage().inserirSenha("123");
		PageFactory.getInstance().getLoginPage().clicarEntrar();
	}
	
	@Dado("^que estou logado como administrador$")
	public void queEstouLogadoComoAdministrador() throws Throwable {
		PageFactory.getInstance().getLoginPage().inserirLogin("administrador@teste.com");
		PageFactory.getInstance().getLoginPage().inserirSenha("123");
		PageFactory.getInstance().getLoginPage().clicarEntrar();
	}
	
	@Dado("^que estou logado como síndico$")
	public void queEstouLogadoComoSíndico() throws Throwable {
		PageFactory.getInstance().getLoginPage().inserirLogin("sindico@teste.com");
		PageFactory.getInstance().getLoginPage().inserirSenha("123");
		PageFactory.getInstance().getLoginPage().clicarEntrar();
	}
	
	@Quando("^acesso a uri \"([^\"]*)\"$")
	public void acessoOLink(String uri) throws Throwable {
	    PageFactory.getInstance().getBrowser().acessar(uri);
	}
	
	@Então("^tela de acesso negado deve ser exibida$")
	public void telaDeAcessoNegadoDeveSerExibida() throws Throwable {
		Assert.assertTrue(PageFactory.getInstance().getAcessoNegadoPage().ehExibida());
		PageFactory.getInstance().getAcessoNegadoPage().clicarBotaoVoltarDashboard();
	}

}
