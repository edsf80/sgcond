package br.com.eswconsulting.sgcond.test.runners;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import br.com.eswconsulting.sgcond.test.pages.Browser;
import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features/usuarioadd.feature",
		glue = "br.com.eswconsulting.sgcond.test.steps",
		plugin = "pretty", 
		monochrome = false, 
		snippets = SnippetType.CAMELCASE,
		dryRun = false, // Não executa testes se estiver faltando passos.
		strict = true)
public class RunnerTest {
	
    private static String url = "jdbc:postgresql://localhost:5432/sgcond-test";
    private static String user = "postgres";
    private static String pass = "secret";
	
	@BeforeClass 
	public static void inicializar() {
		System.setProperty("webdriver.gecko.driver", "/home/edsf/projetos/drivers/geckodriver");
		WebDriver driver = new FirefoxDriver();		
		PageFactory pageFactory = PageFactory.getInstance();
		pageFactory.setDriver(driver);
		Browser browser = PageFactory.getInstance().getBrowser();
		browser.setUrl("http://localhost:5000");
		browser.acessar("");		

        try {
            Class.forName("org.postgresql.Driver");
            Connection conn = DriverManager.getConnection(url, user, pass);
            ScriptRunner runner = new ScriptRunner(conn, false, true);

            runner.runScript(new BufferedReader(new FileReader("src/test/resources/db/inicializacaodb.sql")));
        } catch (Exception e) {
            //throw new RuntimeException(e.getMessage(), e);
        	System.out.println("Deu merda grande");
        	e.printStackTrace();
        }
	}	
	
	@AfterClass
	public static void finalizar() {
		PageFactory.getInstance().getMainPage().fecharBrowser();
	}
}
