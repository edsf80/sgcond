package br.com.eswconsulting.sgcond.test.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EnqueteAddPage extends AbstractPage {

	private static final String TAG_CAMPO_STATUS = "mat-option";
	private static final String XPATH_SETA_CAMPO_STATUS = "//mat-select[@id='sStatusEnquete']//div[@class='mat-select-trigger']//div[@class='mat-select-arrow-wrapper']";

	public EnqueteAddPage(WebDriver driver) {
		super(driver);
	}

	public void inserirCampos(String titulo, String status, List<String> itens) {
		this.driver.findElement(By.id("itTituloEnquete")).sendKeys(titulo);
		if (!status.isEmpty()) {
			this.driver.findElement(By.xpath(XPATH_SETA_CAMPO_STATUS)).click();
			List<WebElement> elements = this.driver.findElements(By.tagName(TAG_CAMPO_STATUS));
			for (final WebElement option : elements) {
				if (option.getText().equals(status)) {
					option.click();
					break;
				}
			}
		}

		for (String item: itens) {
			this.driver.findElement(By.id("itDescricaoItemEnquete")).clear();
			this.driver.findElement(By.id("itDescricaoItemEnquete")).sendKeys(item);
			this.driver.findElement(By.id("btnAddItemEnquete")).click();
		}
	}

	public void alterarCampos(String titulo, String status, ArrayList<String> itens) {
		this.driver.findElement(By.id("itTituloEnquete")).clear();
		this.driver.findElement(By.id("itTituloEnquete")).sendKeys(titulo);
		if (!status.isEmpty()) {
			this.driver.findElement(By.xpath(XPATH_SETA_CAMPO_STATUS)).click();
			List<WebElement> elements = this.driver.findElements(By.tagName(TAG_CAMPO_STATUS));
			for (final WebElement option : elements) {
				if (option.getText().equals(status)) {
					option.click();
					break;
				}
			}
		}

		for (String item: itens) {
			this.driver.findElement(By.id("itDescricaoItemEnquete")).clear();
			this.driver.findElement(By.id("itDescricaoItemEnquete")).sendKeys(item);
			this.driver.findElement(By.id("btnAddItemEnquete")).click();
		}
	}

}
