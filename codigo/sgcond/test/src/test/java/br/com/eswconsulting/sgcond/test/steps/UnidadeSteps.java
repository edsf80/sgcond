package br.com.eswconsulting.sgcond.test.steps;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.MainPage;
import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import br.com.eswconsulting.sgcond.test.pages.UnidadesListPage;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class UnidadeSteps {

	private static final String VALOR_BUSCA_FILTRO = "Cobertura";
	private static final String ID_UNIDADE_ALTERAR = "Alterar";

	private MainPage mainPage;

	private UnidadesListPage unidadesListPage;

	@Before
	public void inicializar() {
		this.mainPage = PageFactory.getInstance().getMainPage();
		this.unidadesListPage = PageFactory.getInstance().getUnidadesListPage();
	}

	@Quando("^acesso a tela de unidades$")
	public void acessoATelaDeUnidades() throws Throwable {
		this.mainPage.clicarMenuUnidades();
		Assert.assertTrue(this.unidadesListPage.listaEhExibida());
	}

	@Então("^todos as unidades cadastradas devem ser exibidas$")
	public void todosAsUnidadesCadastradasDevemSerExibidas() throws Throwable {
		Assert.assertTrue(this.unidadesListPage.listaEhExibida());
	}

	@Quando("^digito no filtro exatamente um valor existente na lista de unidades$")
	public void digitoNoFiltroExatamenteUmValorExistenteNaListaDeUnidades() throws Throwable {
		this.unidadesListPage.inserirValorFiltro(VALOR_BUSCA_FILTRO);
	}

	@Então("^apenas essa unidade deverá ser exibida na lista$")
	public void apenasEssaUnidadeDeveráSerExibidaNaLista() throws Throwable {
		Assert.assertTrue(this.unidadesListPage.listaPossuiValorUnico(VALOR_BUSCA_FILTRO));
	}

	@Então("^o item de menu de unidades não deve ser exibido$")
	public void oItemDeMenuDeUnidadesNãoDeveSerExibido() throws Throwable {
		Assert.assertFalse(this.mainPage.menuUnidadeVisivel());
	}

	@Quando("^clico no botão de adicionar unidade$")
	public void clicoNoBotãoDeAdicionarUnidade() throws Throwable {
		PageFactory.getInstance().getUnidadesListPage().clicarBotaoAdicionar();
	}

	@Quando("^insiro campos \"([^\"]*)\" da unidade$")
	public void insiroCamposDaUnidade(String campos) throws Throwable {
		String camposInd[] = campos.split(",");
		String identificador = "", proprietario = "";
		for (String campo : camposInd) {
			if (campo.trim().equals("identificador")) {
				identificador = "22A";
			} else if (campo.trim().equals("proprietário")) {
				proprietario = "Morador";
			}
		}

		PageFactory.getInstance().getUnidadeAddPage().inserirCampos(identificador, proprietario);
	}

	@Quando("^clico no botão salvar unidade$")
	public void clicoNoBotãoSalvarUnidade() throws Throwable {
		PageFactory.getInstance().getUnidadeAddPage().clicarBotaoSalvar();
	}

	@Então("^a unidade adicionada deve aparecer na lista$")
	public void aUnidadeAdicionadaDeveAparecerNaLista() throws Throwable {
		PageFactory.getInstance().getUnidadesListPage().inserirValorFiltro("22A");
		Assert.assertTrue(PageFactory.getInstance().getUnidadesListPage().listaPossuiValorUnico("22A"));
	}

	@Então("^o botão salvar unidade não deve estar habilitado$")
	public void oBotãoSalvarUnidadeNãoDeveEstarHabilitado() throws Throwable {
		Assert.assertFalse(PageFactory.getInstance().getUnidadeAddPage().botaoSalvarHabilitado());
		// Aqui como é feito sem passar pelo after dessa classe estou finalizando por
		// aqui.
		if (this.mainPage.ehExibida()) {
			this.mainPage.clicarLogout();
		}
	}
	
	@Quando("^insiro campos com identificador existente$")
	public void insiroCamposComIdentificadorExistente() throws Throwable {
		PageFactory.getInstance().getUnidadeAddPage().inserirCampos("Cobertura", "Morador");
	}
	
	@Então("^mensagem de erro de validação de unidade deve aparecer$")
	public void mensagemDeErroDeValidaçãoDeUnidadeDeveAparecer() throws Throwable {
	    Assert.assertTrue(PageFactory.getInstance().getUnidadeAddPage().msgErroValidacaoEhExibido());
	}
	
	@Quando("^seleciono uma unidade existente$")
	public void selecionoUmaUnidadeExistente() throws Throwable {
		PageFactory.getInstance().getUnidadesListPage().inserirValorFiltro(ID_UNIDADE_ALTERAR);
	    this.unidadesListPage.selecionarNaLista(ID_UNIDADE_ALTERAR);
	}

	@Quando("^altero os campos da unidade$")
	public void alteroOsCamposDaUnidade() throws Throwable {
	    PageFactory.getInstance().getUnidadeAddPage().alterarCampos("Coberturaa", "Morador");
	}

	@Então("^a unidade aparece com novo valor na lista$")
	public void aUndiadeApareceComNovoValorNaLista() throws Throwable {
		this.unidadesListPage.inserirValorFiltro("Coberturaa");
		Assert.assertTrue(this.unidadesListPage.listaPossuiValorUnico("Coberturaa"));
	}

}
