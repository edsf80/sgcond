package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AreaComumListPage extends BaseListPage {

	private static final String XPATH_HEADER_ID_AREA = "//span[@class='datatable-header-cell-label draggable'][contains(text(),'Id')]";		

	public AreaComumListPage(WebDriver driver) {
		super(driver);
	}

	public boolean listaEhExibida() {
		return this.itemEhExibido(By.xpath(XPATH_HEADER_ID_AREA));
	}
}
