package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UsuarioAddPage extends AbstractPage {

	private static final String ID_CAMPO_NOME_USUARIO = "itNomeUsuario";
	private static final String ID_CAMPO_SENHA_USUARIO = "itSenhaUsuario";
	private static final String ID_CAMPO_EMAIL_USUARIO = "itEmailUsuario";

	public UsuarioAddPage(WebDriver driver) {
		super(driver);
	}	

	public void inserirCampos(String nome, String email, String senha) {
		this.inserirValorCampo(By.id(ID_CAMPO_NOME_USUARIO), nome);
		this.inserirValorCampo(By.id(ID_CAMPO_EMAIL_USUARIO), email);
		this.inserirValorCampo(By.id(ID_CAMPO_SENHA_USUARIO), senha);
	}

	public void alterarCampos(String nome, String email, String senha) {
		this.alterarValorCampo(By.id(ID_CAMPO_NOME_USUARIO), nome);
		this.alterarValorCampo(By.id(ID_CAMPO_EMAIL_USUARIO), email);
		this.alterarValorCampo(By.id(ID_CAMPO_SENHA_USUARIO), senha);
	}

}
