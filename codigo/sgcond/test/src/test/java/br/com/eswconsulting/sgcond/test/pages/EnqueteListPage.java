package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EnqueteListPage extends BaseListPage {

	private static final String XPATH_DESCRICAO_ENQUETE = "//html//datatable-row-wrapper[1]/datatable-body-row[1]/div[2]/datatable-body-cell[2]/div[1]";
	
	public EnqueteListPage(WebDriver driver) {
		super(driver);
	}

	public boolean listaEhExibida() {
		return this.itemEhExibido(By.xpath(XPATH_DESCRICAO_ENQUETE));
	}
}
