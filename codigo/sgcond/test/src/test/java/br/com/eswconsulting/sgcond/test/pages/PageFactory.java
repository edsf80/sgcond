/**
 * 
 */
package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.WebDriver;

/**
 * @author edsf
 *
 */
public class PageFactory {

	private static PageFactory pageFactory;
	
	private LoginPage loginPage;

	private MainPage mainPage;

	private ChamadosListPage chamadosListPage;
	
	private ChamadoAddPage chamadoAddPage;
	
	private Browser browser;
	
	private AcessoNegadoPage acessoNegadoPage;
	
	private UnidadesListPage unidadesListPage;
	
	private UnidadeAddPage unidadeAddPage;
	
	private UsuarioListPage usuarioListPage;
	
	private WebDriver driver;

	private UsuarioAddPage usuarioAddPage;

	private AreaComumListPage areaComumListPage;

	private AreaComumAddPage areaComumAddPage;
	
	private EnqueteListPage enqueteListPage;
	
	private EnqueteAddPage enqueteAddPage;

	/**
	 * 
	 */
	private PageFactory() {
	}

	public static PageFactory getInstance() {
		if (pageFactory == null) {
			pageFactory = new PageFactory();
		}

		return pageFactory;
	}
	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public LoginPage getLoginPage() {
		if (this.loginPage == null) {
			this.loginPage = new LoginPage(driver);
		}

		return this.loginPage;
	}

	public MainPage getMainPage() {
		if (this.mainPage == null) {
			this.mainPage = new MainPage(driver);
		}

		return this.mainPage;
	}

	public ChamadosListPage getChamadosListPage() {
		if (this.chamadosListPage == null) {
			this.chamadosListPage = new ChamadosListPage(driver);
		}

		return this.chamadosListPage;
	}
	
	public ChamadoAddPage getChamadosAddPage() {
		if (this.chamadoAddPage == null) {
			this.chamadoAddPage = new ChamadoAddPage(driver);
		}

		return this.chamadoAddPage;
	}
	
	public Browser getBrowser() {
		if (this.browser == null) {
			this.browser = new Browser(driver);
		}
		
		return this.browser;
	}
	
	public AcessoNegadoPage getAcessoNegadoPage() {
		if (this.acessoNegadoPage == null) {
			this.acessoNegadoPage = new AcessoNegadoPage(driver);
		}
		
		return this.acessoNegadoPage;
	}

	public UnidadesListPage getUnidadesListPage() {
		if (this.unidadesListPage == null) {
			this.unidadesListPage = new UnidadesListPage(driver);
		}
		
		return this.unidadesListPage;
	}
	
	public UnidadeAddPage getUnidadeAddPage() {
		if (this.unidadeAddPage == null) {
			this.unidadeAddPage = new UnidadeAddPage(driver);
		}
		
		return this.unidadeAddPage;
	}
	
	public UsuarioListPage getUsuarioListPage() {
		if (this.usuarioListPage == null) {
			this.usuarioListPage = new UsuarioListPage(driver);
		}
		
		return this.usuarioListPage;
	}

	public UsuarioAddPage getUsuarioAddPage() {
		if (this.usuarioAddPage == null) {
			this.usuarioAddPage = new UsuarioAddPage(driver);
		}
		
		return this.usuarioAddPage;
	}

	public AreaComumListPage getAreaComumListPage() {
		if (this.areaComumListPage == null) {
			this.areaComumListPage = new AreaComumListPage(driver);
		}
		
		return this.areaComumListPage;
	}

	public AreaComumAddPage getAreaComumAddPage() {
		if (this.areaComumAddPage == null) {
			this.areaComumAddPage = new AreaComumAddPage(driver);
		}
		
		return this.areaComumAddPage;
	}
	
	public EnqueteListPage getEnqueteListPage() {
		if (this.enqueteListPage == null) {
			this.enqueteListPage = new EnqueteListPage(driver);
		}
		
		return this.enqueteListPage;
	}
	
	public EnqueteAddPage getEnqueteAddPage() {
		if (this.enqueteAddPage == null) {
			this.enqueteAddPage = new EnqueteAddPage(driver);
		}
		
		return this.enqueteAddPage;
	}
}
