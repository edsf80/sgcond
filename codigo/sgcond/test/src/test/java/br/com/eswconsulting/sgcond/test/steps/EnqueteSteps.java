package br.com.eswconsulting.sgcond.test.steps;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class EnqueteSteps {

	private static final String TTITULO_ENQUETE_BUSCA = "Enquete teste";
	private static final String TITULO_ENQUETE_INSERIR = "Enquete inserida";
	private static final String STATUS_ENQUETE_INSERIR = "Aberta";
	private static final String ITEM_ENQUETE_INSERIR = "Item";
	private static final String TITULO_ENQUETE_ALTERAR = null;

	@Quando("^acesso a tela de enquetes$")
	public void acessoATelaDeEnquetes() throws Throwable {
		PageFactory.getInstance().getMainPage().clicarMenuEnquetes();
	}

	@Então("^todos as enquetes cadastradas devem ser exibidas$")
	public void todosAsEnquetesCadastradasDevemSerExibidas() throws Throwable {
		Assert.assertTrue(PageFactory.getInstance().getEnqueteListPage().listaEhExibida());
	}

	@Quando("^digito no filtro exatamente um valor existente na lista de enquetes$")
	public void digitoNoFiltroExatamenteUmValorExistenteNaListaDeEnquetes() throws Throwable {
		PageFactory.getInstance().getEnqueteListPage().inserirValorFiltro(TTITULO_ENQUETE_BUSCA);
	}

	@Então("^apenas essa enquete deverá ser exibida na lista$")
	public void apenasEssaEnqueteDeveráSerExibidaNaLista() throws Throwable {
		Assert.assertTrue(PageFactory.getInstance().getEnqueteListPage().listaPossuiValorUnico(TTITULO_ENQUETE_BUSCA));
	}

	@Então("^o item de menu de enquetes não deve ser exibido$")
	public void oItemDeMenuDeEnquetesNãoDeveSerExibido() throws Throwable {
		Assert.assertFalse(PageFactory.getInstance().getMainPage().menuEnqueteVisivel());
	}

	@Quando("^clico no botão de adicionar enquete$")
	public void clicoNoBotãoDeAdicionarEnquete() throws Throwable {
		PageFactory.getInstance().getEnqueteListPage().clicarBotaoAdicionar();
	}

	@Quando("^insiro campos \"([^\"]*)\" da enquete$")
	public void insiroCamposDaEnquete(String lista) throws Throwable {
		String campos[] = lista.split(",");
		String titulo = "";
		String status = "";
		List<String> itens = new ArrayList<>();
		for (String campo : campos) {
			if (campo.equals("Título")) {
				titulo = TITULO_ENQUETE_INSERIR;
			} else if (campo.equals("Status")) {
				status = STATUS_ENQUETE_INSERIR;
			} else if (campo.equals("Itens")) {
				itens.add(ITEM_ENQUETE_INSERIR + "-01");
				itens.add(ITEM_ENQUETE_INSERIR + "-02");
			}
		}

		PageFactory.getInstance().getEnqueteAddPage().inserirCampos(titulo, status, itens);
	}

	@Então("^a enquete adicionada deve aparecer na lista$")
	public void aEnqueteAdicionadaDeveAparecerNaLista() throws Throwable {
		PageFactory.getInstance().getEnqueteListPage().inserirValorFiltro(TITULO_ENQUETE_INSERIR);
		PageFactory.getInstance().getEnqueteListPage().listaPossuiValorUnico(TITULO_ENQUETE_INSERIR);
	}

	@Então("^o botão salvar enquete não deve estar habilitado$")
	public void oBotãoSalvarEnqueteNãoDeveEstarHabilitado() throws Throwable {
		Assert.assertFalse(PageFactory.getInstance().getEnqueteAddPage().botaoSalvarHabilitado());
	}

	@Quando("^seleciono uma enquete existente$")
	public void selecionoUmaEnqueteExistente() throws Throwable {
		PageFactory.getInstance().getEnqueteListPage().inserirValorFiltro(TITULO_ENQUETE_ALTERAR);
	}

	@Quando("^altero os campos da enquete$")
	public void alteroOsCamposDaEnquete() throws Throwable {
		PageFactory.getInstance().getEnqueteAddPage().alterarCampos("Enquete alterada", "Fechada", new ArrayList<String>());
	}

	@Então("^a enquete aparece com novo valor na lista$")
	public void aEnqueteApareceComNovoValorNaLista() throws Throwable {
		PageFactory.getInstance().getEnqueteListPage().inserirValorFiltro("Enquete alterada");
		PageFactory.getInstance().getEnqueteListPage().listaPossuiValorUnico("Enquete alterada");
	}
}
