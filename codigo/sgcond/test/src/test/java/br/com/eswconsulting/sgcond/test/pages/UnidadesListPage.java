package br.com.eswconsulting.sgcond.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UnidadesListPage extends BaseListPage {

	private static final String XPATH_ID_UNIDADE = "//html//datatable-row-wrapper[1]/datatable-body-row[1]/div[2]/datatable-body-cell[2]/div[1]";
	

	public UnidadesListPage(WebDriver webdriver) {
		super(webdriver);
	}

	public boolean listaEhExibida() {
		WebDriverWait wait = new WebDriverWait(driver, 1);
		List<WebElement> elements = wait
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(XPATH_ID_UNIDADE)));
		return elements.size() > 0;
	}	
}
