package br.com.eswconsulting.sgcond.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ChamadoAddPage extends AbstractPage {

	private static final String ID_CAMPO_TITULO = "itTitChamado";
	private static final String ID_CAMPO_DESCRICAO = "itDescChamado";
	private static final String XPATH_SETA_CAMPO_STATUS = "//mat-select[@id='sStatusChamado']//div[@class='mat-select-trigger']//div[@class='mat-select-arrow-wrapper']";
	private static final String TAG_CAMPO_STATUS = "mat-option";
	private static final String ID_BOTAO_SALVAR_CHAMADO = "btnSalvarChamado";

	public ChamadoAddPage(WebDriver driver) {
		super(driver);
	}

	public void inserirDados(String titulo, String descricao, String status) {
		this.driver.findElement(By.id(ID_CAMPO_TITULO)).sendKeys(titulo);
		this.driver.findElement(By.id(ID_CAMPO_DESCRICAO)).sendKeys(descricao);
		if (!status.isEmpty()) {
			this.driver.findElement(By.xpath(XPATH_SETA_CAMPO_STATUS)).click();
			List<WebElement> elements = this.driver.findElements(By.tagName(TAG_CAMPO_STATUS));
			for (final WebElement option : elements) {
				if (option.getText().equals(status)) {
					option.click();
					break;
				}
			}
		}
	}

	public boolean botaoSalvarHabilitado() {
		return this.driver.findElement(By.id(ID_BOTAO_SALVAR_CHAMADO)).isEnabled();
	}

	public void alterarDados(String titulo, String descricao, String status) {
		this.driver.findElement(By.id(ID_CAMPO_TITULO)).clear();
		this.driver.findElement(By.id(ID_CAMPO_TITULO)).sendKeys(titulo);
		this.driver.findElement(By.id(ID_CAMPO_DESCRICAO)).clear();		
		this.driver.findElement(By.id(ID_CAMPO_DESCRICAO)).sendKeys(descricao);
		if (!status.isEmpty()) {
			this.driver.findElement(By.xpath(XPATH_SETA_CAMPO_STATUS)).click();
			List<WebElement> elements = this.driver.findElements(By.tagName(TAG_CAMPO_STATUS));
			for (final WebElement option : elements) {
				if (option.getText().equals(status)) {
					option.click();
					break;
				}
			}
		}		
	}

}
