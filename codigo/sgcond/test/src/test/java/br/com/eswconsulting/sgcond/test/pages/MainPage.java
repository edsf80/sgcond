package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends AbstractPage {

	public MainPage(WebDriver driver) {
		super(driver);
	}

	public boolean ehExibida() {
		if (this.driver.findElements(By.xpath("//div[@id='dvSystemBrand']")).size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void clicarLogout() {
		this.driver.findElement(By.xpath("//html//button[5]")).click();
		WebDriverWait wait = new WebDriverWait(driver, 1);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//html//div[@class='cdk-overlay-container']//button[4]"))).click();
	}

	public void clicarMenuChamados() {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='/chamado']")))
				.click();

	}

	public void fecharBrowser() {
		this.driver.quit();
	}

	public boolean menuChamadoVisivel() {
		return this.driver.findElement(By.xpath("//a[@href='/chamado']")).isDisplayed();				
	}
	
	public boolean menuUnidadeVisivel() {
		return this.driver.findElement(By.xpath("//a[@href='/unidade']")).isDisplayed();				
	}
	
	public boolean menuUsuarioVisivel() {
		return this.driver.findElement(By.xpath("//a[@href='/usuario']")).isDisplayed();				
	}

	public void clicarMenuUnidades() {
		WebDriverWait wait = new WebDriverWait(driver, 1);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='/unidade']")))
				.click();		
	}

	public void clicarMenuUsuarios() {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='/usuario']")))
				.click();		
	}

	public void clicarMenuAreasComuns() {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='/areacomum/cadastro']")))
				.click();
	}

	public boolean menuAreaComumVisivel() {
		return this.driver.findElement(By.xpath("//a[@href='/areacomum/cadastro']")).isDisplayed();
	}

	public void clicarMenuEnquetes() {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@href='/enquete']")))
				.click();		
	}

	public boolean menuEnqueteVisivel() {
		return this.driver.findElement(By.xpath("//a[@href='/enquete']")).isDisplayed();
	}
}
