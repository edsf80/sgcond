package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AcessoNegadoPage extends AbstractPage {

	private static final String X_PATH_CODIGO_ERRO = "//h1[@class='error-title']";
	
	private static final String X_PATH_BOTAO_VOLTAR_DASHBOARD = "//button[@class='mb-1 mat-raised-button mat-primary']";

	public AcessoNegadoPage(WebDriver driver) {
		super(driver);
	}

	public boolean ehExibida() {
		return this.driver.findElement(By.xpath(X_PATH_CODIGO_ERRO)).isDisplayed();
	}
	
	public void clicarBotaoVoltarDashboard() {
		this.driver.findElement(By.xpath(X_PATH_BOTAO_VOLTAR_DASHBOARD)).click();
	}
}
