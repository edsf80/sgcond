/**
 * 
 */
package br.com.eswconsulting.sgcond.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author edsf
 *
 */
public abstract class AbstractPage {

	protected WebDriver driver;

	/**
	 * 
	 */
	public AbstractPage(WebDriver driver) {
		this.driver = driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void esperarCarregar() {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	public void clicarBotaoSalvar() {
		List<WebElement> elements = this.driver
				.findElements(By.xpath("//span[@class='mat-button-wrapper'][contains(text(),'Salvar')]"));
		for (WebElement element : elements) {
			if (element.getText().equals("Salvar")) {
				element.click();
			}
		}
	}

	public boolean botaoSalvarHabilitado() {
		List<WebElement> elements = this.driver.findElements(By.tagName("button"));
		for (WebElement element : elements) {
			if (element.getText().equals("Salvar")) {
				return element.isEnabled();
			}
		}

		return false;
	}

	protected boolean itemEhExibido(By by) {
		WebDriverWait wait = new WebDriverWait(driver, 1);
		List<WebElement> elements = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
		return elements.size() > 0;
	}

	protected void inserirValorCampo(By by, String valor) {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.presenceOfElementLocated(by)).sendKeys(valor);
	}

	protected void alterarValorCampo(By by, String valor) {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(by));
		element.clear();
		element.sendKeys(valor);
	}

	protected boolean elementoPossuiValor(By by, String valor) {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		return wait.until(ExpectedConditions.presenceOfElementLocated(by)).getText().equals(valor);
	}

	protected void selecionarNaLista(By by, String valor) {
		List<WebElement> elements = this.driver.findElements(by);
		for (WebElement element : elements) {
			if (element.getText().equals(valor)) {
				element.click();
			}
		}
	}

}
