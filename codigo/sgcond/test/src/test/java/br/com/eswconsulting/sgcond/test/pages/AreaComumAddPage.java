package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AreaComumAddPage extends AbstractPage {

	private static final String ID_CAMPO_DESCRICAO_AREA_COMUM = "itDescAreaComum";

	public AreaComumAddPage(WebDriver driver) {
		super(driver);
	}

	public void inserirCampo(String areaComumInserirDescricao) {
		this.inserirValorCampo(By.id(ID_CAMPO_DESCRICAO_AREA_COMUM), areaComumInserirDescricao);
	}

	public void alterarCampo(String valor) {
		this.alterarValorCampo(By.id(ID_CAMPO_DESCRICAO_AREA_COMUM), valor);		
	}

}
