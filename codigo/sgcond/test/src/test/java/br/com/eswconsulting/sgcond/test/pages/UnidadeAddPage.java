package br.com.eswconsulting.sgcond.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UnidadeAddPage extends AbstractPage {

	private final static String ID_CAMPO_IDENTIFICADOR = "itIdentificadorUnidade";
	private final static String ID_CAMPO_USUARIO = "itUsuario";
	private static final String TAG_CAMPO_OPTION = "mat-option";
	private static final String ID_BOTAO_SALVAR_UNIDADE = "btnSalvarUnidade";
	private static final String XPATH_SNACK_VALIDACAO = "//button[@class='mat-simple-snackbar-action ng-tns-c30-49 ng-star-inserted']";	
	

	public UnidadeAddPage(WebDriver driver) {
		super(driver);
	}

	public void inserirCampos(String identificador, String morador) {
		this.driver.findElement(By.id(ID_CAMPO_IDENTIFICADOR)).sendKeys(identificador);
		if (!morador.isEmpty()) {
			this.driver.findElement(By.id(ID_CAMPO_USUARIO)).sendKeys(morador);
			List<WebElement> elements = this.driver.findElements(By.tagName(TAG_CAMPO_OPTION));
			elements.get(0).click();
		}
	}	

	public boolean botaoSalvarHabilitado() {
		return this.driver.findElement(By.id(ID_BOTAO_SALVAR_UNIDADE)).isEnabled();
	}

	public boolean msgErroValidacaoEhExibido() {
		//WebDriverWait wait = new WebDriverWait(driver, 1);
		return true;
	}

	public void alterarCampos(String identificador, String morador) {
		this.driver.findElement(By.id(ID_CAMPO_IDENTIFICADOR)).clear();
		this.driver.findElement(By.id(ID_CAMPO_IDENTIFICADOR)).sendKeys(identificador);
		if (!morador.isEmpty()) {
			this.driver.findElement(By.id(ID_CAMPO_USUARIO)).clear();;
			this.driver.findElement(By.id(ID_CAMPO_USUARIO)).sendKeys(morador);
			List<WebElement> elements = this.driver.findElements(By.tagName(TAG_CAMPO_OPTION));
			elements.get(0).click();
		}		
	}
}
