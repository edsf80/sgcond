package br.com.eswconsulting.sgcond.test.steps;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class UsuarioSteps {

	private static final String NOME_USUARIO_BUSCA = "Morador";
	private static final String NOME_USUARIO_INSERIR = "Novo";
	private static final String EMAIL_USUARIO_INSERIR = "novo@teste.com";
	private static final String SENHA_USUARIO_INSERIR = "123";
	private static final String EMAIL_USUARIO_EDITAR = "alterar@teste.com";
	private static final String EMAIL_USUARIO_BUSCA = "morador@teste.com";
	private static final String EMAIL_OUTRO_USUARIO_BUSCA = "morador2@teste.com";

	@Quando("^acesso a tela de usuários$")
	public void acessoATelaDeUsuários() throws Throwable {
		PageFactory.getInstance().getMainPage().clicarMenuUsuarios();
		PageFactory.getInstance().getUsuarioListPage().esperarCarregar();
	}

	@Então("^todos os usuários cadastrados devem ser exibidos$")
	public void todosOsUsuáriosCadastradosDevemSerExibidos() throws Throwable {
		Assert.assertTrue(PageFactory.getInstance().getUsuarioListPage().listaEhExibida());
	}

	@Quando("^digito no filtro exatamente um valor existente na lista de usuários$")
	public void digitoNoFiltroExatamenteUmValorExistenteNaListaDeUsuários() throws Throwable {
		PageFactory.getInstance().getUsuarioListPage().inserirValorFiltro(NOME_USUARIO_BUSCA);
	}

	@Então("^apenas esse usuário deverá ser exibido na lista$")
	public void apenasEsseUsuárioDeveráSerExibidoNaLista() throws Throwable {
		Assert.assertTrue(PageFactory.getInstance().getUsuarioListPage().listaPossuiValorUnico(NOME_USUARIO_BUSCA));
	}

	@Então("^o item de menu de usuários não deve ser exibido$")
	public void oItemDeMenuDeUsuáriosNãoDeveSerExibido() throws Throwable {
		Assert.assertFalse(PageFactory.getInstance().getMainPage().menuUsuarioVisivel());
	}

	@Quando("^insiro campos \"([^\"]*)\" do usuário$")
	public void insiroCamposOUsuário(String lista) throws Throwable {
		String campos[] = lista.split(",");
		String nome = "", email = "", senha = "";
		for (String campo : campos) {
			if (campo.trim().equals("Nome")) {
				nome = NOME_USUARIO_INSERIR;
			} else if (campo.trim().equals("E-mail")) {
				email = EMAIL_USUARIO_INSERIR;
			} else if (campo.trim().equals("Senha")) {
				senha = SENHA_USUARIO_INSERIR;
			}
		}

		PageFactory.getInstance().getUsuarioAddPage().inserirCampos(nome, email, senha);
	}

	@Quando("^clico no botão de adicionar usuário$")
	public void clicoNoBotãoDeAdicionarUsuário() throws Throwable {
		PageFactory.getInstance().getUsuarioListPage().clicarBotaoAdicionar();
		PageFactory.getInstance().getUsuarioAddPage().esperarCarregar();
	}

	@Quando("^clico no botão salvar usuário$")
	public void clicoNoBotãoSalvarUsuário() throws Throwable {
		PageFactory.getInstance().getUsuarioAddPage().clicarBotaoSalvar();
		PageFactory.getInstance().getUsuarioListPage().esperarCarregar();
	}

	@Então("^o usuário adicionado deve aparecer na lista$")
	public void oUsuárioAdicionadoDeveAparecerNaLista() throws Throwable {
		PageFactory.getInstance().getUsuarioListPage().inserirValorFiltro(EMAIL_USUARIO_INSERIR);
		Assert.assertTrue(PageFactory.getInstance().getUsuarioListPage().listaPossuiValorUnico(EMAIL_USUARIO_INSERIR));
	}

	@Então("^o botão salvar usuário não deve estar habilitado$")
	public void oBotãoSalvarUsuárioNãoDeveEstarHabilitado() throws Throwable {
		Assert.assertFalse(PageFactory.getInstance().getUsuarioAddPage().botaoSalvarHabilitado());
	}

	@Quando("^seleciono um usuário da lista$")
	public void selecionoUmUsuárioDaLista() throws Throwable {
		PageFactory.getInstance().getUsuarioListPage().inserirValorFiltro(EMAIL_USUARIO_EDITAR);
		PageFactory.getInstance().getUsuarioListPage().selecionarNaLista(EMAIL_USUARIO_EDITAR);
	}
	
	@Quando("^seleciono outro usuário da lista$")
	public void selecionoOutroUsuárioDaLista() throws Throwable {
		PageFactory.getInstance().getUsuarioListPage().inserirValorFiltro(EMAIL_OUTRO_USUARIO_BUSCA);
		PageFactory.getInstance().getUsuarioListPage().selecionarNaLista(EMAIL_OUTRO_USUARIO_BUSCA);
	}

	@Quando("^altero os campos do usuário$")
	public void alteroOsCamposDoUsuário() throws Throwable {
		PageFactory.getInstance().getUsuarioAddPage().alterarCampos("Alterado", "alterado@teste.com", "123");
	}

	@Então("^o usuário aparece com novo valor na lista$")
	public void oUsuárioApareceComNovoValorNaLista() throws Throwable {
		PageFactory.getInstance().getUsuarioListPage().inserirValorFiltro("alterado@teste.com");
		Assert.assertTrue(PageFactory.getInstance().getUsuarioListPage().listaPossuiValorUnico("alterado@teste.com"));
	}

	@Quando("^altero o e-mail do usuário para um já cadastrado$")
	public void alteroOEMailDoUsuárioParaUmJáCadastrado() throws Throwable {
		PageFactory.getInstance().getUsuarioAddPage().alterarCampos("Alterado", EMAIL_USUARIO_INSERIR, "123");
	}

	@Então("^mensagem de validação do usuário deve ser exibida$")
	public void mensagemDeValidaçãoDoUsuárioDeveSerExibida() throws Throwable {

	}

	@Quando("^insiro campos do usuário com e-mail já cadastrado$")
	public void insiroCamposDoUsuárioComEMailJáCadastrado() throws Throwable {
		PageFactory.getInstance().getUsuarioAddPage().inserirCampos(NOME_USUARIO_BUSCA, EMAIL_USUARIO_BUSCA, "123");
	}
}
