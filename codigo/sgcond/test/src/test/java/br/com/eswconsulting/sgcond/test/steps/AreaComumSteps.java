package br.com.eswconsulting.sgcond.test.steps;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class AreaComumSteps {

	private static final String AREA_COMUM_BUSCA_DESCRICAO = "Area comum teste";
	private static final String AREA_COMUM_INSERIR_DESCRICAO = "Nova area";
	private static final String AREA_COMUM_DESCRICAO_EDITAR = "Area comum para alterar";

	@Quando("^acesso a tela de áreas comuns$")
	public void acessoATelaDeÁreasComuns() throws Throwable {
		PageFactory.getInstance().getMainPage().clicarMenuAreasComuns();
	}

	@Então("^todos as áreas comuns cadastradas devem ser exibidas$")
	public void todosAsÁreasComunsCadastradasDevemSerExibidas() throws Throwable {
		Assert.assertTrue(PageFactory.getInstance().getAreaComumListPage().listaEhExibida());
	}

	@Quando("^digito no filtro exatamente um valor existente na lista de áreas comuns$")
	public void digitoNoFiltroExatamenteUmValorExistenteNaListaDeÁreasComuns() throws Throwable {
		PageFactory.getInstance().getAreaComumListPage().inserirValorFiltro(AREA_COMUM_BUSCA_DESCRICAO);
	}

	@Então("^apenas essa área comum deverá ser exibida na lista$")
	public void apenasEssaÁreaComumDeveráSerExibidaNaLista() throws Throwable {
		Assert.assertTrue(
				PageFactory.getInstance().getAreaComumListPage().listaPossuiValorUnico(AREA_COMUM_BUSCA_DESCRICAO));
	}

	@Então("^o item de menu de áreas comuns não deve ser exibido$")
	public void oItemDeMenuDeÁreasComunsNãoDeveSerExibido() throws Throwable {
		Assert.assertFalse(PageFactory.getInstance().getMainPage().menuAreaComumVisivel());
	}

	@Quando("^clico no botão de adicionar área comum$")
	public void clicoNoBotãoDeAdicionarÁreaComum() throws Throwable {
		PageFactory.getInstance().getAreaComumListPage().clicarBotaoAdicionar();
	}

	@Quando("^insiro o campo \"([^\"]*)\" da área comum$")
	public void insiroOCampoDaÁreaComum(String descricao) throws Throwable {
		if (!descricao.isEmpty()) {
			PageFactory.getInstance().getAreaComumAddPage().inserirCampo(AREA_COMUM_INSERIR_DESCRICAO);
		}
	}

	@Então("^a área comum adicionada deve aparecer na lista$")
	public void aÁreaComumAdicionadaDeveAparecerNaLista() throws Throwable {
		PageFactory.getInstance().getAreaComumListPage().inserirValorFiltro(AREA_COMUM_INSERIR_DESCRICAO);
		Assert.assertTrue(PageFactory.getInstance().getAreaComumListPage().listaPossuiValorUnico(AREA_COMUM_INSERIR_DESCRICAO));
	}
	
	@Então("^o botão salvar área comum não deve estar habilitado$")
	public void oBotãoSalvarÁreaComumNãoDeveEstarHabilitado() throws Throwable {
	    Assert.assertFalse(PageFactory.getInstance().getAreaComumAddPage().botaoSalvarHabilitado());
	}
	
	@Quando("^seleciono uma área comum da lista$")
	public void selecionoUmaÁreaComumDaLista() throws Throwable {
		PageFactory.getInstance().getAreaComumListPage().inserirValorFiltro(AREA_COMUM_DESCRICAO_EDITAR);
	    PageFactory.getInstance().getAreaComumListPage().selecionarNaLista(AREA_COMUM_DESCRICAO_EDITAR);
	}

	@Quando("^altero os campos da área comum$")
	public void alteroOsCamposDaÁreaComum() throws Throwable {
		PageFactory.getInstance().getAreaComumAddPage().alterarCampo("Area Alterada");
	}

	@Então("^a área comum aparece com novo valor na lista$")
	public void aÁreaComumApareceComNovoValorNaLista() throws Throwable {
		PageFactory.getInstance().getAreaComumListPage().inserirValorFiltro("Area Alterada");
		Assert.assertTrue(PageFactory.getInstance().getAreaComumListPage().listaPossuiValorUnico("Area Alterada"));
	}
}
