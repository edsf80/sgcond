package br.com.eswconsulting.sgcond.test.steps;

import org.junit.Assert;

import br.com.eswconsulting.sgcond.test.pages.LoginPage;
import br.com.eswconsulting.sgcond.test.pages.MainPage;
import br.com.eswconsulting.sgcond.test.pages.PageFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class LoginSteps {
	
	private LoginPage loginPage;
	
	private MainPage mainPage;	
	
	@Before
	public void inicializar() {
		//this.driver = new FirefoxDriver();
		this.mainPage = PageFactory.getInstance().getMainPage();
		this.loginPage = PageFactory.getInstance().getLoginPage();
		//this.loginPage.abrir();
	}
	
	@After 
	public void finalizar() {
		if (this.mainPage.ehExibida()) {
			this.mainPage.clicarLogout();
		} else {
			this.loginPage.resetForm();
		}
	}

	@Dado("^que estou acessando a aplicação$")
	public void queEstouAcessandoAAplicação() throws Throwable {		
		this.loginPage.abrir();				
	}
	
	@Quando("^informo usuário existente$")
	public void informoUsuárioESenhaVálida() throws Throwable {
		this.loginPage.inserirLogin("morador@teste.com");		
	}
	
	@Quando("^senha válida$")
	public void senhaVálida() throws Throwable {
		this.loginPage.inserirSenha("123");
	}
	
	@Quando("^informo usuário \"([^\"]*)\" e senha \"([^\"]*)\"$")
	public void informoUsuarioESenha(String usuario, String senha) throws Throwable {
		//this.loginPage.resetForm();
		this.loginPage.inserirLogin(usuario);
		this.loginPage.inserirSenha(senha);
	}
	
	@Quando("^clico no botão entrar$")
	public void clicoNoBotãoEntrar() throws Throwable {
	    this.loginPage.clicarEntrar();
	}
	

	@Então("^visualizo a página inicial$")
	public void visualizoAPáginaInicial() throws Throwable {
	    Assert.assertTrue(this.mainPage.ehExibida());	    
	}
	
	@Então("^mensagem de erro de validação deve aparecer$")
	public void mensagemDeAvisoDeveAparecer() throws Throwable {
	    Assert.assertTrue(this.loginPage.mensagemErroValidaoExibida());
	}
	
	@Então("^botão entrar permanece desabilitado$")
	public void botãoEntrarPermaneceDesabilitado() throws Throwable {
	    Assert.assertTrue(!this.loginPage.botaoEntrarHabilitado());
	}
}
