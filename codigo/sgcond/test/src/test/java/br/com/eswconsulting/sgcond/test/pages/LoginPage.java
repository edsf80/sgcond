package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends AbstractPage {

	private final static String ID_CAMPO_USERNAME = "tUsername";
	private final static String ID_CAMPO_SENHA = "tSenha";
	private final static String ID_BOTAO_LOGIN = "btnLogin";
	private final static String ID_LABEL_MSG_VALIDACAO = "lFalhaLogin";

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public void abrir() {
		this.driver.get("http://localhost:5000");
	}

	public void inserirLogin(String login) {
		WebDriverWait wait = new WebDriverWait(driver, 1);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(ID_CAMPO_USERNAME))).sendKeys(login);
	}

	public void inserirSenha(String senha) {
		this.driver.findElement(By.id(ID_CAMPO_SENHA)).sendKeys(senha);
	}

	public void clicarEntrar() {
		this.driver.findElement(By.id(ID_BOTAO_LOGIN)).click();
	}

	public boolean botaoEntrarHabilitado() {
		return this.driver.findElement(By.id(ID_BOTAO_LOGIN)).isEnabled();
	}

	public boolean mensagemErroValidaoExibida() {
		return this.driver.findElement(By.id(ID_LABEL_MSG_VALIDACAO)).isDisplayed();
	}

	public void resetForm() {
		this.driver.findElement(By.id(ID_CAMPO_USERNAME)).clear();
		this.driver.findElement(By.id(ID_CAMPO_SENHA)).clear();
	}

}
