package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class UsuarioListPage extends BaseListPage {

	private static final String XPATH_HEADER_NOME_USUARIO = "//html//datatable-header-cell[1]";

	public UsuarioListPage(WebDriver driver) {
		super(driver);
	}

	public boolean listaEhExibida() {
		return this.itemEhExibido(By.xpath(XPATH_HEADER_NOME_USUARIO));
	}			
}
