package br.com.eswconsulting.sgcond.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseListPage extends AbstractPage {

	private static final String XPATH_ITEM_DA_LISTA = "//html//datatable-row-wrapper[1]/datatable-body-row[1]/div[2]/datatable-body-cell[2]/div[1]";
	private static final String XPATH_BOTAO_ADICIONAR = "//button[@color='primary']";
	
	public BaseListPage(WebDriver driver) {
		super(driver);
	}
	
	public void selecionarNaLista(String valor) {
		this.selecionarNaLista(By.xpath(XPATH_ITEM_DA_LISTA), valor);
	}
	
	public void clicarBotaoAdicionar() {
		WebDriverWait wait = new WebDriverWait(driver, 1);		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(XPATH_BOTAO_ADICIONAR))).click();;
	}
	
	public boolean listaPossuiValorUnico(String nomeUsuarioBusca) {
		return this.elementoPossuiValor(By.xpath(XPATH_ITEM_DA_LISTA), nomeUsuarioBusca);
	}
	
	public void inserirValorFiltro(String nomeAreaComumBusca) {
		this.inserirValorCampo(By.id("itFiltro"), nomeAreaComumBusca);
	}
}
