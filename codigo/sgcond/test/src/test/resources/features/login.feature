# language: pt
Funcionalidade: Realizar Login
	Como usuário
	Eu quero executar login no sistema
	Para que eu possa utilizá-lo
	
Cenário: Login com sucesso
	Dado que estou acessando a aplicação
	Quando informo usuário existente 
	E senha válida
	E clico no botão entrar
	Então visualizo a página inicial

Esquema do Cenário: Login nome de usuário inexistente ou senha inválida
	Dado que estou acessando a aplicação
	Quando informo usuário <usuario> e senha <senha>	
	E clico no botão entrar
	Então mensagem de erro de validação deve aparecer

Exemplos:
	| usuario                       |   senha |
	| "aa@aa.com"                   |  "123"  |
	| "ednaldo.dilorenzo@gmail.com" |  "xxx"  |

Esquema do Cenário: Login com campo obrigatório não preenchido
	Dado que estou acessando a aplicação
	Quando informo usuário <usuario> e senha <senha>	
	E clico no botão entrar
	Então botão entrar permanece desabilitado

Exemplos:
	| usuario                       |   senha |
	| ""                            |  ""     |
	| ""                            |  "123"  |
	| "aa@aa.com"                   |    ""   |