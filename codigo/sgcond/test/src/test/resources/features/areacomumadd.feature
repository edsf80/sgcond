# language: pt
Funcionalidade: Adicionar área comum
	Como síndico
	Eu quero adicionar áreas comuns ao condomínio
	Para que as mesmas possam ser reservadas

Cenário: Adicionar área comum com campos obrigatórios preenchidos
	Dado que estou logado como síndico
	Quando acesso a tela de áreas comuns
	E clico no botão de adicionar área comum
	E insiro o campo "Descrição" da área comum
	E clico no botão salvar
	Então a área comum adicionada deve aparecer na lista
	
Cenário: Adicionar usuário campos obrigatórios não preenchidos
	Dado que estou logado como síndico
	Quando acesso a tela de áreas comuns
	E clico no botão de adicionar área comum
	E insiro o campo "" da área comum
	Então o botão salvar área comum não deve estar habilitado
	
Cenário: Editar área comum existente
	Dado que estou logado como síndico
	E acesso a tela de áreas comuns
	Quando seleciono uma área comum da lista
	E altero os campos da área comum
	E clico no botão salvar
	Então a área comum aparece com novo valor na lista	

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/areacomum/cadastro/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/areacomum/cadastro/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Administrador não deve acessar por link direto
	Dado que estou logado como administrador
	Quando acesso a uri "/areacomum/cadastro/add"
	Então tela de acesso negado deve ser exibida