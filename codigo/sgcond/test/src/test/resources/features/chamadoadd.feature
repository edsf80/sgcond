# language: pt
Funcionalidade: Adicionar chamado
	Como morador
	Eu quero adicionar chamado do condomínio
	Para reportar problema a ser resolvido

Cenário: Adicionar chamado campos obrigatórios preenchidos
	Dado que estou logado como morador
	Quando acesso a tela de chamados
	E clico no botão de adicionar
	E insiro campos "título, descrição, status"
	E clico no botão salvar
	Então o chamado adicionado deve aparecer na lista
	
Esquema do Cenário: Adicionar chamado campos obrigatórios não preenchidos
	Dado que estou logado como morador
	Quando acesso a tela de chamados
	E clico no botão de adicionar
	E insiro campos <campos>
	Então o botão salvar não deve estar habilitado
	
Exemplos:
	| campos                   |
	| "título, descrição, "    |
	| "título, , status"       |
	| " ,descrição, status"    |
	
Cenário: Editar chamado existente sendo dono
	Dado que estou logado como morador
	E acesso a tela de chamados
	Quando seleciono um chamado da lista
	E altero os campos do chamado
	E clico no botão salvar
	Então o item aparece com novo valor na lista

#Cenário: Editar chamado existente não sendo dono
#	Dado que estou logado como morador
#	E acesso a tela de chamados
#	Quando seleciono um chamado da lista que não sou dono
#	Então o botão salvar não deve estar habilitado
#	
#Cenário: Editar chamado existente sendo síndico
#	Dado que estou logado como síndico
#	E acesso a tela de chamados
#	Quando seleciono um chamado da lista
#	E altero os campos do chamado
#	E clico no botão salvar
#	Então o item aparece com novo valor na lista

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/chamado/add"
	Então tela de acesso negado deve ser exibida