# language: pt
Funcionalidade: Listar Usuários
	Como administrador
	Eu quero listar os usuários do sistema
	Para verificar os usuários cadastrados ou selecionar para edição

Cenário: Dados cadastrados são listados
	Dado que estou logado como administrador
	Quando acesso a tela de usuários
	Então todos os usuários cadastrados devem ser exibidos
	
Cenário: Filtro restringe itens exibidos na lista
	Dado que estou logado como administrador
	Quando acesso a tela de usuários
	E digito no filtro exatamente um valor existente na lista de usuários
	Então apenas esse usuário deverá ser exibido na lista

Cenário: Menu não aparece para funcionário
	Dado que estou logado como funcionário
	Quando acesso a tela principal
	Então o item de menu de usuários não deve ser exibido

Cenário: Menu não aparece para morador
	Dado que estou logado como morador
	Quando acesso a tela principal
	Então o item de menu de usuários não deve ser exibido
	
Cenário: Menu não aparece para Síndico
	Dado que estou logado como síndico
	Quando acesso a tela principal
	Então o item de menu de usuários não deve ser exibido

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/usuario"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/usuario"
	Então tela de acesso negado deve ser exibida
	
Cenário: Síndico não deve acessar por link direto
	Dado que estou logado como síndico
	Quando acesso a uri "/usuario"
	Então tela de acesso negado deve ser exibida