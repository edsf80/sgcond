# language: pt
Funcionalidade: Adicionar unidade
	Como administrador
	Eu quero adicionar uma uniade ao condomínio
	Para que o morador possa acessar o sistema

Cenário: Adicionar unidade campos obrigatórios preenchidos
	Dado que estou logado como administrador
	Quando acesso a tela de unidades
	E clico no botão de adicionar unidade
	E insiro campos "identificador, proprietário" da unidade
	E clico no botão salvar unidade
	Então a unidade adicionada deve aparecer na lista
	
Esquema do Cenário: Adicionar unidade campos obrigatórios não preenchidos
	Dado que estou logado como administrador
	Quando acesso a tela de unidades
	E clico no botão de adicionar unidade
	E insiro campos <campos> da unidade
	Então o botão salvar unidade não deve estar habilitado
	
Exemplos:
	| campos            |
	| "identificador,"  |
	| ", proprietário"  |
	| ","               |
	
Cenário: Adicionar unidade com identificador existente
	Dado que estou logado como administrador
	Quando acesso a tela de unidades
	E clico no botão de adicionar unidade
	E insiro campos com identificador existente
	E clico no botão salvar unidade
	Então mensagem de erro de validação de unidade deve aparecer
	
Cenário: Editar unidade existente
	Dado que estou logado como administrador
	E acesso a tela de unidades
	Quando seleciono uma unidade existente
	E altero os campos da unidade
	E clico no botão salvar unidade
	Então a unidade aparece com novo valor na lista

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/unidade/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/unidade/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Síndico não deve acessar por link direto
	Dado que estou logado como síndico
	Quando acesso a uri "/unidade/add"
	Então tela de acesso negado deve ser exibida