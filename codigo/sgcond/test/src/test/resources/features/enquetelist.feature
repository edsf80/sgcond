# language: pt
Funcionalidade: Listar enquetes
	Como síndico
	Eu quero listar as enquetes do condomínio
	Para verificar as enquetes cadastradas ou selecionar para edição

Cenário: Dados cadastrados são listados
	Dado que estou logado como síndico
	Quando acesso a tela de enquetes
	Então todos as enquetes cadastradas devem ser exibidas
	
Cenário: Filtro restringe itens exibidos na lista
	Dado que estou logado como síndico
	Quando acesso a tela de enquetes
	E digito no filtro exatamente um valor existente na lista de enquetes
	Então apenas essa enquete deverá ser exibida na lista

Cenário: Menu não aparece para funcionário
	Dado que estou logado como funcionário
	Quando acesso a tela principal
	Então o item de menu de enquetes não deve ser exibido

Cenário: Menu não aparece para morador
	Dado que estou logado como morador
	Quando acesso a tela principal
	Então o item de menu de enquetes não deve ser exibido
	
Cenário: Menu não aparece para administrador
	Dado que estou logado como administrador
	Quando acesso a tela principal
	Então o item de menu de enquetes não deve ser exibido

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/enquete"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/enquete"
	Então tela de acesso negado deve ser exibida
	
Cenário: Administrador não deve acessar por link direto
	Dado que estou logado como administrador
	Quando acesso a uri "/enquete"
	Então tela de acesso negado deve ser exibida