# language: pt
Funcionalidade: Listar Chamados
	Como morador
	Eu quero listar os chamados do condomínio
	Para verificar pendências do condomínio

Cenário: Dados cadastrados são listados
	Dado que estou logado como morador
	Quando acesso a tela de chamados
	Então todos os chamados cadastrados devem ser exibidos
	
Cenário: Filtro restringe itens exibidos na lista
	Dado que estou logado como morador
	Quando acesso a tela de chamados
	E digito no filtro exatamente um valor existente na lista
	Então apenas esse valor deverá ser exibido na lista

Cenário: Menu não aparece para funcionário
	Dado que estou logado como funcionário
	Quando acesso a tela principal
	Então o item de menu de chamados não deve ser exibido

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/chamado"
	Então tela de acesso negado deve ser exibida