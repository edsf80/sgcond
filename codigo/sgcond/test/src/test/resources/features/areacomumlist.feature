# language: pt
Funcionalidade: Listar áreas comuns
	Como síndico
	Eu quero listar as áreas comuns do condomínio
	Para verificar as áreas comuns cadastradas ou selecionar para edição

Cenário: Dados cadastrados são listados
	Dado que estou logado como síndico
	Quando acesso a tela de áreas comuns
	Então todos as áreas comuns cadastradas devem ser exibidas
	
Cenário: Filtro restringe itens exibidos na lista
	Dado que estou logado como síndico
	Quando acesso a tela de áreas comuns
	E digito no filtro exatamente um valor existente na lista de áreas comuns
	Então apenas essa área comum deverá ser exibida na lista

Cenário: Menu não aparece para funcionário
	Dado que estou logado como funcionário
	Quando acesso a tela principal
	Então o item de menu de áreas comuns não deve ser exibido

Cenário: Menu não aparece para morador
	Dado que estou logado como morador
	Quando acesso a tela principal
	Então o item de menu de áreas comuns não deve ser exibido
	
Cenário: Menu não aparece para administrador
	Dado que estou logado como administrador
	Quando acesso a tela principal
	Então o item de menu de áreas comuns não deve ser exibido

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/areacomum/cadastro"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/areacomum/cadastro"
	Então tela de acesso negado deve ser exibida
	
Cenário: Administrador não deve acessar por link direto
	Dado que estou logado como administrador
	Quando acesso a uri "/areacomum/cadastro"
	Então tela de acesso negado deve ser exibida