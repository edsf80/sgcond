# language: pt
Funcionalidade: Adicionar usuário
	Como administrador
	Eu quero adicionar usuário do sistema
	Para que o mesmo possa acessar o sistema

Cenário: Adicionar usuário campos obrigatórios preenchidos
	Dado que estou logado como administrador
	Quando acesso a tela de usuários
	E clico no botão de adicionar usuário
	E insiro campos "Nome, E-mail, Senha" do usuário
	E clico no botão salvar usuário
	Então o usuário adicionado deve aparecer na lista
	
Cenário: Adicionar usuário campos e-mail já cadastrado
	Dado que estou logado como administrador
	E acesso a tela de usuários
	E clico no botão de adicionar usuário
	Quando insiro campos do usuário com e-mail já cadastrado
	E clico no botão salvar usuário
	Então mensagem de validação do usuário deve ser exibida
	
Esquema do Cenário: Adicionar usuário campos obrigatórios não preenchidos
	Dado que estou logado como administrador
	Quando acesso a tela de usuários
	E clico no botão de adicionar usuário
	E insiro campos <campos> do usuário
	Então o botão salvar usuário não deve estar habilitado
	
Exemplos:
	| campos             |
	| "Nome, E-mail, "   |
	| ", E-mail, Senha"  |
	| "Nome, , Senha"    |
	
Cenário: Editar usuário existente
	Dado que estou logado como administrador
	E acesso a tela de usuários
	Quando seleciono um usuário da lista
	E altero os campos do usuário
	E clico no botão salvar usuário
	Então o usuário aparece com novo valor na lista	
	
Cenário: Editar usuário existente alterando e-mail para já cadastrado
	Dado que estou logado como administrador
	E acesso a tela de usuários
	E seleciono outro usuário da lista
	Quando altero o e-mail do usuário para um já cadastrado
	E clico no botão salvar usuário
	Então mensagem de validação do usuário deve ser exibida

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/usuario/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/usuario/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Síndico não deve acessar por link direto
	Dado que estou logado como síndico
	Quando acesso a uri "/usuario/add"
	Então tela de acesso negado deve ser exibida
