# language: pt
Funcionalidade: Adicionar e editar enquete
	Como síndico
	Eu quero adicionar uma enquete ao condomínio
	Para que o morador possa votar nela e obter uma opinião

Cenário: Adicionar enquete com campos obrigatórios preenchidos
	Dado que estou logado como síndico
	Quando acesso a tela de enquetes
	E clico no botão de adicionar enquete
	E insiro campos "Título, Status, Itens" da enquete
	E clico no botão salvar
	Então a enquete adicionada deve aparecer na lista
	
Esquema do Cenário: Adicionar enquete campos obrigatórios não preenchidos
	Dado que estou logado como síndico
	Quando acesso a tela de enquetes
	E clico no botão de adicionar enquete
	E insiro campos <campos> da enquete
	Então o botão salvar enquete não deve estar habilitado
	
Exemplos:
	| campos             |
	| "Título,Status,"   |
	| ", Status, Itens"  |
	| "Título, , Itens"  |
	
Cenário: Editar enquete existente
	Dado que estou logado como síndico
	E acesso a tela de enquetes
	Quando seleciono uma enquete existente
	E altero os campos da enquete
	E clico no botão salvar
	Então a enquete aparece com novo valor na lista

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/enquete/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/enquete/add"
	Então tela de acesso negado deve ser exibida
	
Cenário: Administrador não deve acessar por link direto
	Dado que estou logado como administrador
	Quando acesso a uri "/enquete/add"
	Então tela de acesso negado deve ser exibida