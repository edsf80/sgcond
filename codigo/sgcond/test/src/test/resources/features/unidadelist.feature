# language: pt
Funcionalidade: Listar Unidades
	Como administrador
	Eu quero listar as unidades do condomínio
	Para verificar as unidades cadastradas ou selecionar para edição

Cenário: Dados cadastrados são listados
	Dado que estou logado como administrador
	Quando acesso a tela de unidades
	Então todos as unidades cadastradas devem ser exibidas
	
Cenário: Filtro restringe itens exibidos na lista
	Dado que estou logado como administrador
	Quando acesso a tela de unidades
	E digito no filtro exatamente um valor existente na lista de unidades
	Então apenas essa unidade deverá ser exibida na lista

Cenário: Menu não aparece para funcionário
	Dado que estou logado como funcionário
	Quando acesso a tela principal
	Então o item de menu de unidades não deve ser exibido

Cenário: Menu não aparece para morador
	Dado que estou logado como morador
	Quando acesso a tela principal
	Então o item de menu de unidades não deve ser exibido
	
Cenário: Menu não aparece para Síndico
	Dado que estou logado como síndico
	Quando acesso a tela principal
	Então o item de menu de unidades não deve ser exibido

Cenário: Funcionário não deve acessar por link direto
	Dado que estou logado como funcionário
	Quando acesso a uri "/unidade"
	Então tela de acesso negado deve ser exibida
	
Cenário: Morador não deve acessar por link direto
	Dado que estou logado como morador
	Quando acesso a uri "/unidade"
	Então tela de acesso negado deve ser exibida
	
Cenário: Síndico não deve acessar por link direto
	Dado que estou logado como síndico
	Quando acesso a uri "/unidade"
	Então tela de acesso negado deve ser exibida